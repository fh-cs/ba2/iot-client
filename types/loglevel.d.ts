export = loglevel;
declare const loglevel: {
    debug: Function;
    disableAll: Function;
    enableAll: Function;
    error: Function;
    getLevel: Function;
    getLogger: Function;
    getLoggers: Function;
    info: Function;
    levels: {
        DEBUG: number;
        ERROR: number;
        INFO: number;
        SILENT: number;
        TRACE: number;
        WARN: number;
    };
    log: Function;
    methodFactory: Function;
    name: any;
    noConflict: Function;
    setDefaultLevel: Function;
    setLevel: Function;
    trace: Function;
    warn: Function;
};
