Zertifikat wurde mit folgenden Befehl erzeugt:
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout cert.key -out cert.crt -subj '/CN=node-dtls-proxy/O=m4n3dw0lf/C=BR'
```
