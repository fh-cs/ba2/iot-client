Infos für das Erzeugen der Zertifikatsdateien:
http://www.steves-internet-guide.com/mosquitto-tls/


# MQTT-Clients (IoT-Server, IoT-Client):
ca.crt: Dateipfad zu dieser Datei in den Driver-Settings für den MQTT-Driver unter der Eigenschaft 'caFile' angeben.

` "driver": [`\
`   {  ...`\
`      "host": "192.168.110.132",`\
`      "port": 8883,`\
`      ...`\
`      "useSSL": true,`\
`      "caFile":"/home/chris/project/fh/ba/iot-client/cert/ca.crt",`\
`      ... }`
            

Um Self-signed-Zertifikate zu erlauben, wurde im MQTT-Driver die Option *rejectUnauthorized* auf false gesetzt.     

# MQTT-Broker:
ca.crt: Am Host, wo der MQTT-Broker läuft, die Datei unter /etc/mosquitto/ca_certifates ablegen
server.key und server.crt: Diese unter /etc/mosquitto/certs ablegen

In der mosquitto.config diese Dateien angeben.
cafile  /etc/mosquitto/ca_certificates/ca.crt
keyfile /etc/mosquitto/certs/server.key
certfile /etc/mosquitto/certs/server.crt


