# Information for Developers

## Requirements
Project is designed to run on linux systems only.
Following software-packages are required to run this project:
- node.js
- npm
- nodemon
- git
- wireshark
- visual studio code as editor



## Start auto-compilation
1. Open Visual code and press Shift+Ctrl+B
2. Set task **tsc: watch - ba-client/tsconfig.json**
## Start client process
1. Open terminal and navigate to ba-client-directory
2. Run one of the following commands:
  - Run **nodemon ./dist/iot-test.js** using default settings-file *settings.json*
  - For IoT-Client: **nodemon ./dist/iot-test.js -settings settings-client.json**
  - For IoT-Server: **nodemon ./dist/iot-test.js -settings settings-server.json**



\
For Visual-Code only:\
To get a better view, press *Ctrl+K V* (side-view) or *Ctrl+Shift+V* (toogle-view)



# Wireshark
Wireshark is used to capture and measure the packages between client and server.

## Installation
Run following command for installation **sudo apt-get install wireshark**

Do not use super-users to start wireshark. Instead, allow wireshark to capture network traffic as normal user. Run following command and select 'Yes' inside the upcoming dialog **sudo dpkg-reconfigure wireshark-common**.\
All users member of group **wireshark** now allowed to run wireshark, also as normal-user. Add yourself to the wireshark-group by running:
**sudo adduser $USER wireshark**. 


Installed version is:
- Wireshark 2.6.5 (Git v2.6.5 packaged as 2.6.5-1~ubuntu18.04.0)





# MQTT-Client
Wird das MQTT-Protokoll verwendet, stellt der IoT-Client eine Verbindung zum MQTT-Broker her. Auch der IoT-Server stellt eine Verbindung als Client zum Broker her. Basierend auf Topics (Namensräume), verteilt der Broker eingehende Nachrichten eines MQTT-Clients (publisher) an Clients, welche dieses Topic abonniert haben (subscribers).

## Quality of Service (QoS)
Die QoS bestimmt, mit welcher Zuverlässigkeit Daten von einem MQTT-Client an den Broker und umgekehrt gesendet werden.
Mögliche Stufen sind:
- At most once (0): Die meisten Meldungen kommen einmal an (send and forget). Jedoch kann nicht garantiert werden, dass jede Meldung der Empfänger erhält.
- At least once (1): Es wird garantiert, das der Empfänger mindestens eine Nachricht empfängt (möglicherweise aber auch mehr).
- Exactly once (2): Es wird garantiert, das der Empfänger genau eine Nachricht empfängt.

Welches Quality of Service verwendet wird ist abhängig von der Schema-Gruppe (data, notify, methods, config) und ob dieser diese abonniert oder veröffentlicht.

Die Einstellung hierfür erfolgt in der Settings-Datei. Welche QoS-Stufe verwendet wird, wird im Kapitel _Verhalten und Aufbau der Daten_ eingehender beschrieben.

(https://www.hivemq.com/blog/mqtt-essentials-part-6-mqtt-quality-of-service-levels/)

## Retained Messages
Ist ein Subscriber zum Zeitpunkt des Absetzen einer Nachricht durch einen MQTT-Client nicht mit dem Broker verbunden, empfängt dieser nicht die Nachricht. Setzt ein MQTT-Client eine Nachricht mit **retained**-Flag ab, wird die Nachricht unter dieser Topic am Broker zwischengespeichert. Subscribers, welche dieses Topic abonnieren, empfangen diese die letzte, unter dieser Topic, veröffentlichte Nachricht. Somit empfängt der Subscriber die letzte veröffentlichte Meldung.
(https://www.hivemq.com/blog/mqtt-essentials-part-8-retained-messages/)

## Persistente Verbindung
Sollte ein MQTT-Client nicht mit dem Broker verbunden sein, es werden jedoch Nachrichten unter seiner Topic veröffentlicht, so werden Nachrichten mit einer QoS größer gleich 1 vom Broker in einer Queue (Stappelspeicher) für diesen MQTT-Client zwischengespeichert. Nach Verbindungsaufbau mit dem MQTT-Broker empfängt dieser diese zwischengespeicherte Nachrichten bzw. alle Nachrichten, dessen Empfang vom MQTT-Client bis dato nicht bestätigt wurden.
Um eine persistente Verbindung aufzubauen, wird während des Verbingungsaufbaues mit dem MQTT-Broker das **Clean Session**-Flag auf false gesetzt.

Eine Datenzwischenpufferung ist abhängig vom eingesetzen IoT-Device. Geräte, welche keine kontinuierliche Verbindung herstellen (z.B. Batteriebetrieben Geräte), sollten eine persistente Verbindung herstellen. 

Diese Einstellung kann in der Settingsdatei vorgenommen werden. Default-Eigenschaft ist eine Verbindung ohne Persistenz.

(https://www.hivemq.com/blog/mqtt-essentials-part-7-persistent-session-queuing-messages/)

## Keep-Alive

Um sicherzustellen, dass eine Verbindung zwischen einem MQTT-CLient und dem Broker in Ordnung ist, muss innerhalb einer bestimmten Zeitspanne eine Nachricht erfolgreich übertragen werden. Wurde innerhalb dieser Zeitspanne keine Nachricht vom Client an den Broker übertragen, sendet der MQTT-Client einen Ping-Request an den Broker. Dieser Beantwortet diese Anfrage mit einem Ping-Response.
(https://www.hivemq.com/blog/mqtt-essentials-part-10-alive-client-take-over/)

## Resume TLS-Session
Der MQTT-Client unterstützt nicht das Fortsetzen der letzten TLS-Verbindung. Um dies zu ermöglichen sind einige Änderungen um Module notwenig. Wird eine Installation oder Update des Modules durchgeführt, müssen diese Änderungen nachgeholt werden.
- mqtt/lib/connect/tls.js: ca. Zeile 17: Einfügen einer Zeile
```typescript
connection.on('secureConnect', function () {
    // Folgende Zeile einfügen:
    mqttClient.emit('session', connection.getSession())
    if (opts.rejectUnauthorized && !connection.authorized) {
      connection.emit('error', new Error('TLS not authorized'))
    } else {
      connection.removeListener('error', handleTLSerrors)
    }
  })
  ```
  - mqtt/types/lib/client-options.d.ts: Eigenschaft _session_ im Interface _ISecureClientOptions_ hinzufügen
  ```typescript
export interface ISecureClientOptions {
  //...
  rejectUnauthorized?: boolean
  // --> Zeile hinzufügen
  session?: Buffer
}
  ```
- mqtt-driver: TLS-Session wird mit mqtt-Ereignis session übertragen, dieses zwischenspeichern und als Option beim Herstellen der nächsten Verbindung übergeben.
```typescript
    // Neue Eigenschaft hinzufügen
    private tlsSession?: Buffer;

    public connect() {
        return new Promise<void>((resolve, reject) => {
            // ...
            const connOptions: IClientOptions = {
                will: {
                    topic: this.getTopic('srv',
                    // ...
                },
                clientId: set.clientId,
                // ...
                // Zwischengespeicherte Session-ID übernehmen
                session: this.tlsSession
            };
            this.client.on('connect', () => {
                // ...
            });
            this.client.on('message', (topic, message) => {
                // ...
            });
            // Event `session` hinzufügen
            this.client.on('session', (buffer) => {
                Logger.info('Received session ticket');
                this.tlsSession = buffer; 
            });


        });
    }
```

## MQTT-Topic
Built-up:\
iot1/[device-group-id]/[device-id]/[dev-or-srv]/[schema-group]/[data-type]/(var-name)/(zip)

[]=required\
()=optional

- **iot1**: IoT-Interface Version 1
- **device-group-id**: Alpha-numeric identifier of the device-group this device belongs too.
- **device-id**: Alpha-numeric identifier of this device
- **dev-or-srv**: Direction of communication (to device or to server\
    Possible values are:
    - *dev*: Communication to device (device is subscriber, server publisher)
    - *srv*: Communication to server (server is subscriber, device publischer)
- **schema-group**: The schema-group the variable(s) (in data or topic) belongs too
    - *data*: Value from the iot-client to the server
    - *notify*: Binary messages from the client to the server (alarm, warning,...)
    - *config*: Configuration-value from the server to the client. The client returns the value read from the device after request from server.
    - *methods*: Command from the server to the device. If the iot-client forwarded the command to the device successfully, the iot-client returns the value send to the device. In case of error, value null will be returned instead.
- **data-type**: Identifier how the data is presented.
    - *obj*: Data of the mqtt-message contains the value of a variable as object from type *RuntimeVariable(runtime-var)*. The following topic contains the name of the variable.
    - *arr*: Data of the mqtt-message contains a list of variables as object from type *RuntimeVariable(runtime-var)*. The variable-names are part of the data and not part of the following topic.
- **var-name**(option): Name of the variable. Only required if previous topic is *obj*.
- **zip**(option): The data of that message is compressed as gzip. This topic-item must be placed at the last position. The MQTT-Client extracts the data and removes the last topic from the entire topic.


# Verhalten und Aufbau der Daten
Variablen sind einer Schema-Gruppe zugeordnet. Die Zuordnung zu einer bestimmten Gruppe bestimmt das Verhalten dieser Variable.
Mögliche Schema-Gruppen sind:
- data: Messwerte/Analoge Werte
- notify: Meldungen
- config: Parameter
- methods: Befehle

Die Zuordnung zu einer dieser Gruppen und die Kommunikationsrichtung (zum Gerät/vom Gerät) bestimmen den Aufbau der Nutzdaten (payload) und die Qualität der Übertragung. Im folgenden Kapitel und im Experiment wird nur die Verbindungsqualität zwischen IoT-Client und dem serverseitigen Pendant beschrieben (bei Verwendung von MQTT handelt es sich hierbei um den MQTT-Broker).


## Schema-Gruppe 'data'
Hierbei handelt es sich meist um Analogwerte, welche vom IoT-Client an den Server übertragen werden. Wird eine Änderung des Wertes oder des Statuses am IoT-Client erkannt, wird der Wert mit zusätzlichen Informationen sofort oder später, als Paket mit weiteren aufgezeichneten Werte, an den Server übertragen.

### Nutzdaten
Folgende Informationen werden für jeden Wert vom Client an den Server übertragen:
- **Name (name):** Name der Variable
- **Zeitstempel (ts):** Zeitpunkt der Wert bzw. Statusänderung gemäß UTC0. Kleinste Zeiteinheit ist sec. In welchem Format (string oder integer) dieser übertragen wird, ist abhängig vom Aufbau der Nutzdaten. Falls jedoch nicht eigens angeführt, wird dieser als im Unix-Format (Anzahl sec seit 01.01.1970) als 32-bit Integer übertragen.
- **Status (state):** Binäre Information (0/false oder 1/true), ob der Wert fehlerfrei gelesen werden konnte. Ist der WErte 0, ist eine Fehler während dem Lesen aufgetreten. War ein Lesen des Wertes ohne Problem möglich, ist dieser Wert 1.
- **Wert (value):** Wert dieser Variable. Der Datentyp dieser Eigenschaft kann variieren und wird über das Schema bzw. Trigger-Datei definiert. Mögliche Datentypen sind:
  - 16-bit Integer mit Vorzeichen (short)
  - 32-bit Integer mit Vorzeichen (int)
  - 32-bit Gleitkommazahl (float) (default Datentyp)


### Quality of Service

Folgende Qualitätsmerkmale werden für Variablen der Gruppe *data* für MQTT verwendet.

Richtung |QoS   |Retained|
---------|------|--------|----------
subscribe| n/a  | n/a    |
publish  | 1    | none   |



## Schema-Gruppe 'notify'
Hierbei handelt es sich meist um binäre Werte, welche vom IoT-Client an den Server übertragen werden. Wird eine Änderung des Wertes oder des Statuses am IoT-Client erkannt, wird der Wert mit zusätzlichen Informationen sofort an den Server gesendet.

### Nutzdaten
Folgende Informationen werden für jeden Wert vom Client an den Server übertragen:
- **Name (name):** Name der Variable
- **Zeitstempel (ts):** Zeitpunkt der Wert bzw. Statusänderung gemäß UTC0. Kleinste Zeiteinheit ist sec. In welchem Format (string oder integer) dieser übertragen wird, ist abhängig vom Aufbau der Nutzdaten. Falls jedoch nicht eigens angeführt, wird dieser als im Unix-Format (Anzahl sec seit 01.01.1970) als 32-bit Integer übertragen.
- **Status (state):** Binäre Information (0/false oder 1/true), ob der Wert fehlerfrei gelesen werden konnte. Ist der Wert 0, ist ein Fehler während dem Lesen aufgetreten. War ein Lesen des Wertes ohne Problem möglich, ist dieser Wert 1.
- **Wert (value):** Wert dieser Variable. Dieser kann entweder 0 (für false) oder 1 (true) sein. Der Wert wird als Ganzzahl (integer) übertragen.


### Quality of Service
Folgende Qualitätsmerkmale werden für Variablen der Gruppe *notify* für MQTT verwendet.

Richtung |QoS   |Retained|
---------|------|--------|----------
subscribe| n/a  | n/a    |
publish  | 1    | none   |




## Schema-Gruppe 'config'
Hierbei handelt es sich meist um Einstellwerte, welche vom IoT-Server nach Eingabe durch den Benutzer an den IoT-Client übertragen werden. Empfängt der IoT-Client eine 'config'-Variable, retourniert dieser diese.

### Nutzdaten vom IoT-Server and IoT-Client
Ändert der Benutzer einen Einstellwert, wird dieser vom IoT-Server an den IoT-Client übertragen.
- **Name (name):** Name der Variable

- **Wert (value):** Neuer Wert für diese Variable. Der Datentyp dieser Eigenschaft kann variieren und wird über das Schema bzw. Trigger-Datei definiert. Mögliche Datentypen sind:
  - 16-bit Integer mit Vorzeichen (short)
  - 32-bit Integer mit Vorzeichen (int)
  - 32-bit Gleitkommazahl (float) (default Datentyp)

- **Nachricht-ID (msgId):** Eine 8-stellige alphanumerischer, mit Hilfe eines Zufallsgenerators erzeugter Wert, welcher an den IoT-Client gesendet wird. Diese dient für die Identifikation der Antwort vom Client.
Am Server wird diese *msgId* mit zusätzlichen Informationen (z.B. ID des Benutzers) gespeichert. Empfängt der Server eine Nachricht mit dieser *msgId*, kann diese den zusätzlichen Informationen zugeordnet werden und mit diesen weiterverarbeitet werden.

### Nutzdaten vom IoT-Client and IoT-Server
Folgende Informationen werden für jeden Wert vom Client an den Server übertragen:
- **Name (name):** Name der Variable
- **Zeitstempel (ts):** Zeitpunkt der Wert bzw. Statusänderung gemäß UTC0. Kleinste Zeiteinheit ist sec. In welchem Format (string oder integer) dieser übertragen wird, ist abhängig vom Aufbau der Nutzdaten. Falls jedoch nicht eigens angeführt, wird dieser als im Unix-Format (Anzahl sec seit 01.01.1970) als 32-bit Integer übertragen.
- **Status (state):** Binäre Information (0/false oder 1/true), ob der Wert fehlerfrei gelesen werden konnte. Ist der Wert 0, ist ein Fehler während dem Lesen aufgetreten. War ein Lesen des Wertes ohne Problem möglich, ist dieser Wert 1.
- **Wert (value):** Wert dieser Variable. Im Testaufbau der Wert, welcher empfangen wurde. Im realen der Wert, welcher nach dem Schreiben auf das Gerät vom IoT-Client wieder gelesen wurde. Diese besitzt den gleichen Datentypen wie der empfangene Wert.

- **Nachricht-ID (msgId):** Die vom IoT-Server an den IoT-Client gesendet *msgId*.


### Quality of Service
Folgende Qualitätsmerkmale werden für Variablen der Gruppe *config* für MQTT verwendet.

Richtung |QoS   |Retained|
---------|------|--------|----------
subscribe| 2    | none   |
publish  | 1    | none   |



## Schema-Gruppe 'methods'
Hierbei handelt es sich meist um Befehle, welche vom IoT-Server nach Betätigung eines Tasters (oder ähnliches) durch einen Benutzer an den IoT-Client übertragen werden. Empfängt der IoT-Client eine 'methods'-Variable, retourniert er diese in abgeänderter Form.

### Nutzdaten vom IoT-Server and IoT-Client
Betätigt der Benutzer ein Steuerelement (Button), wird dieser vom IoT-Server an den IoT-Client übertragen.
- **Name (name):** Name der Variable
- **Wert (value):** Wert dieser Variable. Dieser kann entweder 0 (für false) oder 1 (true) sein. Der Wert wird als Ganzzahl (integer) übertragen.
- **Nachricht-ID (msgId):** Eine 8-stellige alphanumerischer, mit Hilfe eines Zufallsgenerators erzeugter Wert, welcher an den IoT-Client gesendet wird. Diese dient für die Identifikation der Antwort vom Client.
Am Server wird diese *msgId* mit zusätzlichen Informationen (z.B. ID des Benutzers) gespeichert. Empfängt der Server eine Nachricht mit dieser *msgId*, kann diese den zusätzlichen Informationen zugeordnet werden und mit diesen weiterverarbeitet werden.

### Nutzdaten vom IoT-Client and IoT-Server
Folgende Informationen werden für jeden Wert vom Client an den Server übertragen:
- **Name (name):** Name der Variable
- **Zeitstempel (ts):** Zeitpunkt der Wert bzw. Statusänderung gemäß UTC0. Kleinste Zeiteinheit ist sec. In welchem Format (string oder integer) dieser übertragen wird, ist abhängig vom Aufbau der Nutzdaten. Falls jedoch nicht eigens angeführt, wird dieser als im Unix-Format (Anzahl sec seit 01.01.1970) als 32-bit Integer übertragen.
- **Status (state):** Binäre Information (0/false oder 1/true), ob der Wert fehlerfrei geschrieben werden konnte. Ist der Wert 0, ist ein Fehler während dem Schreiben aufgetreten. War ein Lesen des Wertes ohne Problem möglich, ist dieser Wert 1.
- **Wert (value):** Der empfangene Wert wird an den Server retourniert.
- **Nachricht-ID (msgId):** Die vom IoT-Server an den IoT-Client gesendet *msgId*.


### Quality of Service
Folgende Qualitätsmerkmale werden für Variablen der Gruppe *methods* für MQTT verwendet.

Richtung |QoS   |Retained|
---------|------|--------|----------
subscribe| 2    | none   |
publish  | 1    | none   |


## Verbindungsaufbau und -abbruch
Nach Verbindungsaufbau wird eine Variable CONN_STATE (Schema-Gruppe notify) mit Wert 1 vom IoT-Client veröffentlicht.
Zusätzlich wird als 'LastWill'-Message die gleiche Variable CONN_STATE mit Wert 0 angegeben. Erfolgt ein Verbindungsabbruch, wird diese durch den MQTT-Broker veröffentlicht.

Für 'CONN_STATE' wird die default-Einstellung für 'publish' verwendet.

Richtung |QoS   |Retained|
---------|------|--------|----------
subscribe| n/a  | n/a    |
publish  | 1    | yes    |


# Verschlüsselte Verbindung
Für die verschlüsselte Verbindung werden selbst-gezeichnete Zertifikate verwendet. Diese wurden mittels openSSL erzeugt.
http://www.steves-internet-guide.com/mosquitto-tls/
(Quelle: https://www.akadia.com/services/ssh_test_certificate.html)

## Erzeugen eines Privaten Schlüssels:\
`openssl genrsa -des3 -out server.key 2048`\
Erzeugt eine Datei *server.key*, welche den privaten wie auch den öffentlichen Schlüssel mit einer Bitlänge von 2048-Bits beinhaltet.


## Erzeugen einer Zertifikatanforderungsdatei (CSR-File)\
`openssl req -new -key server.key -out server.csr`\
Die *Certificate Signing Request*-Datei (server.csr) kann für das Anfordern einer Zertifizierung bei einer öffentlichen Authentifizierungsstelle verwendet werden.

## Entfernen der Passwort-Phrase:\
`cp server.key server.key.org`\
`openssl rsa -in server.key.org -out server.key`


## Erzeugen eines SelfSigned-Server-Zertifikates:\
`openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt`\
Erzeugt die *server.crt*-Datei. Diese muss mit dem privaten Schlüssel *server.key* am Server hinterlegt werden.

## Erzeugen des öffentlichen Schlüssels
`openssl rsa -in server.key -out client.pem -outform PEM -pubout`\
Extrahiert den *public*-Schlüssel aus *server.key*-Datei. Diese Datei muss auf die Clients verteilt werden.

`openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365`

-x509: Self signed cetificate
-rsa: 


# CoAP over DTLS
Eine Node-Implementierung für DTLS-Server bzw. CoAP-Client gibt es leider derzeit noch nicht. Der Verbindungsaufbau zwischen den Anwendungen erfolgt über Proxy-Dienste. Ein Client-Proxy leitet eingehende unverschlüsselte UDP-Pakete an einen Server-Proxy verschlusselt weiter. Der Server-Proxy leitet die eingehende verschlüsselte Nachrichten auf ein locales UDP-Port unverschlüsselt weiter.

## Ports
Im Testaufbau werden folgende Ports verwendet.
- Port-A **5683**: UDP-Port Applikation: Applikation empfängt unverschlüsselte Daten auf diesem Port
- Port-B **5687**: UDP-Port Proxy: Der Proxy horcht auf diesen Port auf unverschlüsselte Nachrichten von der lokalen Anwendung und leitet die Nachrichten zum Proxy auf Port-C weiter.
- Port-C **5688**: DTLS-Port Proxy: Proxy empfängt verschlüsselte Daten auf diesen Port und leitet diese zur lokalen Anwendung Port-A weiter.


## Node-DTLS-Proxy

Das npm-package **node-dtls-prox** installiert einnen Proxy, welcher in einem Docker-Container läuft.
Details dazu unter https://github.com/m4n3dw0lf/node-dtls-proxy

Achtung! Nur für Testzwecke in dieser Form geeignet.

Zum Installieren des npm-package folgenden Befehl ausführen:
```
[sudo] npm install -g node-dtls-proxy
``` 


Mit folgenden Befehlen können die Proxies, in einem separaten Terminal, gestartet werden.
- Server-Proxy: 
```
$ dtls2udp <DTLS_LISTEN_PORT> <UDP_LISTEN_PORT> <UDP_ENDPOINT_IP> <UDP_ENDPOINT_PORT>
```
Achtung! Aufgrund unbekannter Ursache horcht dieser Proxy im Testaufbau nicht auf eingehende DTLS-Nachrichten. Anstelle dieses Server-Proxy wurde der Goldy-Proxy zusätzlich installiert. Details siehe nächstes Kapitel.
- Client-Proxy: 
```
$ udp2dtls <UDP_LISTEN_PORT> <DTLS2UDP SERVER IP> <DTLS2UDP SERVER PORT>
```


## Goldy
Aufgrund Probleme mit dem *dtls2udp*-Proxy wurde für den Test dieser Prox verwendet.

Informationen zur Installation und Anwendung dieses Proxies unter: https://github.com/ibm-security-innovation/goldy

Achtung! Möglicherweise kommt es beim Kompilieren zu einem Fehler während des Erstellen abhängiger Bibliotheken (Ordner: deps). Hierfür die source-Dateien in der gewünschten Version runterladen und die Befehle aus dem makefile manuell ausführen (extrahieren, kompilieren). 

Vor dem Start des Tests, goldy starten:
```
./goldy -t 3600 -l 192.168.110.134:5688 -b localhost:5683
 -c /home/chris/project/fh/ba/iot-client/cert/dtls/cert.crt
 -k /home/chris/project/fh/ba/iot-client/cert/dtls/cert.key
```


## IOT-Server (MQTT-Broker-VM)
IP-Adresse Server: 
- 192.168.110.134 als _iot-server_ in _/etc/hosts_ konfigurieren
- 192.168.110.133 als _iot-client_ in _/etc/hosts_ konfigurieren

### Server-Proxy starten
Terminal öffnen und goldy-binary ausführen.\
Listen for DTLS on Port 5688 --> forward UDP to 5683
```
./goldy -t 3600 -l iot-server:5688 -b localhost:5683
 -c /home/chris/project/fh/ba/iot-client/cert/dtls/cert.crt
 -k /home/chris/project/fh/ba/iot-client/cert/dtls/cert.key
```
### Client-Proxy starten
Neues Terminal öffnen und ausführen:
```
$ udp2dtls 5687 iot-client 5688
```


## IoT-Client
IP-Adresse Server: 
- 192.168.110.134 als _iot-server_ in _/etc/hosts_ konfigurieren
- 192.168.110.133 als _iot-client_ in _/etc/hosts_ konfigurieren

### Server-Proxy starten
Terminal öffnen und goldy-binary ausführen.\
Listen for DTLS on Port 5688 --> forward UDP to 5683
```
./goldy -t 3600 -l iot-client:5688 -b localhost:5683 -c /home/chris/project/fh/ba-client/cert/dtls/cert.crt -k /home/chris/project/fh/ba-client/cert/dtls/cert.key
```
### Client-Proxy starten
Neues Terminal öffnen und ausführen:
```
$ udp2dtls 5687 iot-server 5688
```

# MQTT-SN
Für die Kommunikation über MQTT-SN wird am IoT-Device ein MQTT-SN-Client verwendet. Dieser baut eine Verbindung zu einem serverseitigen MQTT-SN-Gateway auf. 
Dieses Gateway übersetzt die Daten und sendet diese an den Broker weiter bzw. übernimmt diesen Job auch in anderer Richtung.

Folgende Ports werden verwendet:
- IoT-Device: UDP 1884 (sendet und empfängt)
- IoT-Gateway: UDP 1884 (sendet und empfängt MQTT-SN)

Aktuell gibt es zwei Versionen von Gateways. Eines, welches als eigener Driver ('mqtt-sn-gw') implementiert und über die Settings-Datei des IoT-Test-Projektes konfiguriert werden kann.
Da dieses Gateway jedoch nur 'dtls' als Socket-Verbindung verwendet und 'udp4' nicht implementiert wurde, was wiederum zu Debug-Probleme führen kann, kann auch ein Gateway verwendet werden, welches Stand-Alone, also nicht in node eingebunden wird, läuft.

## IoT-Gateway (MQTT-SN/MQTT-Gateway):
Ein von Tomoaki YAMAGUCHI auf C/C++ basierende Lösung.

Installation und How-to-use siehe:\
https://github.com/eclipse/paho.mqtt-sn.embedded-c/tree/master/MQTTSNGateway


### Testumgebung:
Software wurde in der Testumgebung unter **./MQTT-SN-Gateway** installiert.
Die Konfigurationsdatei **gateway.conf** befindet sich im selben Ordner.
Durch Aufruf von **./MQTT-SNGateway** in diesem Ordner wird das Gateway gestartet.


## MQTT-SN-Packet (npm module)

In der Datei **writeToStream.js:24** müssen nach Installation oder Update folgende Änderungen durchgeführt werden:
```javascript
function streamWriteLength(stream, length) {
  var buffer;
  if (length >= 256) {
    buffer = new Buffer(3);
    buffer.writeUInt8(1, 0);  // Bug: buffer.writeUInt8>>>(0, 1)<<<; 1.argument=value, 2. offset (mqtt-sn: 1=länge > 255)
    buffer.writeUInt16BE(length, 1); // Bug ...UInt16BE>>>(0, length)<<<
  } else {
    buffer = new Buffer([length]);
  }
  stream.write(buffer);
} 
```


# Nutzdaten

Nach dem Serialisieren der Daten wird zu Beginn ein Byte für die Formatierung angehängt. Mit HIlfe dieser kann der Empfänger den Parser für das Wiederherstellen der Daten auswählen.

Die Zuordnung der Byte-Werte zu einem Parser erfolgt durch den Enumerator **EDataPackerBitValue**. Ein Serialisierungs bzw. Deserialiserungsvorgang kann aus mehreren Subprozessen bestehen (zb. JSON --> CBOR --> gzip).

Welche Datapacker verwendet werden sollen, kann über die settings-Datei im Controller unter der Eigenschaft **dataPackers** festgelegt werden. Die Eigenschaft kann eine Liste von string oder Objecten besitzen, welche die gewünschten Datapacker in korrekter Reihenfolge, wie diese ausgeführt werden soll, auflistet.

``` json
"dataPacker" : ["cbor", {"type": "zip", "enable":true}]
```

## Typen von Datapacker
Mögliche Typen von Datapacker:
- **timestamp**: Zeitstempel werden als Nummer interpretiert. Ein absoluter Zeitstempel wird hinzugefügt und die Zeitstempel sind relative zum absoluten Wert (in sec).
- **json**: Datenobjekt wird in ein JSON-Format serialisiert. Standard data-packer. Wird verwendet wenn kein anderer Datapacker konfiguriert ist.
- **cbor**: Datenobjekt wird in das CBOR-Format serialisiert.
- **zip**: Serialisierte Daten (byte-stream) wird mit Hilfe von **zlib** (https://www.zlib.net/) komprimiert.


## JSON-Komprimierung
https://coderwall.com/p/mekopw/jsonc-compress-your-json-data-up-to-80


https://codebeautify.org/jsonminifier


## Ablauf Data-Packer
Im folgenden wird der Ablauf für des Packen der Daten beschrieben. Alle Schritte sind optional. Erfolgt keiner dieser Schritte, werden die Daten im JSON-Format übertragen.

1. Relative Zeitstempel (''): Es wird zu Beginn eine Eigenschaft 'tsa' als absoluter Zeitpunkt hinzugefügt. Alle weiteren Zeitstempel (Key: ts) werden als Differenz in sec vom Absolutwert beschrieben.
2. Key-Reducer: Die Keys werden durch kürzere Keys ersetzt.
3. Formatierung: daten werden in JSON oder CBOR konvertiert.
4. Komprimierung: Daten werden komprimiert.

