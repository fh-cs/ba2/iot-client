import { Logger } from "./common/logger/logger";
import { Settings } from "./common/settings/settings";
import { TestTrigger } from "./trigger/test-trigger";
import { ControllerFactory } from "./controller/controller-factory";
import { DriverFactory } from "./driver/driver-factory";
import { ISettings } from "./common/settings/settings.model";
import { RecorderFactory } from "./recorder/recorder-factory";


class IotTest {

    private static _instance: IotTest;
    public static get Instance() {
        if (!this._instance) {
            this._instance = new IotTest();
        }
        return this._instance;
    }

    public async start() { 

        // Argument - settings
        let settingsFile = 'settings.json';
        if (this.hasArgument('settings', 's')) {
            settingsFile = this.getArgumentValue('settings', 's');
        }
        Settings.loadSettings(settingsFile);
        Logger.init(Settings.getSettings('logger'));
        
        const settings = Settings.getSettings() as ISettings;
        const recorders = settings.recorder.map(r => RecorderFactory.CREATE(r));
        const driver = settings.driver.map(d => DriverFactory.CREATE(d));
        const controllers = settings.controller.map(a => ControllerFactory.CREATE(a));
        controllers.forEach(a => { 
            a.addDriver(driver); 
            a.addRecorder(recorders);
        });
               
        //Load and sort test-trigger, apply controller and start trigger
        const triggersSorted = settings.trigger.sort( (t1, t2) => {
            const st1 = new Date(Date.parse(t1.startTime));
            const st2 = new Date(Date.parse(t2.startTime));
            return st1.getTime() - st2.getTime();
        });

        for (const trigger of triggersSorted) {
            const t = TestTrigger.getService(trigger);
            t.addController(controllers);
            await t.start();
        }

    }


    private getArgumentValue(name: string, shortName: string) {
        const longArg = '--' + name;
        const shortArg = '-' + shortName;
        const i = process.argv.findIndex(a => a.trim() === longArg || a.trim() === shortArg);
        if (i >= 0) {
            if (i < (process.argv.length - 1)) {
                const v = process.argv[i+1];
                if (!v.trim().startsWith('-')) {
                    return v;
                } else {
                    throw new Error('Value after Argument `' + name + '` is missing (cannot start with dash)!');
                }
            } else {
                throw new Error('Value for Argument `' + name + '` is missing (argument-list to short)!');
            }

        } else  {
            // argument not found
            throw new Error('Argument `' + name + '` not found!');
        }
    }

    private hasArgument(name: string, shortName: string) {
        return process.argv.some( a => {
            const cleanArg = a.trim();
            // argument must start with - or --
            if (cleanArg.startsWith('-')) {
                const a2 = cleanArg.slice(1);
                const isLongArg = a2.startsWith('-');
                const onlyNamesArg = cleanArg.replace('-','').trim();
                return onlyNamesArg.includes(isLongArg ? name : shortName);
            } else {
                return false;
            }
        });
    }
  
      

}

// ENTRY-POINT
IotTest.Instance.start()
    .then(() => {
        Logger.info('Finished');
    })
    .catch(error => {
        Logger.error('Stopped with error!', error);
    });