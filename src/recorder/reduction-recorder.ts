export class ReductionRecorder {
    private static _instance: ReductionRecorder;

    public static GET() {
        if (!this._instance) {
            this._instance = new ReductionRecorder();
        }
        return this._instance;
    }
    private records = new Map<string, IReductionResult>();

    public reset(group?: string) {
        if (group) {
            this.records.set(group, {
                group: group,
                totalBefore: 0,
                totalAfter: 0,
                count: 0
            });
        } else {
            this.records.clear();
        }
    }

    public record(group: string, before: number, after: number) {
        const list = this.records.get(group);
        if (list) {
            list.totalBefore += before;
            list.totalAfter += after;
            list.count++;
            if (list.maximumBefore === undefined || before > list.maximumBefore) {
                list.maximumBefore = before;
            }
            if (list.minimumBefore === undefined || before < list.minimumBefore) {
                list.minimumBefore = before;
            }
            // average in bytes
            list.averageReductionAbs = (list.totalBefore - list.totalAfter) / list.count;
            // minimum - maximum in bytes
            const actRedAbs = before - after;
            if (list.minimumReductionAbs === undefined || actRedAbs < list.minimumReductionAbs) {
                list.minimumReductionAbs = actRedAbs;
            }
            if (list.maximumReductionAbs === undefined || actRedAbs > list.maximumReductionAbs) {
                list.maximumReductionAbs = actRedAbs;
            }
            
            list.averageReductionPerc = this.calcReductionPercentage(list.totalBefore, list.totalAfter);

            const actRedPerc = this.calcReductionPercentage(before, after);

            if (list.minimumReductionPerc === undefined || actRedPerc < list.minimumReductionPerc) {
                list.minimumReductionPerc = actRedPerc;
            }
            if (list.maximumReductionPerc === undefined || actRedPerc > list.maximumReductionPerc) {
                list.maximumReductionPerc = actRedPerc;
            }

        } else {
            // create group
            this.reset(group);
            // call me again
            this.record(group, before, after);
        }
    }

    private calcReductionPercentage(before: number, after: number) {
        const red = before - after;
        return (100 * red) / before;
    }



    public getGroups() {
        const groups = new Array<string>();
        const iter = this.records.forEach((v, k, m) => groups.push(k));
        return groups;
    }

    public getGroupsResult(): IReductionResult[] 
    {
        const groups = this.getGroups();
        return groups.map((v) => {
            return this.getResult(v);
        });
    }

    public getResult(group: string): IReductionResult {
        const gv = this.records.get(group);
        if (gv) {
            return gv;
        } else {
            throw new Error('Group ' + group + ' does not exist in records!');
        }
    }

    public static getResultProperties(): IResultPropertyInfo[] {
        return [ 
            { key: "totalBefore",           description: "Total before", unit: "byte"},
            { key: "totalAfter",            description: "Total after", unit: "byte"},
            { key: "minimumBefore",         description: "Minimum before", unit: "byte"},
            { key: "maximumBefore",         description: "Maximum before", unit: "byte"},
            { key: "count",                 description: "Count", unit: ""},
            { key: "averageReductionAbs",   description: "Compressed average", unit: "byte"},
            { key: "minimumReductionAbs",   description: "Compressed minimum", unit: "byte"},
            { key: "maximumReductionAbs",   description: "Compressed maximum", unit: "byte"},
            { key: "averageReductionPerc",  description: "Compress-rate average", unit: "%"},
            { key: "minimumReductionPerc",  description: "Compress-rate minimum", unit: "%"},
            { key: "maximumReductionPerc",  description: "Compress-rate maximum", unit: "%"},
    ]
    }
}

export interface IReductionResult {
    group?: string
    totalBefore: number,
    totalAfter: number,
    minimumBefore?: number,
    maximumBefore?: number,
    count: number,
    averageReductionAbs?: number,
    averageReductionPerc?: number,
    minimumReductionAbs?: number,
    minimumReductionPerc?: number,
    maximumReductionAbs?: number,
    maximumReductionPerc?: number
}

export interface IResultPropertyInfo {
    key: string,
    description: string, 
    unit?: string
}