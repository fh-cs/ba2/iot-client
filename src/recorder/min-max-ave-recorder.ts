import { IResultPropertyInfo } from "./reduction-recorder";

export class MinMaxAveRecorder {
    private static _instance: MinMaxAveRecorder;

    public static GET() {
        if (!this._instance) {
            this._instance = new MinMaxAveRecorder();
        }
        return this._instance;
    }
    private records = new Map<string, IMinMaxResult>();

    public reset(group?: string) {
        if (group) {
            this.records.set(group, {
                group: group,
                total: 0,
                count: 0
            });
        } else {
            this.records.clear();
        }
    }

    public record(group: string, counterValue: number) {
        const list = this.records.get(group);
        if (list) {
            list.total += counterValue;
            list.count++;
            if (list.maximum === undefined || counterValue > list.maximum) {
                list.maximum = counterValue;
            }
            if (list.minimum === undefined || counterValue < list.minimum) {
                list.minimum = counterValue;
            }
            // average in bytes
            list.average = list.total / list.count;

        } else {
            // create group
            this.reset(group);
            // call me again
            this.record(group, counterValue);
        }
    }

    private calcReductionPercentage(before: number, after: number) {
        const red = before - after;
        return (100 * red) / before;
    }



    public getGroups() {
        const groups = new Array<string>();
        const iter = this.records.forEach((v, k, m) => groups.push(k));
        return groups;
    }

    public getGroupsResult(): IMinMaxResult[] 
    {
        const groups = this.getGroups();
        return groups.map((v) => {
            return this.getResult(v);
        });
    }

    public getResult(group: string): IMinMaxResult {
        const gv = this.records.get(group);
        if (gv) {
            return gv;
        } else {
            throw new Error('Group ' + group + ' does not exist in records!');
        }
    }

    public static getResultProperties(): IResultPropertyInfo[] {
        return [ 
            { key: "minimum",           description: "Minimum", unit: "byte"},
            { key: "maximum",            description: "Maximum", unit: "byte"},
            { key: "average",         description: "Durchschnitt", unit: "byte"},
            { key: "total",         description: "Gesamt", unit: "byte"},
            { key: "count",                 description: "Anzahl", unit: ""}
    ]
    }
}

export interface IMinMaxResult {
    group?: string
    minimum?: number,
    maximum?: number,
    average?: number,
    total: number,
    count: number
}
