import { IRecorderSettings } from "./recorder-settings.model";
import { ERecorderType } from "./recorder-type.enum";
import { IWiresharkSettings } from "./wireshark-settings.model";
import { Recorder } from "./recorder";
import { QuantityRecorder } from "./quantity-recorder";
import { IQuantityRecorderSettings } from "./quantity-recorder-settings.model";
import { Wireshark } from "./wireshark";

export class RecorderFactory {
    public static CREATE(settings: IRecorderSettings): Recorder {
        if(!settings) {
            throw new Error('Failed to create recorder, settings are not provided!');
        }
        switch(settings.type) {
            case ERecorderType.WIRESHARK:
                return new Wireshark(settings as IWiresharkSettings);
            case ERecorderType.QUANTITIY_RECORDER:
                return new QuantityRecorder(settings as IQuantityRecorderSettings);
            default:
                throw new Error('Failed to create recorder for test `' + settings.name + '`, recorder-type `' + settings.type + '` is unknown!');
        }
    }
}