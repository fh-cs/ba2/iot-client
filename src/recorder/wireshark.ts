import { Logger } from "../common/logger/logger";
import { IWiresharkSettings } from "./wireshark-settings.model";
import * as childProcess from "child_process";
import { Recorder } from "./recorder";
import { Helper } from "../common/helper";
import { ESchemaGroup } from "../common/schema-group.enum";

/*
Wireshark 2.1.0 (v2.1.0rc0-502-g328fbc0 from master)
Interactively dump and analyze network traffic.
See https://www.wireshark.org for more information.

Usage: wireshark [options] ... [ <infile> ]

Capture interface:
  -i <interface>           name or idx of interface (def: first non-loopback)
  -f <capfilter|predef:>   packet filter in libpcap filter syntax or
                           predef:filtername - predefined filtername from GUI
                           https://linux.die.net/man/7/pcap-filter
  -s <snaplen>             packet snapshot length (def: 262144)
  -p                       don’t capture in promiscuous mode
  -k                       start capturing immediately (def: do nothing)
  -S                       update packet display when new packets are captured
  -l                       turn on automatic scrolling while -S is in use
  -I                       capture in monitor mode, if available
  -B <buffer size>         size of kernel buffer (def: 2MB)
  -y <link type>           link layer type (def: first appropriate)
  --time-stamp-type <type> timestamp method for interface
  -D                       print list of interfaces and exit
  -L                       print list of link-layer types of iface and exit
  --list-time-stamp-types  print list of timestamp types for iface and exit

Capture stop conditions:
  -c <packet count>        stop after n packets (def: infinite)
  -a <autostop cond.> ...  duration:NUM - stop after NUM seconds
                           filesize:NUM - stop this file after NUM KB
                              files:NUM - stop after NUM files
Capture output:
  -b <ringbuffer opt.> ... duration:NUM - switch to next file after NUM secs
                           filesize:NUM - switch to next file after NUM KB
                              files:NUM - ringbuffer: replace after NUM files
RPCAP options:
  -A <user>:<password>     use RPCAP password authentication
Input file:
  -r <infile>              set the filename to read from (no pipes or stdin!)

Processing:
  -R <read filter>         packet filter in Wireshark display filter syntax
  -n                       disable all name resolutions (def: all enabled)
  -N <name resolve flags>  enable specific name resolution(s): "mnNtdv"
  -d <layer_type>==<selector>,<decode_as_protocol> ...
                           "Decode As”, see the man page for details
                           Example: tcp.port==8888,http
  --disable-protocol <proto_name>
                           disable dissection of proto_name
  --enable-heuristic <short_name>
                           enable dissection of heuristic protocol
  --disable-heuristic <short_name>
                           disable dissection of heuristic protocol

User interface:
  -C <config profile>      start with specified configuration profile
  -Y <display filter>      start with the given display filter
  -g <packet number>       go to specified packet number after "-r"
  -J <jump filter>         jump to the first packet matching the (display)
                           filter
  -j                       search backwards for a matching packet after "-J"
  -m <font>                set the font name used for most text
  -t a|ad|d|dd|e|r|u|ud    output format of time stamps (def: r: rel. to first)
  -u s|hms                 output format of seconds (def: s: seconds)
  -X <key>:<value>         eXtension options, see man page for details
  -z <statistics>          show various statistics, see man page for details

Output:
  -w <outfile|->           set the output filename (or '-' for stdout)

Miscellaneous:
  -h                       display this help and exit
  -v                       display version info and exit
  -P <key>:<path>          persconf:path - personal configuration files
                           persdata:path - personal data files
  -o <name>:<value> ...    override preference or recent setting
  -K <keytab>              keytab file to use for kerberos decryption
  */

export class Wireshark extends Recorder {
    

    public get name(): string {
        return this.settings && this.settings.name ? this.settings.name : 'unknown';
    }

    private static _instance?: Wireshark;


    private _settings?: IWiresharkSettings;
    public get settings() {
        if (!this._settings) {
            throw new Error('Wireshark-settings not set!');
        }
        return this._settings;
    }

    public setSettings(settings: IWiresharkSettings) {
        this._settings = settings;
    }
    public getSettings() {
        if (!this._settings) {
            throw new Error('Wireshark-settings not set!');
        }
        return this._settings;
    }

    public static getProvider() {
        if (!this._instance) {
            this._instance = new Wireshark();
        }
        return this._instance;
    }

    public constructor(settings?: IWiresharkSettings) {
        super();
        if (settings) {
            this.setSettings(settings);
        }
    }

    //#region Implementation of ITestRecord
    async prepare(testName: string) {

    }
    async start(testName: string) {
        return new Promise<void>((resolve, reject) => {
            const filePath = this.settings.filePath + '/' + testName + '_' + Helper.getDateTimeForFile() + '_wire.out';
            //
            //const filter = this.settings.filterIpAddress ? " -f 'ip host " + this.settings.filterIpAddress + "'" :"";
            const filter = " -f '(dst host " + this.settings.myIP + " or dst host " + this.settings.serverIP  +  ") and (src host "+ this.settings.myIP + " or src host " + this.settings.serverIP  +")'";
            Logger.debug('Wireshark filter: `' + filter + '`');
            const cmd = 'wireshark -i ' + this.settings.interface + ' -k -w ' + filePath + filter;
            try {
                childProcess.exec(cmd, (err, stdout, stderr) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            } catch (e) {
                const msg = 'Failed to execute start-command: `' + cmd + '`! Error: ' + e.message;
                Logger.error(msg, e);
                reject(new Error(msg));
            }
        });
    }

    async stop(testName: string) {
        return new Promise<void>(async (resolve, reject) => {
            const pid = await this.getPID('wireshark');            
            const cmd = 'kill -s TERM ' + pid;
            try {
                childProcess.exec(cmd, (err, stdout, stderr) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            } catch (e) {
                const msg = 'Failed to execute stop-command: `' + cmd + '`! Error: ' + e.message;
                Logger.error(msg, e);
                reject(new Error(msg));
            }
        });

    }

    private async getPID(processName: string) {
        return new Promise<string>( (resolve, reject) => {
            //const cmd = 'pkill wireshark';
            const cmd = 'pidof ' + processName;
            
            try {
                childProcess.exec(cmd, (err, stdout, stderr) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(stdout);
                    }
                });
            } catch (e) {
                const msg = 'Failed to execute pid-command: `' + cmd + '`! Error: ' + e.message;
                Logger.error(msg, e);
                reject(new Error(msg));
            }
        });
    }

    recordDataSendOrigin(testName: string, variable: import("../trigger/test-event.model").ITestVar): void {
        //keep unused
    }
    recordDataReceivedOrigin(testName: string, variable: import("../trigger/test-event.model").ITestVar): void {
        //keep unused
    }
    public recordDataSendDriver(testName: string, schemaGroup: ESchemaGroup, byteLength: number): void {
        //keep unused
    }
    public recordDataReceivedDriver(testName: string,schemaGroup: ESchemaGroup, byteLength: number): void {
        //keep unused
    }
    //#endregion ITestRecord

}