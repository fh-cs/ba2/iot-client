import { Recorder } from "./recorder";
import { IQuantityRecorderSettings } from "./quantity-recorder-settings.model";
import * as fs from "fs";
import { Helper } from "../common/helper";
import { ESchemaGroup } from "../common/schema-group.enum";
import { Logger } from "../common/logger/logger";
import { ReductionRecorder } from "./reduction-recorder";
import { MinMaxAveRecorder } from "./min-max-ave-recorder";
import { XY_Recorder } from "./x-y-recorder";

export class QuantityRecorder extends Recorder {

    public static readonly DELIMITER = ';';
    private _settings?: IQuantityRecorderSettings;
    private startTime?: Date;
    private endTime?: Date;

    constructor(settings?: IQuantityRecorderSettings) {
        super();
        if (settings) {
            this.setSettings(settings);
        }
    }
    public setSettings(settings: IQuantityRecorderSettings): void {
        this._settings = settings;
    }
    public getSettings(): IQuantityRecorderSettings {
        if (!this._settings) {
            throw new Error('Quantity-Recorder: Unable to return settings, settings not set!');
        }
        return this._settings;
    }
    public get name(): string {
        if (!this._settings) {
            throw new Error('Quantity-Recorder: Unable to return name, settings not set!');
        }
        if (!this._settings.name) {
            throw new Error('Quantity-Recorder: Unable to return name, `name` not defined in settings!');
        }
        return this._settings.name;
    }
    public async prepare(testName: string): Promise<void> {
        TestCounterFactory.getForTest(testName).resetAll();
        Logger.cleanBuffer();
    }
    public async start(testName: string): Promise<void> {
        this.startTime = new Date();
    }

    public async stop(testName: string): Promise<void> {
        this.endTime = new Date();
        const settings = this.getSettings();
        const filePathBase = settings.filePath + '/' + testName + '_' + Helper.getDateTimeForFile();
        const filePath = filePathBase + '_rec.out.csv';
        const fileContent = this.getFileContent(testName);
        await Logger.exportBuffer(filePathBase + '.logs.csv');
        return new Promise<void>((resolve, reject) => {
            fs.writeFile(filePath, fileContent, {}, (err) => {
                if (err) {
                    reject(new Error('Failed to write to file ' + filePath + '! Error: ' + err.message));
                } else {
                    resolve();
                }
            });
        });
    }
    public recordDataSendOrigin(testName: string, variable: import("../trigger/test-event.model").ITestVar): void {
        TestCounterFactory.getForTest(testName).getForUsage('sent-origin').countUp(variable.schemaGroup);
    }
    public recordDataReceivedOrigin(testName: string, variable: import("../trigger/test-event.model").ITestVar): void {
        TestCounterFactory.getForTest(testName).getForUsage('rec-origin').countUp(variable.schemaGroup);
    }

    public recordDataSendDriver(testName: string, schemaGroup: ESchemaGroup, byteLength: number): void {
        TestCounterFactory.getForTest(testName).getForUsage('sent-driver').countUp(schemaGroup, byteLength);

    }
    public recordDataReceivedDriver(testName: string, schemaGroup: ESchemaGroup, byteLength: number): void {
        TestCounterFactory.getForTest(testName).getForUsage('rec-driver').countUp(schemaGroup, byteLength);
    }

    private getFileContent(testname: string) {
        let content = '';
        content += this.getFileRow('Test-name', testname);
        content += this.getFileRow('Start at', this.startTime ? this.startTime : 'Error: start-time not set!(maybe start not called)');
        content += this.getFileRow('Ended at', this.endTime ? this.endTime : 'Error: end-time not set!');

        content = this.getFileRowsForCounter(content, TestCounterFactory.getForTest(testname).getForUsage('sent-origin'), 'Variables schema-group $GROUP sent');
        content = this.getFileRowsForCounter(content, TestCounterFactory.getForTest(testname).getForUsage('rec-origin'), 'Variables schema-group $GROUP received');
        content = this.getFileRowsForCounter(content, TestCounterFactory.getForTest(testname).getForUsage('sent-driver'), 'Payload [Bytes] schema-group $GROUP sent');
        content = this.getFileRowsForCounter(content, TestCounterFactory.getForTest(testname).getForUsage('rec-driver'), 'Payload [Bytes]  schema-group $GROUP received');
        content += '\n';
        // Insert content reduction counter
        content += this.printReductionHeader();
        ReductionRecorder.GET().getGroups().forEach(v => {
            content += this.printReductionGroup(v);
        });
        content += '\n';
        // Insert content min-max-ave counter
        content += this.printMinMaxAveHeader();
        MinMaxAveRecorder.GET().getGroups().forEach(v => {
            content += this.printMinMaxAveGroup(v);
        });
        content += '\n';
        // Print X-Y-Values
        content += XY_Recorder.print(QuantityRecorder.DELIMITER);
        return content;
    }

    private printReductionHeader() {
        return 'Reduction datapacker' + QuantityRecorder.DELIMITER +
            ReductionRecorder.getResultProperties().map((v) => v.description + (v.unit ? '[' + v.unit + ']' : '')).join(QuantityRecorder.DELIMITER) + 
        '\n';
    }
    private printReductionGroup(group: string) {
        const res = ReductionRecorder.GET().getResult(group);
        return group + QuantityRecorder.DELIMITER + ReductionRecorder.getResultProperties().map((v) => {
            if (res[v.key] !== undefined) {
                return new String(res[v.key]);
            } else {
                return '';
            }
        }).join(QuantityRecorder.DELIMITER) + '\n';
    }

    private printMinMaxAveHeader() {
        return 'Reduction datapacker' + QuantityRecorder.DELIMITER +
            MinMaxAveRecorder.getResultProperties().map((v) => v.description + (v.unit ? '[' + v.unit + ']' : '')).join(QuantityRecorder.DELIMITER) + 
        '\n';
    }
    private printMinMaxAveGroup(group: string) {
        const res = MinMaxAveRecorder.GET().getResult(group);
        return group + QuantityRecorder.DELIMITER + MinMaxAveRecorder.getResultProperties().map((v) => {
            if (res[v.key] !== undefined) {
                return new String(res[v.key]);
            } else {
                return '';
            }
        }).join(QuantityRecorder.DELIMITER) + '\n';
    }

    private getFileRow(argument: string, value: number | string | Date) {
        return argument + QuantityRecorder.DELIMITER + value + QuantityRecorder.DELIMITER + '\n';
    }


    private getFileRowsForCounter(contentIn: string, counter: GroupCounter, remark: string) {
        let content = this.getFileRow(remark.replace('$GROUP', 'TOTAL'), counter.getTotal());
        for (let k of counter.getGroups()) {
            content += this.getFileRow(remark.replace('$GROUP', k), counter.getCounter(k));
        }
        return contentIn += content;
    }



}


class TestCounterFactory {
    private static testUsages = new Map<string, UsageCounter>();

    public static getForTest(testname: string) {
        const cExists = this.testUsages.get(testname);
        if (cExists) {
            return cExists;
        } else {
            const c = new UsageCounter();
            this.testUsages.set(testname, c);
            return c;
        }
    }
}

class UsageCounter {
    private usages = new Map<string, GroupCounter>();

    public getForUsage(usage: string) {
        const cExists = this.usages.get(usage);
        if (cExists) {
            return cExists;
        } else {
            const c = new GroupCounter();
            this.usages.set(usage, c);
            return c;
        }
    }

    public resetAll() {
        this.usages.forEach((v, k) => v.reset());
    }
}


class GroupCounter {

    /**
     * A object-list of counters.
     * The property-key typifies the group, properties are from type number.
     */
    private counters: any;

    constructor() {
        this.counters = {};
    }
    /**
    * Resets the schema-group counter
    */
    public reset(group?: string) {
        if (!group) {
            // empty counter of all groups
            this.counters = {};
        } else {
            if (this.counters[group]) {
                this.counters[group] = 0;
            }
        }
    }
    /**
     * Increases the counter for the specific schema-group by the given byteLength or 1 (if undefined).
     */
    public countUp(group: string, byteLength: number = 1) {
        if (!this.counters) {
            this.counters = {};
        }
        if (!this.counters[group]) {
            this.counters[group] = 0;
        }
        if (byteLength) {
            this.counters[group] += byteLength;
        }
    }
    /**
     * Returns the current value of the counter for the specific schema-group.
     */
    public getCounter(group: string) {
        if (!this.counters) {
            this.counters = {};
        }
        if (!this.counters[group]) {
            this.counters[group] = 0;
        }
        return this.counters[group];
    }

    public getTotal() {
        if (this.counters) {
            let c = 0;
            Object.keys(this.counters).forEach(k => c += this.counters[k]);
            return c;
        }
        return 0;
    }

    public getGroups() {
        if (!this.counters) {
            return new Array<string>();
        } else {
            return Object.keys(this.counters);
        }
    }
}