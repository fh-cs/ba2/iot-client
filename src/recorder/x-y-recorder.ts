import { IResultPropertyInfo } from "./reduction-recorder";


export class XY_Recorder {
    private static _instance: XY_Recorder;

    public static GET() {
        if (!this._instance) {
            this._instance = new XY_Recorder();
        }
        return this._instance;
    }
    private records = new Map<string, IXY_Result>();

    public reset(group?: string) {
        if (group) {
            this.records.set(group, {
                group: group,
                records: new Array<IXY_ResultRecord>()
            });
        } else {
            this.records.clear();
        }
    }

    public record(group: string, yValue: number, xValue?: number, yUnit?: string, xUnit?: string) {
        const list = this.records.get(group);
        if (list) {
            if (!list.records) {
                list.records = new Array<IXY_ResultRecord>();
            }
            if (xValue === undefined) {
                xValue = list.records.length + 1;
            }
            list.records.push({ xValue: xValue, yValue: yValue, xUnit: xUnit, yUnit: yUnit });

        } else {
            // create group
            this.reset(group);
            // call me again
            this.record(group, yValue, xValue);
        }
    }




    public getGroups() {
        const groups = new Array<string>();
        const iter = this.records.forEach((v, k, m) => groups.push(k));
        return groups;
    }

    public getGroupsResult(): IXY_Result[] {
        const groups = this.getGroups();
        return groups.map((v) => {
            return this.getResult(v);
        });
    }

    public getResult(group: string): IXY_Result {
        const gv = this.records.get(group);
        if (gv) {
            return gv;
        } else {
            throw new Error('Group ' + group + ' does not exist in records!');
        }
    }

    public static getResultProperties(): IResultPropertyInfo[] {
        return [
            { key: "xValue", description: "X-Wert" },
            { key: "xUnit", description: "X Einheit" },
            { key: "yValue", description: "Y-Wert" },
            { key: "yUnit", description: "Y Einheit" }
        ]
    }

    public static print(delimiter = ';') {
        let content = this.printHeader();
        const groups = this.GET().getGroups();
        content += groups.map(g => this.printGroup(g)).join(delimiter);
        return content;
    }
    public static printHeader(delimiter = ';') {
        return 'X-Y-Werte' + delimiter + 
            XY_Recorder.getResultProperties().map((v) => v.description + (v.unit ? '[' + v.unit + ']' : '')).join(delimiter) + '\n';
    }
    public static printGroup(group: string, delimiter = ';') {
        const res = XY_Recorder.GET().getResult(group);
        const props = XY_Recorder.getResultProperties();
        if (res.records) {
            // print each record
            return res.records.map(r => {
                // print group and property values (one record = one row)
                return group + delimiter + props.map(v => {
                    // print each property value (if existing, otherwise an empty cell)
                    return r[v.key] !== undefined ? r[v.key] : '';
                }).join(delimiter) + '\n';
            }).join() + '\n';
        }
        return '\n';
    }
}

export interface IXY_Result {
    group?: string;
    records?: IXY_ResultRecord[];
}

export interface IXY_ResultRecord {
    xValue: number;
    xUnit?: string;
    yValue: number;
    yUnit?: string;
}
