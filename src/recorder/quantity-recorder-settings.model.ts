import { IRecorderSettings } from "./recorder-settings.model";

export interface IQuantityRecorderSettings extends IRecorderSettings {
    /**
     * Filepath to the directory of the output-file
     */
    filePath: string;
}