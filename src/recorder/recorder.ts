import { ITestRecorder } from "../controller/controller";
import { IRecorderSettings } from "./recorder-settings.model";
import { ESchemaGroup } from "../common/schema-group.enum";

export abstract class Recorder implements ITestRecorder {
    
    
    
    public abstract setSettings(settings: IRecorderSettings): void;
    public abstract getSettings(): IRecorderSettings;

//#region Implemenation of ITestRecorder
    public abstract name: string;    
    public belongsToController(controllerName: string): boolean {
        const settings = this.getSettings();
        if (!settings) {
            throw new Error('Unable to compare for controller-names, settings are missing!');
        }
        if (!settings.controller) {
            throw new Error('Unable to compare for controller-names, controllerNames are missing in settings!');
        }
        if (typeof settings.controller === 'string') {
            return settings.controller === controllerName;
        } else {
            return settings.controller.some(a =>  a === controllerName);
        }
    }
    public abstract prepare(testName: string): Promise<void>;
    
    public abstract start(testName: string): Promise<void>;

    public abstract stop(testName: string): Promise<void>;

    public abstract recordDataSendOrigin(testName: string, variable: import("../trigger/test-event.model").ITestVar): void;
    
    public abstract recordDataReceivedOrigin(testName: string, variable: import("../trigger/test-event.model").ITestVar): void ;

    public abstract recordDataSendDriver(testName: string, schemaGroup: ESchemaGroup, byteLength: number): void;
    public abstract recordDataReceivedDriver(testName: string, schemaGroup: ESchemaGroup, byteLength: number): void;
    //#endregion ITestRecorder
}