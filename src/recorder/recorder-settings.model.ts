import { ERecorderType } from "../recorder/recorder-type.enum";

export interface IRecorderSettings {
    /**
     * Name of this recorder used for identification and as reference
     */
    name: string;
    /**
     * Name or names of controllers this recorder belongs too.
     */
    controller: string | string[];
    /**
     * Type of the recorder to be used
     */
    type: ERecorderType;
}