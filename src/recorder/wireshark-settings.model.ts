import { IRecorderSettings } from "./recorder-settings.model";

export interface IWiresharkSettings extends IRecorderSettings {
    filePath: string;
    interface: string;
    serverIP: string;
    myIP: string;
}