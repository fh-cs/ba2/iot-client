import { ILoggerSettings, ELogLevel } from "./logger-settings.model";
import { Helper } from "../helper";
import * as fs from "fs";


export class Logger {
    
    private static _service: Logger;
    private _settings?: ILoggerSettings;

    public get settings() {
        return this._settings;
    }

    public init(settings?: ILoggerSettings) {
        if (!settings) {
            this._settings = {
                loglevel: ELogLevel.INFO
            }
        } else {
            this._settings = settings;
        }
    }

    public static getService() {
        if (!this._service) {
            this._service = new Logger();
        }
        return this._service;
    }
    
    public static init(settings?: ILoggerSettings) {
       this.getService().init(settings);
    }


    public static debug(message: string, ... optionalParams: any) {
        // 37=white
        this.log('37', message, optionalParams);
    }

    public static info(message: string, ... optionalParams: any) {
        //34=blue
        this.log('34', message, optionalParams);
    }

    public static warn(message: string, ... optionalParams: any) {
        //33=yellow
        this.log('33', message, optionalParams);
    }

    
    private static log(colorCode: string, message: string, ... optionalParams: any) {
        const myTime = Helper.getTime();
        const location = this.getLocation();
        const msgType = colorCode === '33' ? 'warn' : ( colorCode === '34' ? 'info' : 'debug' );
        const msg = myTime + '|' + location + message;
        if (optionalParams) {
            console.log('\x1b['+ colorCode + 'm%s\x1b[0m', msg, optionalParams);
            try{
                const strOptions = JSON.stringify(optionalParams);
                this.addMessageToBuffer(myTime, location, msgType, message, strOptions);
            } catch(err) {
                this.addMessageToBuffer(myTime, location, msgType, message);
            }
        } else {
            console.log('\x1b[33m%s\x1b[0m', message);
            this.addMessageToBuffer(myTime, location, msgType, message);
        }
    }

    public static error(message: string | Error, ... optionalParams: any) {
        const location = this.getLocation();
        const myTime = Helper.getTime();
        let msg = "";
        if (typeof message === 'string') {
            msg += message;
            
        } else if (typeof message === 'object') {
            msg += message.message;
        }
        const consolemsg = myTime + '|' + location + msg;

        if (optionalParams) {
            console.error('\x1b[31m%s\x1b[0m', msg, optionalParams);
            
            try{
                const strOptions = JSON.stringify(optionalParams);
                this.addMessageToBuffer(myTime, location, 'error', msg, strOptions);
            } catch(err) {
                this.addMessageToBuffer(myTime, location, 'error', msg);
            }
            
        } else {
            console.error('\x1b[31m%s\x1b[0m', msg);
            this.addMessageToBuffer(myTime, location, 'error', msg);
        }
    }
    

    private static messages = new Array<{ts: string, location: string, messageType: string, message: string, optionalMessage?:string}>();

    public static cleanBuffer() {
        this.messages.splice(0, this.messages.length);
    }

    public static exportBuffer(filepath:string, cleanBuffer = true) {
        const delimiter = ';';
        const msgs = this.messages.map( m => m.ts + delimiter + m.location + delimiter + m.messageType + delimiter + m.message + delimiter + (m.optionalMessage ? m.optionalMessage  : '') + delimiter);
        const content = msgs.join('\n');
        if (cleanBuffer) {
            this.cleanBuffer();
        }
        return new Promise<void>( (resolve, reject) => {
            fs.writeFile(filepath, content, {}, (err) => {
                if (err) {
                    reject(new Error('Failed to write logs to file ' + filepath + '! Error: ' + err.message));
                } else {
                    resolve();
                }
            });
        });

        
    }

    public static addMessageToBuffer(ts: string, location: string, messageType: string, message: string, optionalMessage?:string) {
        this.messages.push({ts:ts, location: location, messageType: messageType, message: message, optionalMessage: optionalMessage})
    }

    private static getLocation() {
        var stack = new Error().stack;
        if (stack) {
            try {
                const stackRows = stack.split('\n');
                if (stackRows.length > 1) {
                    return this.getFirstStackOutside(stackRows.splice(1));
                }
            } catch(e) {
            }
        }
        return '';
    }

    private static getFirstStackOutside(stacks: string[]): string {
        if (stacks && stacks.length > 0) {
            // at /ba-client/dist/service/test-trigger.js:199:40 --> test-trigger.js:199.40
            const dirs = stacks[0].split('/');
            if (dirs.length > 0) {
                const loc = dirs[dirs.length - 1].replace(')','').trim();
                if (loc.includes('logger')) {
                    return this.getFirstStackOutside(stacks.slice(1));
                } else {
                    // my-file.js:123:34 --> my-file.js
                    const parts = loc.split(':'); 
                    return (parts.length > 0 ? parts[0] : loc) + ': ';
                }
            }
        }
        return '';
    }

    
}
