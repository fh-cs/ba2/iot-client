export class Helper {
    public static getDateTimeForFile() {
        const dt = new Date();
        let strDt = '';
        strDt += this.fillUp(dt.getFullYear(), 4);
        strDt += this.fillUp(dt.getMonth() + 1, 2);
        strDt += this.fillUp(dt.getDate(), 2);
        strDt += '_';
        strDt += this.fillUp(dt.getHours(), 2);
        strDt += this.fillUp(dt.getMinutes(), 2);
        return strDt;

    }
    
    public static getTime() {
        const date = new Date();
        return  this.fillUp(date.getHours(), 2) + ':' + 
                this.fillUp(date.getMinutes(), 2) + ':' + 
                this.fillUp(date.getSeconds(), 2 );
    }
   

    public static fillUp(numberToFill: number, size: number) {
        const nLength = Math.trunc(numberToFill).toString().length;
        const zeros = size - nLength;
        let pref = '';
        for (let i = 0; i < zeros; i++) {
            pref += '0';
        }
        return pref + Math.trunc(numberToFill).toString();
    }
}