export enum ESchemaGroup {
    DATA = 'data',
    NOTIFY = 'notify',
    CONFIG = 'config',
    METHOD = 'method'
}