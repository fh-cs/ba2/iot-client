import * as fs from 'fs';


export class ReferenceList<T> {

    public static readonly DELIMITER = ';';
    public static readonly COMMENT = '#';

    private static references = new Map<string, Map<string, string>>();

    public static loadReferences(path: string, groupColumn = 0, keyColumn = 1, valueColumn = 2) {
        const fb = fs.readFileSync(path).toString();
        const rows = fb.split('\n');
        for (const row of rows) {
            if (!row.trim().startsWith(ReferenceList.COMMENT)) {
                const cells = row.split(ReferenceList.DELIMITER);
                if (cells.length >= 3) {
                    const group = cells[groupColumn].trim();
                    const key = cells[keyColumn].trim();
                    const val = cells[valueColumn].trim();
                    const groupMap = this.references.get(group);
                    if (groupMap) {
                        groupMap.set(key, val);
                    } else {
                        const keyMap = new Map<string, string>();
                        keyMap.set(key, val);
                        this.references.set(group, keyMap);
                    }
                }
            }
        }
    }


    public static has(group: string, key?: string) {
        if (!key) {
            return this.references.has(group);
        } else {
            const gm = this.references.get(group);
            if (gm) {
                return gm.has(key);
            } else {
                return false;
            }
        }
    }

    public static getList(group: string) {
        const gm = this.references.get(group);
        if (gm) {
            return gm;
        } else {
            throw new Error('Group does not exist in reference-list! Group: ' + group);
        }
    }

    public static hasKey(group: string, key: string) {
        const gm = this.references.get(group);
        if (gm) {
            return gm.has(key);
        } 
        return false;
    }

    public static getValue(group: string, key: string) {
        const gm = this.references.get(group);
        if (gm) {
            const v = gm.get(key);
            if (v !== undefined) {
                return v;
            } else {
                throw new Error('Key does not exist in reference-list group ' + group + '! Key: ' + key);
            }
        } else {
            throw new Error('Group does not exist in reference-list! Group: ' + group);
        }
    }

    public static getKey(group: string, value: string) {
        const gm = this.references.get(group);
        if (gm) {
            let key: string | undefined;
            gm.forEach( (v, k, map) => {
                if (v === value) {
                    key = k;
                }
            });
            if (key !== undefined) {
                return key;
            } else {
                throw new Error('Value ' + value + ' does not exist in group ' + group + '!');
            }
        } else {
            throw new Error('Group does not exist in reference-list! Group: ' + group);
        }
    }
    public static hasValue(group: string, value: string) {
        const gm = this.references.get(group);
        if (gm) {
            let found = false;
            gm.forEach( (v, k, map) => {
                if (v === value) {
                    found = true;
                }
            });
            return found;
        } 
        return false;
    }
    
}