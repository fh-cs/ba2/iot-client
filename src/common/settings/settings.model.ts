import { ITestTriggerSettings } from "../../trigger/test-trigger-settings.model";
import { IControllerSettings } from "../../controller/controller-settings.model";
import { IDriverSettings } from "../../driver/driver-settings.model";
import { IRecorderSettings } from "../../recorder/recorder-settings.model";
import { ILoggerSettings } from "../logger/logger-settings.model";

export interface ISettings {
    trigger: ITestTriggerSettings[];
    controller: IControllerSettings[];
    driver: IDriverSettings[];
    recorder: IRecorderSettings[];
    logger: ILoggerSettings;
    
}
