import * as fs from 'fs';
import { ISettings } from './settings.model';



export class Settings {
    private static _service?: Settings;
    private settings?: ISettings;
    private _filepath = 'settings.json';
    public get filepath() {
        return this._filepath;
    }
    
    loadSettings(filepath?: string) {
        if (filepath) {
            this._filepath = filepath;
        }
        const data = fs.readFileSync(this.filepath);
        this.settings = JSON.parse(data.toString());
    }

    hasSettings(key?: string) {
        if (!key) {
            return this.settings !== undefined;
        } else {
            return this.settings && (<any>this.settings)[key];
        }
    }

    getSettings(key?: string) {
        if (!key) {
            return this.settings;
        } else {
            return this.hasSettings(key) ? (<any>this.settings)[key] : undefined;
        }
    }

    public static getService() {
        if (!this._service) {
            this._service = new Settings();
        }
        return this._service;
    }

    public static loadSettings(filepath?: string) {
        return this.getService().loadSettings(filepath);
    }

    public static hasSettings(key?: string) {
        return this.getService().hasSettings(key);
    }

    public static getSettings(key?: string) {
        if (this.hasSettings(key)) {
            return this.getService().getSettings(key);
        } else {
            throw new Error('Settings not available ' + (key ? ' or key `' + key + ' does not exist!' : '!'));
        }
        
    }

}