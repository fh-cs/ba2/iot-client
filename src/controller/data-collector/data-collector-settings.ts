export interface IDataCollectorSettings {
    /**
     * If false or undefined, pass through send-commands. If true, collect data and send until a certain quantity (according settings) is reached or prepare-disconnect is called.
     * Default-value is true.
     */
    enable: boolean;
    /**
     * Maximum number of records to store. If undefined, value is infinity.
     */
    maximum?: number;

    /**
     * Hold records until the output stream length in BYTE (precalculated) has reached that value
     * If undefined, function is disabled.
     */
    maximumSize?: number;
}