import { IDataCollectorSettings } from "./data-collector-settings";
import { Subject } from "rxjs";
import { ITestVar } from "../../trigger/test-event.model";
import { IComVar } from "../rt-variable/runtime-var.model";
import { RuntimeVarFactory } from "../rt-variable/runtime-var.factory";
import { ESchemaGroup } from "../../common/schema-group.enum";
import { DataPackerOperator } from "../data-packer/datapacker-operator";
import { Logger } from "../../common/logger/logger";
import { MinMaxAveRecorder } from "../../recorder/min-max-ave-recorder";
import { XY_Recorder } from "../../recorder/x-y-recorder";

export class DataCollector  {

    private _settings: IDataCollectorSettings;
    public onProceedData = new Subject<IDataCollectorRecord | IDataCollectorRecord[]>(); 
    private records = new Array<IDataCollectorRecord>();
    
    public get settings() {
        return this._settings;
    }
    constructor(settings?: IDataCollectorSettings) { 
        if (settings) {
            this._settings = settings;
        } else {
            this._settings = {
                enable: false
            };
        }
    }

    sendRequest(testname: string, variable: ITestVar) {
        // Pass through (if disabled or variable is not from schema-group data)
        if (!this.settings.enable || variable.schemaGroup !== ESchemaGroup.DATA) {
            this.onProceedData.next({testname: testname, testVariable: variable, comVariable: RuntimeVarFactory.CREATE(variable)});
            return;
        }
        
        // Collect
        this.records.push({testname: testname, testVariable: variable, comVariable: RuntimeVarFactory.CREATE(variable)});
        const comVars = this.records.map(v => v.comVariable);
        const preSizeUnpacked = new Buffer(JSON.stringify(comVars)).byteLength;
        const preSizePacked = DataPackerOperator.GET().packData(comVars).byteLength;

        // Record unpacked/packed as XY for trend-chart
        XY_Recorder.GET().record('Datapacker', preSizePacked, preSizeUnpacked, 'byte', 'byte')

        let maxSizeReached = false;
        if (this.settings.maximumSize) {
            try {
                Logger.debug('Datacollector: Precalculated size: ' + preSizePacked + '/' + this.settings.maximumSize + 'byte');
                maxSizeReached =  preSizePacked >= this.settings.maximumSize;
                if (maxSizeReached) {
                    MinMaxAveRecorder.GET().record('Datacollector max-size records', this.records.length);
                    MinMaxAveRecorder.GET().record('Datacollector max-size byte-length', preSizePacked);
                }
            } catch(error) {
                Logger.error('Error during precalculation of payload!', error);
            }
        }

        if (maxSizeReached || this.settings.maximum && this.records.length > this.settings.maximum) {
           this.emitRecords();
        }
        
    }


    cleanUp() {
        this.emitRecords();
    }

    private emitRecords() {
        const recordsClone = JSON.parse(JSON.stringify(this.records)) as IDataCollectorRecord[];
        this.records.splice(0, this.records.length);
        this.onProceedData.next(recordsClone);
    }


}

export interface IDataCollectorRecord {
    testname: string,
    testVariable: ITestVar,
    comVariable: IComVar<any>
}