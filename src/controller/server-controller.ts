import { Controller } from "./controller";
import { Logger } from "../common/logger/logger";
import { ESchemaGroup } from "../common/schema-group.enum";
import { RequestVar, ComVar, IComVar } from "./rt-variable/runtime-var.model";
import { ITestVar } from "../trigger/test-event.model";

export class ServerController extends Controller {
    
    
    protected emitReceived(group: ESchemaGroup, data: Buffer | ComVar<any> | ComVar<any>[]) {
        if (Buffer.isBuffer(data)) {
            this.recordDataReceivedDriver(this.testname, group, data.byteLength);
            const vars = this.unpackData(data);
            this.emitReceived(group, vars);
            return;
        }
        if (Array.isArray(data)) {
            data.forEach( v => {
                this.emitReceived(group, v);
            });
            return;
        }
                
        const reqVar = data as RequestVar;
        Logger.debug('Controller: Received var `' + reqVar.name + '` from group:' + group, reqVar);
        const testVar: ITestVar = {
            name : reqVar.name,
            schemaGroup: group,
            value : reqVar.value
        };
        
        this.recordDataReceivedOrigin(this.testname, testVar);
        
        this.onReceive.next(testVar);
    }

    
}