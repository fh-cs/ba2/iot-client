import { IControllerSettings } from "./controller-settings.model";
import { Controller } from "./controller";
import { EControllerType } from "./controller-type.enum";
import { ServerController } from "./server-controller";
import { DeviceController } from "./device-controller";

export class ControllerFactory {
    private static _services = new Map<string, Controller>();
    /**
     * REturns a controller-instance based on the given settings
     */
    public static CREATE(settings: IControllerSettings) {
        if (!settings) {
            throw new Error('Failed to create new controller! Settings are not provided!');
        }
        switch(settings.type) {
            case EControllerType.DEV_CONTROLLER:
                return new DeviceController(settings);
            case EControllerType.SVR_CONTROLLER:
                return new ServerController(settings);
            default:
                throw new Error('ControllerFactory: Type of ' + settings.type + ' does not exist (or not implemented yet)!');
        }
    }
    public static getService(nameOrSettings: string | IControllerSettings) {
        let name = '';
        let settings: IControllerSettings | undefined;
        if (typeof nameOrSettings === 'string') {
            name = nameOrSettings;
            settings = undefined;
        } else {
            name = nameOrSettings.name;
            settings = nameOrSettings;
        }
        const serv = this._services.get(name);
        if (serv) {
            return serv;
        } else {
            if (settings) {
                let controller = this.CREATE(settings);
                this._services.set(name, controller);
                return controller;
            } else {
                throw new Error('ControllerFactory: Settings required to create new instance!');
            }
        }
    }
}


