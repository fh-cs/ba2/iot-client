import { DataPacker } from "./datapacker";
import { ComVar } from "../rt-variable/runtime-var.model";
import * as cbor from 'cbor';
import { EDataPackerType } from "./datapacker-type.enum";
import { DataPackerSettings } from "./datapacker-settings";
import { EDataPackerBitValue } from "./datapacker-bitvalue.enum";
import { ReductionRecorder } from "../../recorder/reduction-recorder";
import { Logger } from "../../common/logger/logger";

export class CborDataPacker extends DataPacker {
    

    constructor(settings?: DataPackerSettings) {
        super(EDataPackerBitValue.CBOR, settings);
    }

    protected doConversion(varToSend: any): boolean {
        return true;
    }

    protected packDataIntern(unpackedData: any) {
        if (Buffer.isBuffer(unpackedData)) {
            throw new Error('CBor-Datapacker requires an Object for serialization!');
        }
        const dataString = JSON.stringify(unpackedData);
        const dataClone = JSON.parse(dataString);
        Logger.debug('CBOR-Data to pack', dataString);
        const jsonBuffer = new Buffer(dataString);
        const cbor1 = require('cbor');
        const cborBuffer = cbor1.encode(dataClone) as Buffer;
        ReductionRecorder.GET().record('CBOR-Datenformat', jsonBuffer.length , cborBuffer.length);
        Logger.debug('CBor Datapacker: Data unpacked length: ' + jsonBuffer.length + 'byte --> packed length: ' + cborBuffer.length + 'byte', JSON.stringify(cborBuffer));

        return cborBuffer;
    }

    
    public unpackData(receivedData: Buffer): ComVar<any> | ComVar<any>[] {
        const jsonData = cbor.decodeAllSync(receivedData);
        // cbor encapsulates data into an array
        if (Array.isArray(jsonData) && jsonData.length === 1) {
            return jsonData[0];
        } else {
            return jsonData;
        }
    }

    protected getDefaultSettings() {
        return {
            type: EDataPackerType.CBOR
        };
    }
}