export enum EDataPackerType {
    NAME = 'name',
    VALUE_TIMESTAMP = 'value-timestamp',
    TIMESTAMP = 'timestamp',
    JSON = 'json',
    CBOR = 'cbor',
    ZIP = 'zip'
}