import { DataPacker } from "./datapacker";
import { DataPackerSettings } from "./datapacker-settings";
import { EDataPackerBitValue } from "./datapacker-bitvalue.enum";
import { ComVar, IRuntimeVar, RuntimeVar, IRequestVar } from "../rt-variable/runtime-var.model";
import { EDataPackerType } from "./datapacker-type.enum";
import { Logger } from "../../common/logger/logger";
import { ReferenceList } from "../../common/reference-list";

export class ValueTimestampDataPacker extends DataPacker {
    private static REFERENCE_GROUP = 'var-name';

    constructor(settings?: DataPackerSettings) {
        super(EDataPackerBitValue.VALUE_TIMESTAMP, settings);
    }

    protected doConversion(varToSend: any): boolean {
        return Array.isArray(varToSend) || (typeof varToSend === 'object');
    }

    protected packDataIntern(varToSend: any): any {
        if (Array.isArray(varToSend)) {
            const packed = this.convertList(varToSend);
            Logger.debug('Packed array-data:', JSON.stringify(packed));
            return packed;
        } else if (typeof varToSend === 'object') {
            const packedObj = this.convertObject(varToSend);
            Logger.debug('Packed object-data:', JSON.stringify(packedObj));
            return packedObj;
        } else {
            throw new Error('Timestamp-Datapacker requires a List or and object for serialization!');
        }
    }

    public unpackData(receivedData: any): ComVar<any> | ComVar<any>[] {
        if (Array.isArray(receivedData)) {
            const originArray =  this.convertListBack(receivedData);
            Logger.debug('Unpacked array-data:', JSON.stringify(originArray));
            return originArray;
        } else if (typeof receivedData === 'object') {
            const originObj = this.convertObjectBack(receivedData);
            Logger.debug('Unpacked object-data:', JSON.stringify(originObj));
            return originObj;
        } else {
            throw new Error('ValueTimestamp-Datapacker requires an array or object for deserialization!');
        }
    }

    protected getDefaultSettings(): DataPackerSettings {
        return {
            type: EDataPackerType.VALUE_TIMESTAMP,
            enable: true
        };
    }

    private convertList(comVars: RuntimeVar<any>[]) {
        // Logger.debug('Datapacker: Relative value-timestamp', JSON.stringify(comVars));

        if (comVars.length > 0) {
            // Get variable names
            const dataPacked = new Array<IValueTimestampRecord>();
            const last: { time?: number } = {};

            comVars.forEach(cv => {
                const cd = this.convertObject(cv, last);
                dataPacked.push(cd);
            });
            return dataPacked;
        } else {
            // empty array
            return [];
        }
    }


    private convertObject(v: RuntimeVar<any>, last?: { time?: number }) {
        // if last is undefined, relative-timestamp is not used
        if (!last) {
            last = {};
        }
        Logger.debug('Runtime-Record before packaging:', JSON.stringify(v));
        const myTimeAbs = Math.trunc(new Date(v.ts).getTime() / 1000);
        const newRec: IValueTimestampRecord = {};
        // name or id from reference list (if registered)
        if (ReferenceList.hasKey(ValueTimestampDataPacker.REFERENCE_GROUP, v.name)) {
            newRec.i = Number.parseInt(ReferenceList.getValue(ValueTimestampDataPacker.REFERENCE_GROUP, v.name));
        } else {
            newRec.n = v.name;
        }
        
        if (v.ts && v.ts !== null) {
            // timestamp absolute
            if (!last.time) {
                last.time = myTimeAbs;
                newRec.ts = myTimeAbs;
            } else {
                // timestamp relative
                newRec.tr = myTimeAbs - last.time;
                last.time = myTimeAbs;
            }
        }
        
        // set state false if value is missing or state defined and false
        if (v.value === undefined || (v.state !== undefined && !v.state)) {
            newRec.s = false;
        }

        if (v.value !== undefined) {
            newRec.v = v.value;
        }        
        
        // Message-ID (config and notify only)
        if (v['msgId'] !== undefined) {
            newRec.m = v['msgId'];
        }

        Logger.debug('Runtime-Record after packaging:', JSON.stringify(newRec));
        return newRec;
    }

  

    private convertListBack(data: IValueTimestampRecord[]) {
        // Logger.debug('Data before unpack: \n' + JSON.stringify(data));
        let rtVars = new Array<IRuntimeVar<any>>();
        const last: {time?: number } = {};
        data.forEach((d, i, a) => {
            rtVars.push(this.convertObjectBack(d, last));
        });

        return rtVars.sort((a, b) => {
            if (a.ts && b.ts) {
                return a.ts.getTime() - b.ts.getTime();
            } else {
                throw new Error('Sort records aborted, because timestamp is missing!');
            }
        });
    }


    private convertObjectBack(r: IValueTimestampRecord, last?: { time?: number }) {
        if (!last) {
            last = {};
        }
        Logger.debug('Incoming Record before unpackaging:', JSON.stringify(r));
        // get name, from object or from reference-list by received ID
        let name: string;
        if (r.n) {
            name = r.n;
        } else if (r.i !== undefined) {
            if (ReferenceList.hasValue(ValueTimestampDataPacker.REFERENCE_GROUP, r.i.toString())) {
                name = ReferenceList.getKey(ValueTimestampDataPacker.REFERENCE_GROUP, r.i.toString());
            } else {
                throw new Error('Value ' + r.i + ' in Referencelist group `' + ValueTimestampDataPacker.REFERENCE_GROUP + '` not found!');
            }
        } else {
            throw new Error('Name (n) either ID (i) not found in record, but one is required! Record: ' + JSON.stringify(r));
        }

        const cVar: IRuntimeVar<any> = { name: name, state: 0, value: null, ts: new Date() };
        if (r.ts || r.tr) {
            if (last.time === undefined || r.ts) {
                // absolute timestamp
                if (r.ts) {
                    cVar.ts = new Date(r.ts * 1000);
                    last.time = r.ts;
                } else {
                    throw new Error('First record must have absolute timestamp (ts)!');
                }
            } else {
                // relative timestamp
                if (r.tr !== undefined) {
                    cVar.ts = new Date((last.time + r.tr) * 1000);
                    last.time += r.tr;
                } else {
                    throw new Error('Relative timestamp (tr) not defined!');
                }
            }
        }
        
        // status and value
        cVar.state = (r.s !== undefined && r.s === false) ? 0 : 1;
        cVar.value = r.v;
       
        if (r.m) {
            cVar['msgId'] = r.m;
        }
        Logger.debug('Runtime Record after unpackaging:', JSON.stringify(cVar));
        return cVar;
    }

    


}



interface IValueTimestampRecord {
    /**
     * Name of the variable
     */
    n?: string;
    /**
     * ID of the variable
     */
    i?: number;
    /**
     * Absolute timestamp
     * In Unix-Timestamp (seconds since 01.01.1970).
     * If undefined, `tr` must be defined instead.
     */
    ts?: number;
    /**
     * Relative timestamp in seconds to previous record.
     * If undefined, `ts` must be defined instead.
     */
    tr?: number;
    /**
     * Value absolute
     */
    v?: number;

    /**
     * Status
     * If undefined, status is OKAY (true) and keep
     * Property `v` omitted
     */
    s?: boolean;

    /**
     * Message-Id
     */
    m?: string;
}
