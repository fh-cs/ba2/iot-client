import { DataPacker } from "./datapacker";
import { DataPackerSettings } from "./datapacker-settings";
import { EDataPackerBitValue } from "./datapacker-bitvalue.enum";
import { ComVar, IRuntimeVar, IComVar, RuntimeVar } from "../rt-variable/runtime-var.model";
import { EDataPackerType } from "./datapacker-type.enum";
import { IDataCollectorRecord } from "../data-collector/data-collector";
import { ReductionRecorder } from "../../recorder/reduction-recorder";
import { Logger } from "../../common/logger/logger";

export class TimestampDataPacker extends DataPacker {


    constructor(settings?: DataPackerSettings) {
        super(EDataPackerBitValue.TIMESTAMP, settings);
    }

    protected doConversion(varToSend: any): boolean {
        return Array.isArray(varToSend);
    }

    protected packDataIntern(varToSend: any): any {
        if (!Array.isArray(varToSend)) {
            throw new Error('Timestamp-Datapacker requires a List for serialization!');
        }

        const packed = this.changeDataToRelativeTimestamps(varToSend);
        return packed;
    }

    public unpackData(receivedData: any): ComVar<any> | ComVar<any>[] {
        if (Buffer.isBuffer(receivedData)) {
            throw new Error('Timestamp-Datapacker requires an object for deserialization!');
        }
        if (Array.isArray(receivedData)) {
            receivedData = receivedData[0];
        }
        if (receivedData.t && receivedData.r) {
            const unpacked = this.changeDataToAbsoluteTimestamp(receivedData);
            return unpacked;
        } else {
            throw new Error('Timestamp-Datapacker requires an object with properties `t` and `r` for deserialization!');
        }
    }

    protected getDefaultSettings(): DataPackerSettings {
        return {
            type: EDataPackerType.TIMESTAMP,
            enable: true
        };
    }

    private changeDataToRelativeTimestamps(comVars: RuntimeVar<any>[]) {
        Logger.debug('Datapacker: Relative timestamp', JSON.stringify(comVars));
        let timeAbsSec: number = Math.trunc(Date.now() / 1000);
        if (comVars.length > 0) {
            const sizeNormal = new Buffer(JSON.stringify(comVars)).length;
            // Take timestamp from first record
            const firstTsRecord = comVars.find(rVar => rVar && rVar.ts !== undefined);
            if (firstTsRecord) {
                timeAbsSec = Math.trunc(new Date(firstTsRecord.ts).getTime() / 1000);
            }
            const recsChanged = comVars.map(rVar => {
                //Clone runtime-variable
                const v = JSON.parse(JSON.stringify(rVar)) as RuntimeVar<any>;
                
                if (v.ts !== undefined) {
                    // Calculate relative time-stamp in sec and delete absolute time-stamp
                    const timeRecSec = Math.trunc(new Date(v.ts).getTime() / 1000);
                    const timeRelSec = timeRecSec - timeAbsSec;
                    Logger.debug('Time absolute: ' + timeAbsSec + 'sec; Time Record: ' + timeRecSec + 'sec; Time relative: ' + timeRelSec + 'sec');
                    v['t'] = timeRelSec;
                    delete v.ts;
                }
                return v;
            });
            const retValue = { t: timeAbsSec, r: recsChanged };
            Logger.debug('Data with relative timestamp:' + JSON.stringify(retValue));
            const size = new Buffer(JSON.stringify(retValue)).length;
            ReductionRecorder.GET().record('Relative Timestamp', sizeNormal, size);
            return retValue;
        } else {
            // empty array
            return { t: timeAbsSec, r: [] };
        }
    }

    private changeDataToAbsoluteTimestamp(data: { t: number, r: IComVar<any>[] } | IComVar<any>[]) {
        if (Array.isArray(data)) {
            return data;
        } else {
            if (data.t !== undefined) {
                return data.r.map(v => {
                    const rt = v as IRuntimeVar<any>;
                    if (rt.t) {
                        rt.ts = new Date((data.t + rt.t) * 1000);
                        delete rt.t;
                    }
                    return rt;
                });
            } else {
                throw new Error('Change from relative to absolute timestamps failed, absolute timestamp `t` is undefined!');
            }
        }
    }
}