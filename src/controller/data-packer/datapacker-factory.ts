import { EDataPackerType } from "./datapacker-type.enum";
import { JsonDataPacker } from "./json-datapacker";
import { CborDataPacker } from "./cbor-datapacker";
import { DataPacker } from "./datapacker";
import { DataPackerSettings } from "./datapacker-settings";
import { EDataPackerBitValue } from "./datapacker-bitvalue.enum";
import { ZipDataPacker } from "./zip-datapacker";
import { TimestampDataPacker } from "./timestamp-datapacker";
import { ValueTimestampDataPacker } from "./value-timestamp-datapacker";
import { NameDataPacker } from "./name-datapacker";

export class DataPackerFactory {
    public static CREATE(typeOrSettings?: EDataPackerType | DataPackerSettings): DataPacker {
        let settings: DataPackerSettings | undefined;
        let type: EDataPackerType;
        if (typeOrSettings === undefined) {
            typeOrSettings = EDataPackerType.JSON;
        }
        if (typeof typeOrSettings === 'string') {
            type = typeOrSettings;
        } else {
            type = typeOrSettings.type;
            settings = typeOrSettings;
        }
        switch (type) {
            // If you add new data-packer, don't forget to add here: `static GetTypesWithBitValue()`
            case EDataPackerType.NAME:
                return new NameDataPacker(settings);
            case EDataPackerType.VALUE_TIMESTAMP:
                return new ValueTimestampDataPacker(settings);
            case EDataPackerType.TIMESTAMP:
                return new TimestampDataPacker(settings);
            case EDataPackerType.JSON:
                return new JsonDataPacker(settings);
            case EDataPackerType.CBOR:
                return new CborDataPacker(settings);
            case EDataPackerType.ZIP:
                return new ZipDataPacker(settings);
            default:
                throw new Error('Create type `' + typeOrSettings + '` of data-packer failed, type unknown (or not implemented)!')
        
        }
    }
    
    public static GET_LIST(type?: EDataPackerType | DataPackerSettings | EDataPackerType[] | DataPackerSettings[]) {
        type = type || EDataPackerType.JSON;
        
        const arr = new Array<DataPacker>();
        if (Array.isArray(type)) {
            for (const i of type) {
                arr.push(this.CREATE(i));
            }
        } else {
            arr.push(this.CREATE(type));
        }
        // is empty, add default parser
        if (arr.length <= 0) {
            arr.push(this.CREATE(EDataPackerType.JSON));
        }
        return arr;
    }

    /**
     * Returns the next datapacker-type listed in the given byte-value, starting either from least-significant-bit (bit 0) or most-significant bit.
     * Returns undefined type was not present (perhaps, byteValue is 0x00).
     */
    public static GetNextType(byteValue: EDataPackerBitValue, startWithMostSignificant = false) {
        const typeWithBitValue = this.GetTypesWithBitValue(startWithMostSignificant);
        const tv = typeWithBitValue.find(t => (byteValue & t.bitValue) > 0);
        return tv ? tv.type : undefined;
    }

    public static GetTypes(formatByte: number, startWithMostSignificant = false) {
        const types = this.GetTypesWithBitValue(startWithMostSignificant);
        return types.filter( (v, i, a) => {
            return (formatByte & v.bitValue) > 0;
        }).map( (v) => v.type );
    }

    public static GetTypesWithBitValue(startWithMostSignificant = false): { type: EDataPackerType, bitValue: EDataPackerBitValue }[] {
        const list = [
            { type: EDataPackerType.NAME, bitValue: EDataPackerBitValue.NAME },
            { type: EDataPackerType.VALUE_TIMESTAMP, bitValue: EDataPackerBitValue.VALUE_TIMESTAMP },
            { type: EDataPackerType.TIMESTAMP, bitValue: EDataPackerBitValue.TIMESTAMP },
            { type: EDataPackerType.JSON, bitValue: EDataPackerBitValue.JSON },
            { type: EDataPackerType.CBOR, bitValue: EDataPackerBitValue.CBOR },
            { type: EDataPackerType.ZIP, bitValue: EDataPackerBitValue.ZIP }
        ];
        return list.sort((a, b) => startWithMostSignificant ? b.bitValue - a.bitValue : a.bitValue - b.bitValue);
    }
}