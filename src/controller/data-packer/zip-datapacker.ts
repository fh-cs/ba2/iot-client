import { DataPacker } from "./datapacker";
import { DataPackerSettings } from "./datapacker-settings";
import { EDataPackerBitValue } from "./datapacker-bitvalue.enum";
import { ComVar } from "../rt-variable/runtime-var.model";
import { EDataPackerType } from "./datapacker-type.enum";
import * as zlib from "zlib";
import { Logger } from "../../common/logger/logger";
import { ReductionRecorder } from "../../recorder/reduction-recorder";
import { ZipDatapackerSettings as ZipDataPackerSettings } from "./zip-datapacker-settings";

export class ZipDataPacker extends DataPacker {
    

    constructor(settings?: ZipDataPackerSettings) {
        super(EDataPackerBitValue.ZIP, settings);
    }

    

    protected doConversion(varToSend: any): boolean {
       if (!Buffer.isBuffer(varToSend)) {
           throw new Error('ZIP-Datapacker requires a buffer!');
       }
       // compress and compare byte-length -> only zip if it makes sense
       const lenUnzipped = varToSend.byteLength;
       const lenZipped = zlib.deflateSync(varToSend).length;
       const settings = this.settings as ZipDataPackerSettings;
       // Zip data if 
       // - alwas is true, 
       // - or minimumSize is defined and unzipped (origin) data is above minimumSize,
       // - or minimumSize is UNdefined and zipped data is below unzipped data
       const doZip = settings.always || (settings.minimumSize && lenUnzipped > settings.minimumSize) || (!settings.minimumSize && lenZipped < lenUnzipped);
       return this.enabled ? doZip : false;      
    }
    
    protected packDataIntern(varToSend: ComVar<any> | ComVar<any>[] | Buffer): Buffer {
        if (!Buffer.isBuffer(varToSend)) {
            throw new Error('Zip-Datapacker requires a Buffer for serialization! Object to Buffer serialization to be called first!');
        }
        const zippedBuffer = zlib.deflateSync(varToSend);
        ReductionRecorder.GET().record('ZLIB-Komprimierung', varToSend.length, zippedBuffer.length);
        //Logger.info('ZIP-Datapacker Size Buffer unzipped: ' + varToSend.length + 'byte Size zipped: ' + zippedBuffer.length + 'byte');
        return zippedBuffer;
    }
    
    public unpackData(receivedData: Buffer): Buffer {
        if (!Buffer.isBuffer(receivedData)) {
            throw new Error('Zip-Datapacker requires a Buffer for deserialization!');
        }
        Logger.debug('ZIP-Unpack: Data BEFORE: Length: ' + receivedData.byteLength, JSON.stringify(receivedData));
        const unzipped = zlib.inflateSync(receivedData);
        Logger.debug('ZIP-Unpack: Data AFTER: Length: ' + receivedData.byteLength, JSON.stringify(receivedData));
        return unzipped;
    }

    protected getDefaultSettings() {
        return {
            type: EDataPackerType.ZIP
        };
    }
}