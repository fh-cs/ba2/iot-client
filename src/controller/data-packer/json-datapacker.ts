import { DataPacker } from "./datapacker";
import { ComVar } from "../rt-variable/runtime-var.model";
import { EDataPackerType } from "./datapacker-type.enum";
import { DataPackerSettings } from "./datapacker-settings";
import { EDataPackerBitValue } from "./datapacker-bitvalue.enum";

export class JsonDataPacker extends DataPacker {
    

    constructor(settings?:DataPackerSettings) {
        super(EDataPackerBitValue.JSON, settings);
    }

    protected doConversion(varToSend: any): boolean {
       return true;
    }
    
    protected packDataIntern(varToSend: ComVar<any> | ComVar<any>[] | Buffer): Buffer {
        if (Buffer.isBuffer(varToSend)) {
            throw new Error('JSON-Datapacker requires an Object for serialization!');
        }
        return new Buffer(JSON.stringify(varToSend));
    }
    
    public unpackData(receivedData: Buffer): ComVar<any> | ComVar<any>[] {
        return JSON.parse(receivedData.toString());
    }

    protected getDefaultSettings() {
        return {
            type: EDataPackerType.JSON
        };
    }
}