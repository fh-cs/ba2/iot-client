import { DataPackerSettings } from "./datapacker-settings";

export interface ZipDatapackerSettings extends DataPackerSettings {
    /**
     * Zip data if unzipped data is above (byte) this value.
     * Default is 0 (zip always or only if zipped data is below unzipped)
     */
    minimumSize?: number,
    /**
     * If true, zip data always or 
     * only if zipped data is less origin data and minimumSize is undefined or 0.
     * Default is false.
     */
    always?: boolean
}