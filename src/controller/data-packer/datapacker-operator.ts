import { EDataPackerType } from "./datapacker-type.enum";
import { DataPackerSettings } from "./datapacker-settings";
import { ComVar } from "../rt-variable/runtime-var.model";
import { DataPackerFactory } from "./datapacker-factory";
import { Logger } from "../../common/logger/logger";

export class DataPackerOperator {
    private static _instance: DataPackerOperator;

    private settings: EDataPackerType | DataPackerSettings | EDataPackerType[] | DataPackerSettings[] | undefined;

    public static GET() {
        if (!this._instance) {
            this._instance = new DataPackerOperator();
        }
        return this._instance;
    }

    public getSettings() {
        return this.settings ? this.settings : EDataPackerType.JSON;
    }

    public setSettings(settings: EDataPackerType | DataPackerSettings | EDataPackerType[] | DataPackerSettings[] | undefined ) {
        this.settings = settings;
    }

    public packData(varToSend: ComVar<any> | ComVar<any>[]): Buffer {
        const packers = DataPackerFactory.GET_LIST(this.getSettings());
        let packData: { formatByte: number, data:ComVar<any> | ComVar<any>[]|Buffer } = {formatByte: 0, data: varToSend};
        for (const packer of packers) {
            packData = packer.packData(packData.data, packData.formatByte);
        }
        
    
        if (!packData.data || !Buffer.isBuffer(packData.data)) {
            throw new Error('Result of data-packing is not from type Buffer!');
        }
        // add format-byte to the beginning of the byte-stream
        const formBuffer = Buffer.alloc(1);
        formBuffer.writeInt8(packData.formatByte, 0);
        return Buffer.concat([formBuffer, packData.data ]);
    }
    
    public unpackData(receivedData: Buffer): ComVar<any> | ComVar<any>[] {
        if (!receivedData || receivedData.length === 0) {
            throw new Error('Data-packer-executor cannot unpack without format-byte! ' + (receivedData ? 'Buffer-length is 0!' : 'Buffer is undefined!'));
        }
        let formatByte = receivedData.readUInt8(0);
        if (!formatByte) {
            throw new Error('Miss format-content in format-byte! Byte-Value is 0!');
        }
        if(receivedData.length === 1) {
            throw new Error('Payload after format-byte is missing!');
        }
        // get payload without format-byte
        let dataBuffer: any = receivedData.slice(1);
        Logger.debug('Unpack data in following order: ' + DataPackerFactory.GetTypes(formatByte, true).toString());
        let type: EDataPackerType | undefined;
        while ((type = DataPackerFactory.GetNextType(formatByte, true)) !== undefined) {
            const unpacker = DataPackerFactory.CREATE(type);
            // Logger.debug('Unpack data with unpacker: ' + unpacker.settings.type + ' Data BEFORE:', JSON.stringify(dataBuffer));
            dataBuffer = unpacker.unpackData(dataBuffer);
            // Logger.debug('Unpack data with unpacker: ' + unpacker.settings.type + ' Data AFTER:',  JSON.stringify(dataBuffer));
            formatByte &= ~unpacker.bitValue;
        }
        // Logger.debug('Unpacked received data:', dataBuffer);
        return dataBuffer;
    }
}