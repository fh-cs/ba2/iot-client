export enum EDataPackerBitValue {
    NAME = 0x01,
    VALUE_TIMESTAMP = 0x02,
    TIMESTAMP = 0x04,
    JSON = 0x08,
    CBOR = 0x10,
    ZIP = 0x20
}