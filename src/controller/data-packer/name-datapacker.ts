import { DataPacker } from "./datapacker";
import { ComVar } from "../rt-variable/runtime-var.model";
import { EDataPackerType } from "./datapacker-type.enum";
import { DataPackerSettings } from "./datapacker-settings";
import { EDataPackerBitValue } from "./datapacker-bitvalue.enum";
import { Logger } from "../../common/logger/logger";

export class NameDataPacker extends DataPacker {

    private static SUBSTITUDES: { long: string, short: string }[] = [
        { long: 'state', short: 's' },
        { long: 'name', short: 'n' },
        { long: 'value', short: 'v' },
        { long: 'ts', short: 't' }
    ];

    constructor(settings?: DataPackerSettings) {
        super(EDataPackerBitValue.NAME, settings);
    }

    protected doConversion(varToSend: any): boolean {
        return true;
    }

    protected packDataIntern(varToSend: ComVar<any> | ComVar<any>[] | Buffer): any {
        if (Buffer.isBuffer(varToSend)) {
            throw new Error('JSON-Datapacker requires an Object for serialization!');
        }
        if (Array.isArray(varToSend)) {
            return varToSend.map(v => {
                const nv = this.replaceObjectNames(v, false);
                Logger.debug('Name-Datapacker: Data packed:\n' + JSON.stringify(nv));
                return nv;
            });
        }
        const newVar = this.replaceObjectNames(varToSend, false);
        Logger.debug('Name-Datapacker: Data packed:\n' + JSON.stringify(newVar));
        return newVar;
    }

    public unpackData(receivedData: ComVar<any> | ComVar<any>[]): ComVar<any> | ComVar<any>[] {
        if (Buffer.isBuffer(receivedData)) {
            throw new Error('Name-Datapacker requires an Object for deserialization!');
        }
        if (Array.isArray(receivedData)) {
            return receivedData.map(v => this.replaceObjectNames(v, true));
        }
        const reconstructed = this.replaceObjectNames(receivedData, true);
        Logger.debug('Name-Datapacker: Data unpacked:\n' + JSON.stringify(reconstructed));
        return reconstructed;
    }

    private replaceObjectNames(receivedData: ComVar<any>, shortToLong: boolean): ComVar<any> {
        // is object
        const clone = JSON.parse(JSON.stringify(receivedData)) as ComVar<any>;
        const source = shortToLong ? 'short' : 'long';
        const dest = !shortToLong ? 'short' : 'long';
        NameDataPacker.SUBSTITUDES.forEach(s => {
            if (clone[s[source]] !== undefined) {
                clone[s[dest]] = clone[s[source]];
                delete clone[s[source]];
            } else {
                Logger.warn('Object has no key: ' + s[source] + '! Substitude: ' + JSON.stringify(s) + '; Object: ' + JSON.stringify(clone));
            }
        });
        return clone;
    }

    protected getDefaultSettings() {
        return {
            type: EDataPackerType.NAME
        };
    }
}