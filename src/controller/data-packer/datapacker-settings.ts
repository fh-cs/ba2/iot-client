import { EDataPackerType } from "./datapacker-type.enum";

export interface DataPackerSettings {
    type: EDataPackerType;
    /**
     * Data-Packer is enabled. Default is true.
     */
    enable?: boolean;
}