import { ComVar } from "../rt-variable/runtime-var.model";
import { DataPackerSettings } from "./datapacker-settings";
import { EDataPackerBitValue } from "./datapacker-bitvalue.enum";

export abstract class DataPacker {

    private _settings: DataPackerSettings;
    public get settings() {
        if (!this._settings) {
            throw new Error('Settings not supplied');
        } else {
            return this._settings;
        }
    }

    public get bitValue() {
        return this._bitValue;
    }
    
    constructor(private _bitValue: EDataPackerBitValue, settings?: DataPackerSettings) {
        if (settings) {
            this._settings = settings;
        } else {
            this._settings = this.getDefaultSettings();
        }
    }

    protected get enabled() {
        return this.settings.enable || true;
    }

    protected abstract getDefaultSettings(): DataPackerSettings;

    public packData(varToSend: any, formatByte?: number): {formatByte: number, data: ComVar<any> | ComVar<any>[] | Buffer} {
        formatByte = formatByte || 0;
        if (!this.enabled || !this.doConversion(varToSend)) {
            return { formatByte: formatByte, data: varToSend};
        } else {
            return { formatByte: formatByte | this.bitValue, data: this.packDataIntern(varToSend)}; 
        }
    }


    protected abstract doConversion(varToSend: any): boolean;

    /**
     * Convert com-data (single or multiple items) to buffer for transmission
     */
    protected abstract packDataIntern(unpackedData: any): any;
    /**
     * Convert com-data (single or multiple items) to buffer
     */
    public abstract unpackData(receivedData: any): any | Buffer;
}