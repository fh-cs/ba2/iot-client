

export interface IComVar<T> {
    /** Value to be transmitted */
    value: T;
    /** Name of the variable */
    name: string;
}

export interface IComVarShort<T> {
    /** Value to be transmitted */
    v: T;
    /** Name of the variable */
    n: string;
}



export abstract class ComVar<T> implements IComVar<T> {
    public value: T;
    public name: string;

    constructor(name: string, value: T) {
        this.name = name;
        this.value = value;
    }

}

export interface IRuntimeVar<T> extends IComVar<T> {
    /**
     * Status of the variable
     * 0=Invalid (reading failed)
     * 1=Valid
     */
    state: 0 | 1;
    /**
     * Timestamp this value was captured
     */
    ts: Date | undefined;
    /**
     * Relative timestamp in seconds to absolute time-stamp (part of parent object).
     * Positive value: younger timestamp (add to absolute), Negative: older (substract).
     */
    t?: number | undefined;
}

export interface IRuntimeVarShort<T> extends IComVarShort<T> {
    /**
     * Status of the variable
     * 0=Invalid (reading failed)
     * 1=Valid
     */
    s: boolean;
    /**
     * Absolute Timestamp this value was captured
     */
    ts: Date | undefined;
    /**
     * Relative timestamp in seconds to absolute time-stamp (part of parent object).
     * Positive value: younger timestamp (add to absolute), Negative: older (substract).
     */
    t: number | undefined;
}



export class RuntimeVar<T> extends ComVar<T> implements IRuntimeVar<T> {
    
    public state: 0 | 1 = 0;
    public ts = new Date();
    public t: number | undefined;

    constructor(name: string, value: T, ts?: Date) {
        super(name, value);
        this.state = 1;
        if (ts) {
            this.ts = ts;
        }
    }
}
/**
 * Data-model for variables from schema-group 'data' or 'notify'
 */
export interface IDataVar extends RuntimeVar<number> {
}
/**
 * Variable from schema-group 'data' or 'notify'
 */
export class DataVar extends RuntimeVar<number> implements IDataVar {
    constructor(name: string, value?: number, ts?: Date) {
        super(name, (value !== undefined ? value : 0), ts);
    }
}
/**
 * Data-model for variables send from server to client from schema-group 'config' or 'methods'
 */
export interface IRequestVar extends IComVar<number> {
    /**
     * Message-Id required to identify the response of a request.
     * The server creates the msgId and sends the ID with the request. The client returns the received msg-id with the response back to the server.
     */
    msgId: string;
}

/**
 * Data-model for variables send from server to client from schema-group 'config' or 'methods'
 */
export interface IRequestVarShort extends IComVarShort<number> {
    /**
     * Message-Id required to identify the response of a request.
     * The server creates the msgId and sends the ID with the request. The client returns the received msg-id with the response back to the server.
     */
    m: string;
}
/**
 * Variable send from server to client from schema-group 'config' or 'methods'
 */
export class RequestVar extends ComVar<number> implements IRequestVar {
    public msgId: string;

    constructor(name: string, value?: number, msgId?: string) {
        super(name, (value !== undefined ? value : 0));
        if (msgId) {
            this.msgId = msgId;
        } else {
            this.msgId = RequestVar.createMsgId();
        }
    }

    private static createMsgId() {
        return '_' + Math.random().toString(36).substr(2, 9);
    }
}


/**
 * Data-model for variables send from client to server from schema-group 'config' or 'methods'
 */
export interface IResponseVar extends IRuntimeVar<number> {
    /**
     * Message-Id required to identify the response of a request.
     * The server creates the msgId and sends the ID with the request. The client returns the received msg-id with the response back to the server.
     */
    msgId: string;
}
/**
 * Variable send from client to server from schema-group 'config' or 'methods'
 */
export class ResponseVar extends RuntimeVar<number> implements IResponseVar {
    public msgId: string;

    constructor(requestVar: RequestVar);
    constructor(name: string, value: number, msgId: string, ts?: Date);
    constructor(nameOrRequestVar: string | RequestVar, value?: number, msgId?: string, ts?: Date) {
        let name: string;
        if (typeof nameOrRequestVar === 'string') {
            name = nameOrRequestVar;
            
        } else {
            name = nameOrRequestVar.name;
            msgId = nameOrRequestVar.msgId;
            value = nameOrRequestVar.value;
        }
        super(name, (value !== undefined ? value : 0), ts);
        if (!msgId) {
            throw new Error('Message-Id from request is missing!');
        }
        this.msgId = msgId;
    }
}


export class StringVar extends RuntimeVar<string> {
    constructor(name: string, value?: string, ts?: Date) {
        super(name, (value !== undefined ? value : ''), ts);
    }
}

export class BooleanVar extends RuntimeVar<boolean> {
    constructor(name: string, value?: boolean, ts?: Date) {
        super(name, (value !== undefined ? value : false), ts);
    }
}