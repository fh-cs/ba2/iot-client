import { ITestVar } from "../../trigger/test-event.model";
import { RequestVar, DataVar, IRequestVar, ResponseVar, ComVar } from "./runtime-var.model";

export class RuntimeVarFactory {
    public static CREATE(testVar: ITestVar, messageId?: string): ComVar<number> {
        if (!testVar.schemaGroup) {
            throw new Error('Create runtime-variable failed, schema-group is missing!');
        }
        
        switch(testVar.schemaGroup) {
            case "config":
                return new RequestVar(testVar.name, testVar.value, messageId);
            case "data":
                return new DataVar(testVar.name, testVar.value);
            case "notify":
                return new DataVar(testVar.name, testVar.value);
            case "method":
                return new RequestVar(testVar.name, testVar.value, messageId);
            default:
                throw new Error('Create runtime-variable failed, schema-group ' + testVar.schemaGroup + ' is unknown!');
        }
    }

    public static CREATE_FROM_REQUEST(requestVar: IRequestVar) {
        return new ResponseVar(requestVar.name, requestVar.value, requestVar.msgId);
    }
}