import { ITestTriggerAbleService } from "../trigger/test-trigger";
import { IControllerSettings } from "./controller-settings.model";
import { Subject, Subscription } from 'rxjs';
import { ITestVar, ETestAction } from "../trigger/test-event.model";
import { Logger } from "../common/logger/logger";
import { EControllerType } from "./controller-type.enum";
import { RuntimeVarFactory } from "./rt-variable/runtime-var.factory";
import { ComVar, IRuntimeVar, IComVar } from "./rt-variable/runtime-var.model";
import { ESchemaGroup } from "../common/schema-group.enum";
import { DataCollector, IDataCollectorRecord } from "./data-collector/data-collector";
import { ReferenceList } from "../common/reference-list";
import { DataPackerOperator } from "./data-packer/datapacker-operator";
import { ReductionRecorder } from "../recorder/reduction-recorder";

export interface ITestDriver {
    name: string;
    /**
     * Returns true if this instance belongs to the controller with the provided **controllerName**.
     */
    belongsToController(controllerName: string): boolean;
    /**
     * Connect to server
     */
    connectForTest(): Promise<void>;
    sendTest(testVar: ITestVar | ESchemaGroup, data: Buffer): Promise<void>;
    /**
     * Send open jobs and close connection afterwards
     */
    disconnectForTest(): Promise<void>;
    /**
     * Subscription of received data
     */
    onReceive: Subject<{ schemaGroup: ESchemaGroup, data: Buffer }>;
}

export interface ITestRecorder {
    name: string;
    /**
     * Returns true, if the recorder belongs to the controller with the provided **controllerName**.
     */
    belongsToController(controllerName: string): boolean;
    /**
     * Prepare recorder. Stop if still running, reset counter, aso.
     */
    prepare(testName: string): Promise<void>;
    /**
     * Start recording
     */
    start(testName: string): Promise<void>;
    /**
     * Stop recording und save to file
     */
    stop(testName: string): Promise<void>;
    /**
     * Record the data requested to send to server before improvement (compression, packaging, aso)
     */
    recordDataSendOrigin(testName: string, variable: ITestVar): void;
    /**
     * Record the data received from server after reconstruction (extraction, unpackaging, aso)
     */
    recordDataReceivedOrigin(testName: string, variable: ITestVar): void;
    /**
     * Record data send to the server after data-improvement (compression, packaging, aso)
     * @param byteLength The size of the data to send in bytes as byte-array
     */
    recordDataSendDriver(testName: string, schemaGroup: ESchemaGroup, byteLength: number): void;
    /**
     * Record the data received from server before reconstruction (extraction, unpackaging, aso)
     * @param byteLength The size of the data received in bytes as byte-array
     */
    recordDataReceivedDriver(testName: string, schemaGroup: ESchemaGroup, byteLength: number): void;
}




export abstract class Controller implements ITestTriggerAbleService {


    private _name: string;
    private _type: EControllerType;
    private _onReceive = new Subject<ITestVar>();
    /**
     * Name of the test-trigger currently ongoing
     */
    private _testname = '';

    public get name() {
        return this._name;
    }
    public get type() {
        return this._type;
    }


    /**
     * Name of the test-trigger currently ongoing
     */
    protected get testname() {
        return this._testname;
    }
    protected set testname(name: string) {
        this._testname = name;
    }

    private _settings?: IControllerSettings;
    private _driverServices = new Map<string, { service: ITestDriver, subscription: Subscription }>();
    private _recorder = new Array<ITestRecorder>();
    private _dataCollector?: DataCollector;


    protected get dataCollector() {
        if (!this._dataCollector) {
            this._dataCollector = new DataCollector(this.getSettings().dataCollector);
            this._dataCollector.onProceedData.subscribe(records => this.send(records));
        }
        return this._dataCollector;
    }


    public constructor(settings: IControllerSettings) {
        if (!settings) {
            throw new Error('TestAdapter: GetServer failed, argument settings is missing!');
        }

        this._type = EControllerType.DEV_CONTROLLER;
        this._name = settings.name;
        this._settings = settings;


        if (this._settings.referenceList) {
            try {
                ReferenceList.loadReferences(this._settings.referenceList);
            } catch (err) {
                Logger.error('Failed to load reference-list!');
            }
        }
        DataPackerOperator.GET().setSettings(this.getSettings().dataPacker);
        ReductionRecorder.GET().reset();
    }

    public setSettings(settings: IControllerSettings) {
        this._settings = settings;
    }

    public getSettings() {
        if (this._settings) {
            return this._settings;
        } else {
            throw new Error('Test-Adapter ' + this.name + ': Settings are missing!');
        }
    }

    public hasSettings() {
        return this._settings !== undefined;
    }

    //#region RECORDER
    public addRecorder(recorder: ITestRecorder | ITestRecorder[]) {
        if (Array.isArray(recorder)) {
            recorder.forEach(r => this.addRecorder(r));
            return;
        }
        if (!recorder.belongsToController(this.name)) {
            return;
        }
        if (this._recorder.findIndex(r => r.name === recorder.name) < 0) {
            this._recorder.push(recorder);
        }
    }
    public getRecorder(recorderName: string) {
        const rec = this._recorder.find(r => r.name === recorderName);
        if (rec) {
            return rec;
        } else {
            throw new Error('Test-Adapter ' + this.name + ': Recorder ' + recorderName + ' not added!');
        }
    }
    public hasRecorder(recorderName: string) {
        return this._recorder.find(r => r.name === recorderName) !== undefined;
    }
    private async prepareRecorder(testname: string) {
        for (const rec of this._recorder) {
            try {
                await rec.prepare(testname);
            } catch (e) {
                Logger.error('Prepare recorder ' + rec.name + ' failed!', e);
            }
        }
    }
    private async startRecorder(testname: string) {
        for (const rec of this._recorder) {
            try {
                await rec.start(testname);
            } catch (e) {
                Logger.error('Start recorder ' + rec.name + ' failed!', e);
            }
        }
    }
    private async stopRecorder(testname: string) {
        for (const rec of this._recorder) {
            try {
                await rec.stop(testname);
            } catch (e) {
                Logger.error('Stop recorder ' + rec.name + ' failed!', e);
            }
        }
    }

    protected async recordDataSendOrigin(testname: string, variable: ITestVar | ITestVar[]) {
        if (Array.isArray(variable)) {
            for (const item of variable) {
                await this.recordDataSendOrigin(testname, item);
            }
            return;
        }
        // variable is not an array
        for (const rec of this._recorder) {
            try {
                rec.recordDataSendOrigin(testname, variable);
            } catch (e) {
                Logger.error('Recorder ' + rec.name + ' failed to record data sent!', e);
            }
        }
    }
    protected async recordDataReceivedOrigin(testname: string, variable: ITestVar) {
        if (Array.isArray(variable)) {
            for (const item of variable) {
                await this.recordDataReceivedOrigin(testname, item);
            }
            return;
        }
        // variable is not an array
        for (const rec of this._recorder) {
            try {
                rec.recordDataReceivedOrigin(testname, variable);
            } catch (e) {
                Logger.error('Recorder ' + rec.name + ' failed to record data received!', e);
            }
        }
    }
    protected async recordDataSendDriver(testname: string, variable: ITestVar | ESchemaGroup, byteLength: number) {
        for (const rec of this._recorder) {
            const sg = typeof variable === 'object' ? variable.schemaGroup : variable;
            try {
                rec.recordDataSendDriver(testname, sg, byteLength);
            } catch (e) {
                Logger.error('Recorder ' + rec.name + ' failed to record data sent to driver!', e);
            }
        }
    }
    protected async recordDataReceivedDriver(testname: string, schemaGroup: ESchemaGroup, byteLength: number) {
        for (const rec of this._recorder) {
            try {
                rec.recordDataReceivedDriver(testname, schemaGroup, byteLength);
            } catch (e) {
                Logger.error('Recorder ' + rec.name + ' failed to record data received from driver!', e);
            }
        }
    }
    //#endregion RECORDER


    //#region DRIVER-SERVICE

    public addDriver(service: ITestDriver | ITestDriver[]) {
        if (Array.isArray(service)) {
            service.forEach(s => this.addDriver(s));
            return;
        }
        if (!service.belongsToController(this.name)) {
            return;
        }
        if (!this._driverServices.has(service.name)) {
            const sub = service.onReceive.subscribe(received => {
                const group = received.schemaGroup;
                const dataReceived = received.data;
                Logger.debug('Controller: ' + this.name + ': Received variable from group : ' + group);
                this.emitReceived(group, dataReceived);
            });
            this._driverServices.set(service.name, { service: service, subscription: sub });
        } else {
            Logger.warn('Controller `' + this.name + '`: Failed to add driver `' + service.name + '`! Driver with same name was added before!');
        }
    }

    public removeDriver(name: string) {
        const adapt = this._driverServices.get(name);
        if (adapt) {
            adapt.subscription.unsubscribe();
            this._driverServices.delete(name);
            return adapt.service;
        }
    }

    public hasDriver(name: string) {
        return this._driverServices.has(name);
    }

    public getDriver(name: string) {
        const drv = this._driverServices.get(name);
        if (drv) {
            return drv.service;
        } else {
            throw new Error('Driver `' + name + '` does not exist at controller ' + this.name + '!');
        }
    }

    public getDriverNames() {
        const names = new Array<string>();
        this._driverServices.forEach((v, k) => {
            names.push(k);
        });
        return names;
    }

    public getDrivers() {
        const drvrs = new Array<ITestDriver>();
        this._driverServices.forEach((v, k) => {
            drvrs.push(v.service);
        });
        return drvrs;
    }
    //#endregion DRIVER-SERVICE

    //#region Implementation of ITestAble
    // Interface-methods for TRIGGER

    public belongsToTrigger(triggerName: string): boolean {
        if (!this._settings) {
            throw new Error('Unable to compare trigger-names, settings are missing!');
        }
        if (typeof this._settings.trigger === 'string') {
            return this._settings.trigger === triggerName;
        } else {
            return this._settings.trigger.some(t => t === triggerName);
        }
    }

    public async onEvent(testname: string, event: import("../trigger/test-event.model").ETestAction, data?: ITestVar | undefined): Promise<void> {
        if (this.testname && this.testname !== testname) {
            throw new Error('Failed to process event of test `' + testname + '`! Another test `' + this.testname + '` is currently ongoing!');
        }
        switch (event) {
            case ETestAction.PREPARE:
                this.testname = testname;
                this.checkDriver();
                this.checkRecorder();
                await this.prepareRecorder(testname);
                await this.startRecorder(testname);
                break;
            case ETestAction.CONNECT:
                await this.connect(testname);
                break;
            case ETestAction.SEND:
                if (data) {
                    this.dataCollector.sendRequest(testname, data);
                    //await this.send(testname, data);
                } else {
                    throw new Error('Controller: onEvent: ' + event + '! Data missing!');
                }
                break;
            case ETestAction.PREPARE_DISCONNECT:
                await this.prepareDisconnect(testname);
                break;
            case ETestAction.DISCONNECT:
                await this.disconnect(testname);
                break;
            case ETestAction.EXIT:
                await this.stopRecorder(testname);
                this.testname = '';
                break;
            default:
                throw new Error('Controller: onEvent: ' + event + ' unkown or not implemented!');
        }
    }

    /**
     * Returns the subject for onReceived-event.
     */
    public get onReceive() {
        return this._onReceive;
    }
    //#endregion 


    protected async connect(testname: string) {
        this.connectToDrivers(testname);
    }
    protected async connectToDrivers(testname: string) {
        Logger.debug('Controller' + this.name + ': Start to connect');
        this._driverServices.forEach(async (d) => {
            try {
                await d.service.connectForTest();
            } catch (err) {
                Logger.error('Failed to connect driver ' + d.service.name + '!', err);
            }
        });
    }

    protected async send(dataCollectorRecords: IDataCollectorRecord | IDataCollectorRecord[]): Promise<void>;
    protected async send(testname: string, testVar: import("../trigger/test-event.model").ITestVar, comVar?: ComVar<any>): Promise<void>;
    protected async send(testnameOrRecordOrRecords: string | object, testVar?: import("../trigger/test-event.model").ITestVar, comVar?: ComVar<any>): Promise<void> {
        if (typeof testnameOrRecordOrRecords === 'string') {
            const testname = testnameOrRecordOrRecords;
            // Call method using first method-signature with testname, testVar and comVar
            if (!testVar) {
                throw new Error('Send variable for test ' + testname + ' failed! TestVar is missing!');
            }
            if (!comVar) {
                comVar = RuntimeVarFactory.CREATE(testVar);
            }
            Logger.debug('Controller' + this.name + ': Send variable ' + (testVar && testVar.name ? testVar.name : '[undefined]'));
            await this.recordDataSendOrigin(testname, testVar);
            const buffer = this.packData(comVar);
            await this.recordDataSendDriver(testname, testVar, buffer.byteLength);
            await this.sendToDrivers(testVar, buffer);
        } else if (Array.isArray(testnameOrRecordOrRecords)) {
            const records = testnameOrRecordOrRecords as IDataCollectorRecord[];
            if (records.length > 0) {
                const testname = records[0].testname;
                const schemaGroup = records[0].testVariable.schemaGroup;
                Logger.debug('Controller' + this.name + ': Send collection of variables Length: ' + (records.length ? records.length : '[0]'));
                for (const r of records) {
                    await this.recordDataSendOrigin(r.testname, r.testVariable);
                }
               
                const buffer = this.packData(records.map(r => r.comVariable));
                await this.recordDataSendDriver(testname, schemaGroup, buffer.byteLength);
                await this.sendToDrivers(schemaGroup, buffer);
            }
        } else {
            const rec = testnameOrRecordOrRecords as IDataCollectorRecord;
            Logger.debug('Controller' + this.name + ': Send variable from collector: ' + (rec && rec.testVariable.name ? rec.testVariable.name : '[undefined-or-empty]'));
            await this.recordDataSendOrigin(rec.testname, rec.testVariable);
            const buffer = this.packData(rec.comVariable);
            await this.recordDataSendDriver(rec.testname, rec.testVariable, buffer.byteLength);
            await this.sendToDrivers(rec.testVariable, buffer);
        }
    }


    
    protected packData(varToSend: any): Buffer {
        return DataPackerOperator.GET().packData(varToSend);
    }

    protected unpackData(receivedData: Buffer): ComVar<any> | ComVar<any>[] {
        return DataPackerOperator.GET().unpackData(receivedData);
    }

    protected async sendToDrivers(testVar: ITestVar | ESchemaGroup, buffer: Buffer) {
        this._driverServices.forEach(async (d) => {
            try {
                await d.service.sendTest(testVar, buffer);
            } catch (err) {
                Logger.error('Failed to send via driver ' + d.service.name + '!', err);
            }
        });
    }

    protected async prepareDisconnect(testname: string) {
        this.dataCollector.cleanUp();
    }

    protected async disconnect(testname: string) {
        this.disconnectToDrivers(testname);
    }

    protected async disconnectToDrivers(testname: string) {
        Logger.debug('Controller ' + this.name + ': Start to disconnect');
        this._driverServices.forEach(async (d) => {
            try {
                await d.service.disconnectForTest();
            } catch (err) {
                Logger.error('Failed to disconnect' + d.service.name + '!', err);
            }
        });
    }


    protected abstract emitReceived(group: ESchemaGroup, data: Buffer);


    /**
     * Check drivers
     * Throws an error in case of major exception.
     * Logs messages (warning) in case of minor problem.
     */
    protected checkDriver() {
        if (!(this._driverServices && this._driverServices.size > 0)) {
            Logger.warn(this.name + ': Driver not applied!');
        }
    }
    /**
     * Check recorders
     * Throws an error in case of major exception.
     * Logs messages (warning) in case of minor problem.
     */
    protected checkRecorder() {
        if (!(this._recorder && this._recorder.length > 0)) {
            Logger.warn(this.name + ': Recorder not applied!');
        }
    }



}