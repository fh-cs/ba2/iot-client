import { EControllerType } from "../controller/controller-type.enum";
import { EDataPackerType } from "./data-packer/datapacker-type.enum";
import { IDataCollectorSettings } from "./data-collector/data-collector-settings";
import { DataPackerSettings } from "./data-packer/datapacker-settings";

export interface IControllerSettings {
    /**
     * Name of this adapter
     */
    name: string;
    /**
     * Name of test-trigger(s) this controller belongs too.
     * Use string-array if this adapter belongs to multiple triggers
     */
    trigger: string | string[];
    /**
     * Type of test-adapter to be used
     */
    type: EControllerType;
    /**
     * Type of data-formater used to convert com-vars to buffer and return.
     * If absent, JSON-DataPacker is used.
     */
    dataPacker?: EDataPackerType | DataPackerSettings | EDataPackerType[] | DataPackerSettings[];
    /**
     * 
     */
    dataCollector?: IDataCollectorSettings;
    /**
     * Filepath to the reference-list.
     */
    referenceList?: string;
}