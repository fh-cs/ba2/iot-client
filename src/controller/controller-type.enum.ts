export enum EControllerType {
    DEV_CONTROLLER = 'dev-controller',
    SVR_CONTROLLER = 'svr-controller',
}