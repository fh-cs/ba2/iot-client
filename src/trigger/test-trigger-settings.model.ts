export interface ITestTriggerSettings {
    name: string,
    enable: boolean,
    testFile: string,
    startTime: string,
    outputDirectory: string
}