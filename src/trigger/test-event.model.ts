import { ESchemaGroup } from "../common/schema-group.enum";

export interface ITestEvent {
    /**
     * Time in seconds from start this event should occur
     */
    time: number;
    /**
     * The action
     */
    action: ETestAction;
    /**
     * Variable send to test-service
     */
    var?: ITestVar;
}

export interface ITestVar {
    schemaGroup: ESchemaGroup;
    name: string;
    value?: number;
}

export enum ETestAction {
    /**
     * First event called.
     * Prepare auxiliaries and get ready for connecting
     */
    PREPARE = 'prepare',
    CONNECT = 'connect',
    SEND = 'send',
    /**
     * Last event before disconnect.
     * Get open work done before disconnect-event appears.
     */
    PREPARE_DISCONNECT = 'prepare-disconnect',
    DISCONNECT = 'disconnect',
    EXIT = 'exit'
}


export class TestEvent implements ITestEvent {
    public time: number;
    public action: ETestAction; 
    public var?: ITestVar;
    private timer: NodeJS.Timeout | undefined;

    constructor(fileData: ITestEvent) {
        this.time = fileData.time;
        this.action = fileData.action;
        this.var = fileData.var
    }

    setTimer() {
        return new Promise<{action: ETestAction, var?: ITestVar}>( (resolve, reject) => {
            this.timer = setTimeout(() => {
                const e = { action: this.action, var: this.var};
                this.timer = undefined;
                resolve(e);
            }, this.time * 1000);
        });
    }

    stopTimer() {
        if (this.timer) {
            clearTimeout(this.timer);
        }
    }
    
}