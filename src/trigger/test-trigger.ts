import { ITestTriggerSettings } from "./test-trigger-settings.model";
import * as fs from 'fs';
import { TestEvent, ETestAction, ITestVar } from "./test-event.model";
import { Logger } from "../common/logger/logger";
import { Subject, Subscription } from "rxjs";

export interface ITestTriggerAbleService {
    name: string;
    /**
     * Returns true if the trigger-able-service belongs to the provided trigger-name.
     */
    belongsToTrigger(triggerName: string): boolean;
    onEvent(testname: string, event: ETestAction, data?: ITestVar): Promise<void>;
    onReceive: Subject<ITestVar>;
}

export class TestTrigger {
    private static _service = new Map<string, TestTrigger>();

    private _settings?: ITestTriggerSettings;
    private events?: TestEvent[];
    private _controllers = new Map<string, { service: ITestTriggerAbleService, subscription: Subscription }>();

    public get name() {
        return this._settings && this._settings.name ? this._settings.name : '';
    }

    private constructor(settings?: ITestTriggerSettings) {
        if (settings) {
            this._settings = settings;
        }
    }

    public static getService(nameOrSettings: string | ITestTriggerSettings) {
        let name: string;
        let settings: ITestTriggerSettings | undefined;
        if (typeof nameOrSettings === 'string') {
            name = nameOrSettings;
            settings = undefined;
        } else {
            name = nameOrSettings.name;
            settings = nameOrSettings;
        }
        const s = this._service.get(name);
        if (!s) {
            const t = new TestTrigger(settings);
            this._service.set(name, t);
            return t;
        } else {
            return s;
        }
    }

    public setSettings(settings: ITestTriggerSettings) {
        this._settings = settings;
    }
    public getSettings() {
        return this._settings;
    }
    public addController(controller: ITestTriggerAbleService | ITestTriggerAbleService[]) {
        if (Array.isArray(controller)) {
            controller.forEach(a => this.addController(a));
            return;
        }
        if (!controller.belongsToTrigger(this.name)) {
            return;
        }
        if (!this._controllers.has(controller.name)) {
            const sub = controller.onReceive.subscribe(varReceived => {
                Logger.debug('Tester: ' + this.name + ': Received: ' + varReceived.schemaGroup + ':' + varReceived.name +
                    ' Value: ' + (varReceived.value !== undefined ? varReceived.value : '[undefined]'));
            });
            this._controllers.set(controller.name, { service: controller, subscription: sub });
        }
    }

    public getControllers() {
        const list = new Array<ITestTriggerAbleService>();
        this._controllers.forEach( (v, k) => list.push(v.service));
        return list;
    }

    public removeController(name: string) {
        const ctrl = this._controllers.get(name);
        if (ctrl) {
            ctrl.subscription.unsubscribe();
            this._controllers.delete(name);
            return ctrl.service;
        }
    }

    public async start() {
        if (!this._settings) {
            throw new Error("Tester: Settings missing or not set!");
        }
        if (!this._settings.enable) {
            Logger.warn('Trigger ' + this.name + ' not enabled!');
            return;
        }
        try {
            await this.loadTestFile();
            await this.waitForStart();
            await this.setEvents();
        } finally {
            this.stopWaiting();
            this.stopEventTimer();
        }
    }

    private async loadTestFile() {
        if (this._settings) {
            const fb = await fs.readFileSync(this._settings.testFile).toString();
            const rows = fb.split('\n');
            const events = new Array<TestEvent>();
            rows.forEach((r, rowIndex) => {
                // Comment line in file
                if (r.trimLeft().startsWith('#')) {
                    return;
                }
                //time[seconcs];action;schemaGroup;varname;value;
                const cols = r.split(';');
                const t = Number.parseInt(cols[0]);
                const a = cols[1] as ETestAction;
                let v: ITestVar | undefined;
                if (a === ETestAction.SEND) {
                    if (cols.length < 5) {
                        throw new Error('Load-Test-File row ' + rowIndex + ': Send properties are missing!');
                    }
                    v = {
                        schemaGroup: cols[2].trim(),
                        name: cols[3].trim(),
                        value: Number.parseFloat(cols[4])
                    } as ITestVar;
                }
                //Add to event-list
                events.push(new TestEvent({
                    time: t,
                    action: a,
                    var: v
                }));

            });
            this.events = events;
            let countSendData = 0;
            let countSendNotify = 0;
            let countSendMethod = 0;
            let countSendConfig = 0;
            this.events.forEach( v => {
                if (v.var) {
                    if (v.var.schemaGroup === 'notify') { countSendNotify++; }
                    if (v.var.schemaGroup === 'data') { countSendData++; }
                    if (v.var.schemaGroup === 'method') { countSendMethod++; }
                    if (v.var.schemaGroup === 'config') { countSendConfig++; }
                }
            });
            Logger.debug('Tester ' + this.name + ': Events loaded! Send data: ' + countSendData + ' notify: ' + countSendNotify + ' method: ' + countSendMethod + ' config: ' + countSendConfig);
            return true;
        }
        return false;
    }


    private waitInterval: NodeJS.Timeout | undefined;
    /**
     * Wait until start-time is reached.
     * Returns the date-time at resolve.
     */
    private async waitForStart() {
        return new Promise<Date>((resolve, reject) => {
            if (this._settings) {
                const dateStart = new Date(Date.parse(this._settings.startTime));
                if (dateStart.getTime() < Date.now()) {
                    reject(new Error('Tester: ' + this.name + ' Start time in past'));
                }
                Logger.info('Tester: ' + this.name + ': Start test at ' + dateStart.toString());
                let counter = 0;
                this.waitInterval = setInterval(() => {
                    if (Date.now() >= dateStart.getTime()) {
                        Logger.debug('waiting done');
                        if (this.waitInterval) {
                            clearInterval(this.waitInterval);
                            this.waitInterval = undefined;
                        }
                        resolve(new Date());
                    } else {
                        if (counter % 100 === 0) {
                            Logger.debug('Still waiting!');
                        }
                        counter++;
                    }
                }, 100);
            }
        });
    }

    public stopWaiting() {
        if(this.waitInterval) {
            clearInterval(this.waitInterval);
        }
    }

    private setEvents() {
        return new Promise((resolve, reject) => {
            if (!this.events) {
                throw new Error('Tester: ' + this.name + ' Events missing!');
            }
            this.events.forEach(e => {
                e.setTimer().then(event => {
                    if(this.emitAction(event)) {
                        //emitAction returns true if action `exit` was called
                        resolve();
                    }
                });
            });
        });
    }

    public stopEventTimer() {
        if (this.events) {
            this.events.forEach(e => {
                try {
                    e.stopTimer();
                } catch(err) {
                    Logger.error('Failed to stop timer of event ' + this.name + ' ' + e.time + '!', err);
                }
            });
        }
    }

    private emitAction(event: { action: ETestAction, var?: ITestVar }) {
        if (!(event && event.action)) {
            return false;
        }
        Logger.debug('Emit event: ' + new Date().toTimeString() + ' --> ' + event.action + (event.var ? (event.var.name ? ' Var: ' + event.var.name : '') : ''));
        
        this._controllers.forEach( async (s) =>  {
            try {
                await s.service.onEvent(this.name, event.action, event.var);
            } catch(error) {
                Logger.error('Forward event to ' + s.service.name + ' failed!', error);
            }            
        });
        return event.action === ETestAction.EXIT;
    }

}