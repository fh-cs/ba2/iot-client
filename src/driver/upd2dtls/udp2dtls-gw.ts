import { Driver } from '../driver';
import { IUdp2DtlsGatewaySettings } from './udp2dtls-gw-settings';
import { Logger } from '../../common/logger/logger';
import { ESchemaGroup } from '../../common/schema-group.enum';
import { SocketWrapper } from '../mqtt-sn/socket-wrapper';
import { Socket, createSocket } from "dgram";
import { AddressInfo } from 'net';

export class Udt2DtlsGateway extends Driver {

    private dtlsSocket: SocketWrapper | undefined;
    private udpSocket: Socket | undefined;
    private udpSender: AddressInfo | undefined;
    private dtlsConnected = false;
   

    constructor(settings?: IUdp2DtlsGatewaySettings) {
        super(settings);
    }

    public getSettings() {
        return super.getSettings() as IUdp2DtlsGatewaySettings;
    }
    /**
     * At connect, prepare upd-socket and open udp-port for listening
     */
    public async connectForTest() {
        await this.initUpdSocket();
    }

    /**
     * Initialize UDP-Socket and bind to UDP-Port as set in config.
     * If socket-instance exists, close connection first.
     */
    private async initUpdSocket() {
        await this.closeUdpSocket();

        this.udpSocket = createSocket('udp4');
        this.udpSocket.on('error', (err) => {
            this.udpOnError(err);
        });
        this.udpSocket.on('connect', async () => {
            this.udpOnConnect();
        });
        this.udpSocket.on('message', (msg, info) => {
            this.udpSender = info;
            this.udpOnReceive(msg);
        });
        this.udpSocket.on('close', async () => {
            this.udpOnClose();
        });
        this.udpSocket.bind(this.getSettings().udpPort);
    }

    private closeUdpSocket() {
        return new Promise<void>( (resolve, reject) => {
            if (this.udpSocket) {
                this.udpSocket.close(() => {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    /**
     * Connects to the DTLS-Partner (server) and waits until connected.
     * Throws an error if waiting-time for connect feedback exceeded.
     */
    private async connectDtlsSocket() {
        await this.closeDtlsSocket();
        Logger.debug('SETTINGS UDP-2-DTLS:\n' + JSON.stringify(this.getSettings()));
        this.dtlsSocket = new SocketWrapper('dtls',
            this.getSettings().remoteAddress,
            this.getSettings().remoteDtlsPort,
            undefined,  //local port - determine port by os
            this.getSettings().dtlsCertFile,
            this.getSettings().dtlsKeyFile);
        this.dtlsSocket.event.on('connect', () => this.dtlsOnConnect());
        this.dtlsSocket.event.on('message', (data) => this.dtlsOnReceive(data));
        this.dtlsSocket.event.on('close', () => this.dtlsOnClose());
        this.dtlsSocket.event.on('error', (error) => this.dtlsOnError(error));
        await this.dtlsSocket.connect();
        await this.waitForDtlsConnected();
    }

    private async closeDtlsSocket() {
        if (this.dtlsSocket) {
            await this.dtlsSocket.close();
        }
    }

    

    private dtlsOnConnect() {
        this.dtlsConnected = true;
        Logger.info('UDP2DTLS-GW: DTLS connected to remote partner!');
    }

    private async dtlsOnReceive(data: any) {
        if (this.udpSocket) {
            await this.udpSend(data);
            
        } else {
            Logger.warn('UDP2DTLS-GW: DTLS received data but UDP-Socket not ready!');
        }
    }

    private dtlsOnClose() {
        this.dtlsConnected = false;
        Logger.info('UDP2DTLS-GW: DTLS Connection closed to remote partner!');
    }

    private dtlsOnError(error: any) {
        Logger.error('DTLS-Socket Error! ' + JSON.stringify(error));
    }

    
    private udpOnConnect() {
        Logger.info('UDP2DTLS-GW: Is listening on UDP-Port: ' + this.getSettings().udpPort);
    }


    /**
     * Receive data from UDP and forward to DTLS-partner
     */
    private async udpOnReceive(data: any) {
        if (!this.dtlsSocket) {
            await this.connectDtlsSocket();
        }

        if (this.dtlsSocket) {
            await this.dtlsSocket.send(data);
            Logger.debug('UDP2DTLS-GW: UDP --> DTLS:\n' + JSON.stringify(data));
        } else {
            Logger.warn('UDP2DTLS-GW: DTLS received data but UDP-Socket not ready!');
        }
    }

    private async udpSend(data) {
        return new Promise<void>( (resolve, reject) => {
            if (this.udpSocket) {
                if (this.udpSender) {
                    this.udpSocket.send(data, this.udpSender.port, this.udpSender.address, (error, bytes) => {
                        Logger.debug('UDP2DTLS-GW: DTLS --> UPD:\n' + JSON.stringify(data));
                        resolve();
                    });
                } else {
                    reject(new Error('Send data to UDP-Parnter failed, has not received any message from UDP-Partner until now!'));
                }
            } else {
                reject(new Error('Send data to UDP-Parnter failed, UDP-Socket not initialized!'));
            }
        });
    }



    private udpOnClose() {
        Logger.info('UDP2DTLS-GW: UDP Connection closed to UDP-partner!');
    }

    private udpOnError(error: any) {
        Logger.error('UDP-Socket Error! ' + JSON.stringify(error));
    }

    public async sendTest(testVarOrSchemaGroup: import("../../trigger/test-event.model").ITestVar | ESchemaGroup, buffer: Buffer) {
        // this is gateway
        // in not sending any messages
    }

    public async disconnectForTest() {
        this.closeUdpSocket();
        this.closeDtlsSocket();
    }


     /**
     * Waits until `dtlsConnected` is true.
     * @param timeout Maximum time for waiting in msec. Default is 5000msec.
     */
    private async waitForDtlsConnected(timeout?: number) {
        return new Promise<void>( (resolve, reject) => {
            const endTime = Date.now() + (timeout ? timeout : 5000);
            if (this.dtlsConnected) {
                resolve();
            } else {
                const sub = setInterval(() => {
                    if (this.dtlsConnected) {
                        clearInterval(sub);
                        resolve();
                    } else if (Date.now() > endTime) {
                        clearInterval(sub);
                        reject(new Error('Time to wait for DTLS-Connected exceeded!'))
                    }
                }, 100);
            }
        });
    }
}