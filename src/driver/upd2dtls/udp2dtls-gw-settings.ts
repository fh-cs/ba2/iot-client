import { IDriverSettings } from "../driver-settings.model";

export interface IUdp2DtlsGatewaySettings extends IDriverSettings {


    /**
     * Port this gateway is receiving UDP-messages
     */
    udpPort: number;
    /**
     * Hostname of the remote gateway (DTLS-Server)
     */
    remoteAddress: string;
    /**
     * Port the remote gateway is listening for incoming DTLS-messages
     */
    remoteDtlsPort: number;


  
    /**
     * Path to the DTLS-private-key-File
     */
    dtlsKeyFile?: string;
    /**
     * Path to the DTLS-Certificate-File
     */
    dtlsCertFile?: string;
    /**
     * Path to the CA-File for Encryption
     */
    dtlsCaFile?: string;
    
}
