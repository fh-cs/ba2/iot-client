import { IDriverSettings } from "../driver-settings.model";

export interface IMqttSnGatewaySettings extends IDriverSettings {


    
    /**
     * Port address the gateway is listening for mqtt-sn.
     * If not set or 0, default port is used.
     */
    port?: number;
    /**
     * If set, DTLS will be used.
     */
    useSSL?: boolean;
  
    /**
     * Path to the DTLS-private-key-File
     */
    dtlsKeyFile?: string;
    /**
     * Path to the DTLS-Certificate-File
     */
    dtlsCertFile?: string;
    /**
     * Path to the CA-File for Encryption
     */
    dtlsCaFile?: string;
    /**
     * URL of MQTT-Broker
     * 
     * e.g.: \
     * mqtt://localhost:3883\
     * mqtts://localhost:8883
     */
    brokerUrl: string;
    /**
     * Path to the Public-Key-File from MQTT-Broker
     */
    brokerKeyFile?: string;
    /**
     * Path to the Certificate-File from MQTT-Broker
     */
    brokerCertFile?: string;
    
}
