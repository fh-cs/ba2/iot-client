import { Driver } from "../driver";
import { IMqttSnGatewaySettings } from "./mqtt-sn-gw-settings";
import { ESchemaGroup } from "../../common/schema-group.enum";
import { Logger } from "../../common/logger/logger";

export class MqttSnGateway extends Driver {

    private mqttSnGW;
    constructor(settings?: IMqttSnGatewaySettings) {
        super(settings);
    }


    public async connectForTest() {
        const logMethods = {
            debug: (msg) => Logger.debug(msg),
            info: (msg) => Logger.info(msg),
            warn: (msg) => Logger.warn(msg),
            error: (msg, err) => Logger.error(msg, err)
        };
        const settings = this.getSettings() as IMqttSnGatewaySettings;
        const fs = require('fs');
        const MQTTSNGW = require('mqttsngw');
        const Core = require('mqttsngw-core');
        const DTLS = require('mqttsngw-dtls');
        const Broker = require('mqttsngw-mqttbroker');
        this.mqttSnGW = MQTTSNGW({
            log: logMethods                             // Will debug log all events on the event bus
        }).attach(DTLS({
            log: logMethods,                            // Log all events from the DTLS module
            bind: settings.port,
            key: fs.readFileSync(settings.dtlsKeyFile),  // GW's private key
            cert: fs.readFileSync(settings.dtlsCertFile), // GW's certificate
            ca: settings.dtlsCaFile ? fs.readFileSync(settings.dtlsCaFile) : undefined    // CA certificate for checking sensor certificates
          })).attach(Core({
            log: logMethods                             // Log all events from the Core module
          })).attach(Broker({
            log: logMethods,                            // Log all events from the MQTT broker module
            broker: (clientId) => {              // Factory for every MQTT connection
              return {
                url: settings.brokerUrl,
                key: fs.readFileSync(settings.brokerKeyFile),
                cert: fs.readFileSync(settings.brokerCertFile)
              };
            }
          })) 
          this.mqttSnGW.start();
    }
    public async sendTest(testVarOrSchemaGroup: import("../../trigger/test-event.model").ITestVar | ESchemaGroup, buffer: Buffer) {
        // this is gateway
        // in not sending any messages
    }

    public async disconnectForTest() {
        if (this.mqttSnGW) {
            this.mqttSnGW.shutdown();
        }
    }
}