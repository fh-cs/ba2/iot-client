export interface IConnectionState {
    connected: boolean;
}

export class ConnectionState implements IConnectionState {
    connected = false;
    constructor(state: boolean | IConnectionState) {
        if (typeof state === 'boolean') {
            this.connected = state;
        } else {
            this.connected = state.connected;
        }
        
    }
}