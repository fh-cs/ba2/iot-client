import { IDriverSettings } from "./driver-settings.model";
import { ITestDriver } from "../controller/controller";
import { Subject } from "rxjs";
import { ESchemaGroup } from "../common/schema-group.enum";

export abstract class Driver implements ITestDriver {
    /**
     * Identifier for that driver
     */
    public get name() {
        return this.getSettings().name;
    }

    
    private _settings: IDriverSettings | undefined;
    /**
     * Returns the settings for the driver.
     * Throws an error in case of missing settings.
     */
    public getSettings() {
        if (this._settings) {
            return this._settings;
        } else {
            throw new Error('Settings for driver are not set!');
        }
        
    }
    /**
     * Set settings or replace existing with this one.
     */
    public setSettings(settings: IDriverSettings) {
        this._settings = settings;
    }
    /**
     * Returns true if settings were applied and available
     */
    public hasSettings() {
        return this._settings !== undefined;
    }

    constructor(settings?: IDriverSettings) {
        if (settings) {
            this._settings = settings;
        }
        
    }

//#region Implementation ITestDriver
    public belongsToController(controllerName: string): boolean {
        const settings = this.getSettings();
        if (!settings.controller) {
            throw new Error('Controller missing in settings!');
        }
        if (typeof settings.controller === 'string') {
            return settings.controller === controllerName;
        } else {
            return settings.controller.some(a => a === controllerName);
        }
    }
    
    public abstract connectForTest(): Promise<void>;
    public abstract sendTest(schemaGroup: ESchemaGroup, buffer: Buffer): Promise<void>;
    public abstract sendTest(testVar: import("../trigger/test-event.model").ITestVar, buffer: Buffer): Promise<void>;
    public abstract sendTest(testVarOrSchemaGroup: import("../trigger/test-event.model").ITestVar | ESchemaGroup, buffer: Buffer): Promise<void>;
    public abstract disconnectForTest(): Promise<void>;
    
    public _onReceive = new Subject<{schemaGroup: ESchemaGroup, data: Buffer}>();
    public get onReceive() {
        return this._onReceive;
    }
//#endregion
}