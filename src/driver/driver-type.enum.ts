export enum EDriverType {
    DEVICE_MQTT_JSON = 'dev-mqtt-json',
    SERVER_MQTT_JSON = 'srv-mqtt-json',
    CLIENT_COAP = 'clt-coap',
    SERVER_COAP = 'srv-coap',
    MQTT_SN_GW = 'mqtt-sn-gw',
    MQTT_SN_DEVICE = 'mqtt-sn-device',
    UDP2DTLS_GW = 'udp-2-dtls-gw'
}