import { MqttBufferSubscription } from "../mqtt/mqtt-subscription";
import { ESchemaGroup } from "../../common/schema-group.enum";
import { Logger } from "../../common/logger/logger";
import { MqttSnDriver } from "./mqtt-sn-driver";


export class DeviceMqttSnDriver extends MqttSnDriver {
    protected getTopicForPublish(schemaGroup: ESchemaGroup): string {
        return this.getTopic('srv', schemaGroup, 'arr');
    }
    protected getTopicForSubscription(schemaGroup: ESchemaGroup): string {
        return this.getTopic('dev', schemaGroup, 'arr', true);
    }

    protected async addSubscriptions() {
        // add subscription for CONFIG (from server)
        const subConfig = new MqttBufferSubscription({
            topic: this.getTopicForSubscription(ESchemaGroup.CONFIG),
            callback: (topic, msg) => {
                this.onReceive.next({ schemaGroup: ESchemaGroup.CONFIG, data: msg });
            }
        });
        await this.addSubscriber(subConfig);
        Logger.debug('Has added subscription for topic `' + subConfig.topic + '`');

        // add subscription for METHOD (from server)
        const subMethod = new MqttBufferSubscription({
            topic: this.getTopicForSubscription(ESchemaGroup.METHOD),
            callback: (topic, msg) => {
                this.onReceive.next({ schemaGroup: ESchemaGroup.METHOD, data: msg });
            }
        });
        await this.addSubscriber(subMethod);
        Logger.debug('Has added subscription for topic `' + subMethod.topic + '`');
    }
}