import { Driver } from "../driver";
import { IMqttConnectionOptions } from "../mqtt/mqtt-settings";
import { IMqttSnSettings } from "./mqtt-sn-driver-settings";
import { Logger } from "../../common/logger/logger";
import { MqttSubscription } from "../mqtt/mqtt-subscription";
import { ESchemaGroup } from "../../common/schema-group.enum";
import { MqttTopic } from "../mqtt/mqtt-topic";
import { IClientPublishOptions, ClientSubscribeCallback, IClientSubscribeOptions } from "mqtt";
import { EventEmitter } from "events";
import { SocketWrapper } from "./socket-wrapper";
import { EMqttSnReturnCode, IMqttSnConnectData, IMqttSnResponseMessage, IMqttSnPublishData, IMqttSnData, IMqttSnMessageData, IMqttSnPublishQoS2Data, IMqttSnWillTopicData, IMqttSnWillMessageData, IMqttSnPingData, IMqttSnSubscribeData } from "./mqtt-sn-data";
import { BooleanVar } from "../../controller/rt-variable/runtime-var.model";
import { DataPackerOperator } from "../../controller/data-packer/datapacker-operator";

export abstract class MqttSnDriver extends Driver {

    private checkKeepAliveTimer?: NodeJS.Timeout;

    private topicReferences = new Array<{ name: string, id: number }>();

    /**
     * Events
     * - connect: Emitted after receiving conn-ack. cb: (void) => void;
     * - received: Has received a msg (after parser). cb: (data: IMqttSnData) => void;
     * - close: After connection closed. cb: (void) => void;
     */
    public on = new EventEmitter();

    private _socket?: SocketWrapper;
    protected get socket() {
        if (!this._socket) {
            const set = this.getSettings();
            this._socket = new SocketWrapper(
                set.useSSL ? 'dtls' : 'udp4', 
                set.gwAddress, 
                this.getSettings().gwPort, 
                set.listenPort,
                this.getSettings().certFile,
                this.getSettings().privateKeyFile,
                this.getSettings().pskIdentity,
                this.getSettings().pskSecret);
            this._socket.event.on('error', (err) => {
                Logger.error('Received error from socket! Close socket!', err);
                if (this._socket) {
                    this._socket.close();
                }
            });
            this._socket.event.on('connect', async () => {
                // event will be emit after receiving acknowledge (in connect)
            });
            this._socket.event.on('close', async () => {
                this.setDisconnected();
                this.on.emit('close');
            });
            this._socket.event.on('message', async (buffer) => {
                const msg = await this.parse(buffer);

                // get payload length
                let payloadLen = 0;
                const pubMsg = msg as IMqttSnPublishData;
                if (pubMsg.payload) {
                    payloadLen = Buffer.isBuffer(pubMsg.payload) ? pubMsg.payload.byteLength : new Buffer(pubMsg.payload).byteLength;
                }
                Logger.debug('MQTT-SN: Sent message to gateway! Message-Length: ' + buffer.byteLength + 'byte' + (payloadLen ? ' Payload-Length: ' + payloadLen + 'byte' : ''), JSON.stringify(msg));

                this.on.emit('received', msg);
                await this.receivedData(msg);
            });
        }
        return this._socket;
    }
    private maxRetries = 3;


    protected get packet() {
        return require('mqttsn-packet');
    }

    private parse(buffer: Buffer) {
        return new Promise<IMqttSnData>((resolve, reject) => {
            const parser = this.packet.parser();
            parser.on('packet', (msg) => {
                resolve(msg);
            });
            parser.parse(buffer);
        });
    }

    public get gwAddress() {
        return this.getSettings().gwAddress || 'localhost';
    }
    public get gwPort() {
        return this.getSettings().gwPort || 3688;
    }


    private _lastAdvertiseMessage?: { gwId: number, duration: number } = undefined;
    public get lastAdvertiseMessage() {
        return this._lastAdvertiseMessage;
    }


    private subscriptions = new Array<MqttSubscription<any>>();


    constructor(settings?: IMqttSnSettings) {
        super(settings);

        this.prepareTopicReferences();

        this.checkKeepAliveTimer = setInterval(async () => {
            await this.checkKeepAlive();
        }, 1000);
    }

    public getSettings() {
        return super.getSettings() as IMqttSnSettings;
    }

    /**
     * Time (in msec since 01.01.1970) when driver received last message
     */
    private lastContact?: number;
    /**
     * Returns the time the next ping is required (in msec since 01.01.1970)
     */
    protected get nextPingTime() {
        const keepAlive = this.getSettings().keepalive;
        const msecKeepAlive = keepAlive ? keepAlive * 1000 : 60000;
        return (this.lastContact ? this.lastContact : Date.now()) + msecKeepAlive;
    }
    /**
     * Returns the communication timeout in msec for ack-packets
     */
    protected get comTimeout() {
        return 1000;
    }
    /**
     * Returns true if connected
     */
    public get isConnected() {
        return this.socket && this.lastContact ? Date.now() <= (this.nextPingTime + this.comTimeout) : false;
    }

    private setConnected() {
        this.lastContact = Date.now();
    }

    private extendConnected() {
        if (this.lastContact) {
            this.lastContact = Date.now();
        }
    }

    private setDisconnected() {
        this.lastContact = undefined;
    }


    protected abstract async addSubscriptions(): Promise<void>;
    protected abstract getTopicForPublish(schemaGroup: ESchemaGroup): string;
    protected abstract getTopicForSubscription(schemaGroup: ESchemaGroup): string;
    /**
     * Returns the topic in following format:\
     * - if topicToAppend is provided:\
     *  iot1/[device-group-id]/[device-id]/[topicToAppend]
     * - if topicToAppend not provided:\
     * iot1/[device-group-id]/[device-id]
     * @param forSubscription 
     * Appends a multiple-wildcard (#) at the end of the topic for subscription
     */
    public getTopic(topicToAppend?: string): string;
    public getTopic(topicToAppend?: string, forSubscription?: boolean): string;
    /**
     * Returns the topic in following format:\
     * iot1/[device-group-id]/[device-id]/[devOrSrv]/[schemaGroup]/[dataType]/(varName)
     * @param devOrSrv 
     * Direction of communication (to device or to server)\
     * Possible values are:
     *  - dev: Communication to device (device is subscriber, server publisher)
     *  - srv: Communication to server (server is subscriber, device publischer)
     * @param schemaGroup
     * The schema-group the variable(s) (defined in data or topic) belongs too
     *  - data: Value from the iot-client to the server
     *  - notify: Binary messages from the client to the server (alarm, warning,...)
     *  - config: Configuration-value from the server to the client. The client returns the value read from the device after request from server.
     *  - methods: Command from the server to the device. If the iot-client forwarded the command to the device successfully, the iot-client returns the value send to the device. In case of error, value null will be returned instead.
     * @param varName
     * Name of the variable. Only required if previous topic is obj.
     * @param forSubscription 
     * Appends a multiple-wildcard (#) at the end of the topic for subscription
     */
    public getTopic(devOrSrv: 'dev' | 'srv', schemaGroup: ESchemaGroup | '+', dataType: 'obj' | 'arr' | '+', forSubscription?: boolean): string;
    public getTopic(devOrSrv: 'dev' | 'srv', schemaGroup: ESchemaGroup | '+', dataType: 'obj' | 'arr' | '+', varName?: string, appendWildcardForSubscription?: boolean): string;
    public getTopic(devSrvOrTopicToAppend?: string, schemaGroupOrSubscription?: ESchemaGroup | '+' | boolean, dataType?: 'obj' | 'arr' | '+', varNameOrSubscription?: string | boolean, forSubscription?: boolean): string {
        const set = this.getSettings() as IMqttSnSettings;
        const baseTopic = 'iot1' + MqttTopic.TOPIC_DELIMITER + set.groupId + MqttTopic.TOPIC_DELIMITER + set.deviceId;
        if (devSrvOrTopicToAppend === 'dev' || devSrvOrTopicToAppend === 'srv') {
            if (!schemaGroupOrSubscription) {
                throw new Error('Argument schemaGroup is missing!');
            }
            if (typeof schemaGroupOrSubscription !== 'string') {
                throw new Error('Argument schemaGroup must be from type `string`!');
            }
            if (!dataType) {
                throw new Error('Argument dataType is missing!');
            }

            let varName: string | undefined;
            let appendSub = false;
            if (varNameOrSubscription && typeof varNameOrSubscription === 'string') {
                varName = varNameOrSubscription;
            }
            if (varNameOrSubscription && typeof varNameOrSubscription === 'boolean') {
                appendSub = varNameOrSubscription;
            }
            if (forSubscription) {
                appendSub = forSubscription;
            }

            let appTopic = MqttTopic.TOPIC_DELIMITER + devSrvOrTopicToAppend +
                MqttTopic.TOPIC_DELIMITER + schemaGroupOrSubscription +
                MqttTopic.TOPIC_DELIMITER + dataType +
                (varName ? MqttTopic.TOPIC_DELIMITER + varName : '');
            appTopic += appendSub ? MqttTopic.TOPIC_DELIMITER + MqttTopic.MULTIPLE_WILDCARD : '';

            return baseTopic + appTopic;
        } else {
            // Append topic (first version)
            if (schemaGroupOrSubscription && typeof schemaGroupOrSubscription !== 'boolean') {
                throw new Error('Argument `forSubscription` must be from type `boolean`!');
            }
            let appTopic = devSrvOrTopicToAppend ? MqttTopic.TOPIC_DELIMITER + devSrvOrTopicToAppend : '';
            appTopic += schemaGroupOrSubscription ? MqttTopic.TOPIC_DELIMITER + MqttTopic.MULTIPLE_WILDCARD : '';
            return baseTopic + appTopic;
        }


    }

    private _isConnecting = false;
    public get isConnecting() {
        return this._isConnecting;
    }

    public async connect(retryCount?: number) {
        retryCount = retryCount || 0;
        // another connect-process is ongoing
        if (!retryCount && this.isConnecting) {
            Logger.warn('Connect aborted, another connect-process is ongoing!');
            return;
        }
        // max.number of retries reached --> stop
        if (retryCount > this.maxRetries) {
            this._isConnecting = false;
            this.setDisconnected();
            throw new Error('Connect to gateway failed! Number of maximum retries reached!');
        }
        // Cleanup work (remove temporary stored data)
        this.pubMsgsWaitingForRelease.splice(0, this.pubMsgsWaitingForRelease.length);
        // Bind to socket
        // Logger.debug('Connecting, bind to socket!');
        await this.socket.connect();
        // Send message
        // Logger.debug('Connecting, send connect-message!');
        try {
            const connOptions: IMqttSnConnectData = {
                cmd: 'connect',
                will: true,
                clientId: this.getSettings().clientId,
                duration: this.getSettings().keepalive || 500,
                cleanSession: !this.getSettings().persistent,
            };
            await this.sendMessageToGw(connOptions);
        } catch (err) {
            Logger.error('Failed to send connection-message to gateway! Retry!');
            await this.wait(1000, 3000);
            await this.connect(retryCount++);
        }
        // Wait for ack and check
        // Logger.debug('Connection, waiting for response from gateway!');
        try {
            const connAck = await this.waitForMessage('connack') as IMqttSnResponseMessage;
            // Logger.debug('Connection acknowledge message ' + JSON.stringify(connAck));
            if (connAck.returnCode === EMqttSnReturnCode.accept) {
                // Logger.debug('Connection, response `accepted` from gateway received!');
                this._isConnecting = false;
                this.setConnected();
                this.on.emit('connect');
            } else if (connAck.returnCode === EMqttSnReturnCode.rejectedCongestion) {
                Logger.warn('Connection, response `reject:congestion` from gateway received! Retry!');
                await this.wait(1000, 3000);
                await this.connect(retryCount++);
            } else {
                Logger.error('Failed to connect to gateway ' + this.gwAddress + ':' + this.gwPort + '! Received return-code: ' + connAck.returnCode);
                this.setDisconnected();
                await this.wait(1000, 3000);
                await this.connect(retryCount++);
            }
        } catch (err) {
            // timed-out
            Logger.error('Failed to connect to gateway ' + this.gwAddress + ':' + this.gwPort + '!', err);
            this.setDisconnected();
            await this.wait(1000, 3000);
            await this.connect(retryCount++);
        }
    }


    private pubMsgsWaitingForRelease = new Array<IMqttSnPublishData>();

    private async receivedData(data: IMqttSnData) {
        if (!data) {
            return;
        }
        // Update last-contact timestamp
        this.extendConnected();

        if (data.cmd === 'willtopicreq') {
            return await this.sendWillTopic();
        }
        if (data.cmd === 'willmsgreq') {
            return await this.sendWillMessage();
        }
        if (data.cmd === 'register') {
            const regData = data as IMqttSnMessageData;
            if (regData.topicId !== undefined && regData.topicName) {
                this.setTopicReference(regData.topicName, regData.topicId);
                // send reg-ack back
                const msgRegAck: IMqttSnMessageData = {
                    cmd: 'regack',
                    topicIdType: 'normal',
                    topicId: regData.topicId,
                    msgId: regData.msgId,
                    returnCode: EMqttSnReturnCode.accept
                };
                await this.sendMessageToGw(msgRegAck);
            } else {
                Logger.warn('Received topic-register without topic-id from gateway! Response with Reject-Code invalid topic!');
                // send reg-ack back
                const msgRegAck: IMqttSnMessageData = {
                    cmd: 'regack',
                    topicIdType: 'normal',
                    topicId: regData.topicId,
                    msgId: regData.msgId,
                    returnCode: EMqttSnReturnCode.rejectedNotSupported
                };
                await this.sendMessageToGw(msgRegAck);
            }

            return;
        }
        if (data.cmd === 'advertise') {
            const adData = data as any;
            if (adData.gwId && adData.duration) {
                this._lastAdvertiseMessage = {
                    gwId: adData.gwId,
                    duration: adData.duration
                };
            }
        }

        if (data.cmd === 'publish') {
            const pubData = data as IMqttSnPublishData;
            // do checks
            if (pubData.msgId === undefined) {
                Logger.warn('Received publish-message without message-id!');
                return;
            }
            if (pubData.topicId === undefined || !this.hasTopicReference(pubData.topicId)) {
                Logger.warn('Received publish-message without or unknown topic-id! TopicId: ' + (pubData.topicId !== undefined ? pubData.topicId : 'undefined'));
                // send rejected message back
                const msgRejTopicId: IMqttSnPublishData = {
                    cmd: 'puback',
                    topicIdType: 'normal',
                    topicId: pubData.topicId || 0,
                    msgId: pubData.msgId,
                    returnCode: EMqttSnReturnCode.rejectedInvalidTopicID
                };
                await this.sendMessageToGw(msgRejTopicId);
                return;
            }

            // process message - how depends on QoS
            // QoS 0 and 1: process message immediately
            if (!pubData.qos || pubData.qos === 1) {
                // process message immediately
                // Convert to buffer in case of string payload
                if (!pubData.payload || typeof pubData.payload === 'string') {
                    pubData.payload = Buffer.from(pubData.payload || '');
                }
                await this.processMessage(this.getTopicReference(pubData.topicId).name, pubData.payload);
            }
            // QoS 1: send acknowledge back
            if (pubData.qos === 1) {

                const pubAck: IMqttSnPublishData = {
                    cmd: 'puback',
                    topicIdType: 'normal',
                    topicId: pubData.topicId,
                    msgId: pubData.msgId,
                    returnCode: EMqttSnReturnCode.accept
                };
                await this.sendMessageToGw(pubAck);
            } else if (pubData.qos === 2) {
                // put message in release pipeline and process after pub-rel-message from gateway
                if (!this.pubMsgsWaitingForRelease.some(msg => msg.msgId === pubData.msgId)) {
                    this.pubMsgsWaitingForRelease.push(pubData);
                }
                // send received back
                const pubRec: IMqttSnPublishQoS2Data = {
                    cmd: 'pubrec',
                    msgId: pubData.msgId
                };
                await this.sendMessageToGw(pubRec);
            }
        }
        // publish release message - QoS2 only
        if (data.cmd === 'pubrel') {
            const pubrel = data as IMqttSnPublishQoS2Data;
            if (pubrel.msgId !== undefined) {
                const msgPubIndex = this.pubMsgsWaitingForRelease.findIndex(msg => msg.msgId === pubrel.msgId);
                if (msgPubIndex >= 0) {
                    const msgPub = this.pubMsgsWaitingForRelease[msgPubIndex];
                    if (msgPub.topicId && this.hasTopicReference(msgPub.topicId)) {
                        // Convert to buffer in case of string payload
                        if (!msgPub.payload || typeof msgPub.payload === 'string') {
                            msgPub.payload = Buffer.from(msgPub.payload || '');
                        }
                        await this.processMessage(this.getTopicReference(msgPub.topicId).name, msgPub.payload);
                        this.pubMsgsWaitingForRelease.splice(msgPubIndex, 1);
                    } else {
                        Logger.warn('Publish-release message received, but a topic for topic-id `' + (msgPub.topicId || 'undefined') + '` does not exist!');
                    }
                } else {
                    Logger.warn('Publish-release message received, but a pub-message with msg-id `' + pubrel.msgId + '` not found in pipeline! Perhaps duplicate received!');
                }

                // send complete-message back
                const pubComp: IMqttSnPublishQoS2Data = {
                    cmd: 'pubcomp',
                    msgId: pubrel.msgId
                };
                await this.sendMessageToGw(pubComp);
            } else {
                Logger.warn('Received publish-message without message-id! Response with pubcomp-message not possible!');
                return;
            }
            return;
        }
    }


    /**
     * Converts the given data to an octet-buffer for socket transport
     */
    protected convertToMessage(data: IMqttSnData): Buffer {
        return this.packet.generate(data);
    }


    private async processMessage(topic: string, message: Buffer) {
        const topicArray = MqttTopic.convertTopicToArray(topic);
        Logger.debug('MQTT received topic : ' + topic + ' Message-length: ' + message.length);
        this.subscriptions.forEach(sub => {
            try {
                sub.emit(topicArray, message);
            } catch (error) {
                Logger.error('Failed to emit message to subsciber! Topic subscribed: ' + sub.topic + ' Topic received: ' + topic, error);
            }
        });
    }



    public async sendPublish(topic: string, message: string | Buffer | object | number, options?: IClientPublishOptions): Promise<void> {
        if (!this.isConnected) {
            throw new Error('Is not connected to gateway!');
        }
        const set = this.getSettings() as IMqttSnSettings;

        let topicId: number | undefined;
        if (!this.hasTopicReference(topic)) {
            // Send register topic request
            topicId = await this.requestTopic(topic);
        } else {
            topicId = this.getTopicReference(topic).id;
        }

        // common-options
        const opt: IClientPublishOptions = {
            qos: set.publish.qos,
            retain: set.publish.retain
        };
        // options provided as argument
        if (options !== undefined) {
            opt.qos = options.qos;
            opt.retain = options.retain
        }

        // Prepare data
        let data: string | Buffer;
        if (typeof message === 'string') {
            data = message;
        } else if (typeof message === 'number') {
            data = Buffer.allocUnsafe(4);
            data.writeFloatBE(message, 0);
        } else {
            if (Buffer.isBuffer(message)) {
                data = message;
            } else {
                data = JSON.stringify(message);
            }
        }
        let payload: string | Buffer;
        if (typeof message === 'string') {
            payload = message;
        } else if (Buffer.isBuffer(message)) {
            payload = message; //.toString('utf8');
        } else if (typeof message === 'object') {
            payload = JSON.stringify(message);
        } else {
            throw new Error('Type of message not supported!');
        }
        const connOptions: IMqttSnPublishData = {
            cmd: 'publish',
            dup: false,
            qos: opt.qos,
            retain: opt.retain,
            topicIdType: 'normal',
            topicId: topicId,
            msgId: this.getNextMsgId(),
            payload: payload
        };

        if (!opt.qos) {
            return await this.sendPublishQos0(connOptions);
        } else if (opt.qos === 1) {
            return await this.sendPublishQos1(topic, connOptions);
        } else if (opt.qos === 2) {
            return await this.sendPublishQos2(topic, connOptions);
        } else {
            throw new Error('Publish message with QoS ' + opt.qos + ' is not supported!');
        }
    }


    private async sendPublishQos0(data: IMqttSnPublishData) {
        await this.sendMessageToGw(data);
    }

    private async sendPublishQos1(topicname: string, data: IMqttSnPublishData, retryCounter?: number) {
        retryCounter = retryCounter || 0;
        if (retryCounter > this.maxRetries) {
            throw new Error('Send publish-message failed! Numer of maximum retries reached!');
        }
        // mark retry-package
        if (retryCounter > 0) {
            data.dup = true;
        }

        try {
            const sendMsg = await this.sendMessageToGw(data) as IMqttSnPublishData;
            const recMsg = await this.waitForMessage('puback', sendMsg.msgId) as IMqttSnPublishData;
            if (recMsg.returnCode !== undefined && recMsg.returnCode === EMqttSnReturnCode.accept) {
                return;
            } else if (recMsg.returnCode !== undefined && recMsg.returnCode === EMqttSnReturnCode.rejectedInvalidTopicID) {
                const id = await this.requestTopic(topicname);
                data.topicId = id;
                return await this.sendPublishQos1(topicname, data, retryCounter++);
            } else {
                throw new Error('Return-code of publish-message is missing or not acceptet! Return-code: ' + recMsg.returnCode);
            }
        } catch (err) {
            Logger.error('Send publish-message QoS1 failed! Retry again!', err);
            await this.wait(100, 500);
            return await this.sendPublishQos1(topicname, data, retryCounter++);
        }
    }

    /**
     * Client sends a pub-messages and waits for pub-rec from gw. Client retries if pub-rec is not received.
     * If client receives pub-rec, a pub-rel will be send and retry is suppressed.
     */
    private async sendPublishQos2(topicname: string, data: IMqttSnPublishData, retryCounter?: number) {
        retryCounter = retryCounter || 0;
        if (retryCounter > this.maxRetries) {
            throw new Error('Send publish-message failed! Numer of maximum retries reached!');
        }

        try {
            const sendMsg = await this.sendMessageToGw(data) as IMqttSnPublishData;
            const recMsg = await this.waitForMessage('pubrec', sendMsg.msgId) as IMqttSnPublishData;
            if (recMsg.returnCode !== undefined && recMsg.returnCode === EMqttSnReturnCode.accept) {
                return await this.sendPublishQoS2Release(sendMsg.msgId);
            } else if (recMsg.returnCode !== undefined && recMsg.returnCode === EMqttSnReturnCode.rejectedInvalidTopicID) {
                const id = await this.requestTopic(topicname);
                data.topicId = id;
                return await this.sendPublishQos2(topicname, data, retryCounter++);
            } else {
                throw new Error('Return-code of publish-message is missing or not acceptet! Return-code: ' + recMsg.returnCode);
            }
        } catch (err) {
            Logger.error('Send publish-message QoS1 failed! Retry again!', err);
            await this.wait(100, 500);
            return await this.sendPublishQos2(topicname, data, retryCounter++);
        }
    }

    private async sendPublishQoS2Release(msgId: number, retryCounter?: number) {
        retryCounter = retryCounter || 0;
        if (retryCounter > this.maxRetries) {
            throw new Error('Failed to send pub-rel! Maximum retries reached!');
        }
        const dataRel: IMqttSnPublishQoS2Data = {
            cmd: 'pubrel',
            msgId: msgId
        };
        try {
            await this.sendMessageToGw(dataRel);
            return await this.waitForMessage('pubcomp', msgId);
        } catch (err) {
            return await this.sendPublishQoS2Release(msgId, retryCounter++);
        }
    }

    protected sendMessageToGw(message: IMqttSnData) {
        return new Promise<IMqttSnData>((resolve, reject) => {
            // get Payload length for debug
            const msg = message as IMqttSnPublishData;
            let payloadLen: number;
            if (msg.payload) {
                payloadLen = Buffer.isBuffer(msg.payload) ? msg.payload.byteLength : new Buffer(msg.payload).byteLength;
                if (Buffer.isBuffer(msg.payload)) {
                    let myCount =0;
                    msg.payload.forEach( (v, i, a) => myCount++);
                    if (payloadLen !== myCount) {
                        Logger.warn('Buffer byte length not working! my-count: ' + myCount);
                    }
                }
            }

            const buffer = this.convertToMessage(message);
            this.socket.send(buffer).then(() => {
                Logger.debug('MQTT-SN: Sent message to gateway! Message-Length: ' + buffer.byteLength + 'byte' + (payloadLen ? ' Payload-Length: ' + payloadLen + 'byte' : ''), JSON.stringify(message));
                resolve(message);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    protected async subscribe(topic: string, callback?: ClientSubscribeCallback, options?: IClientSubscribeOptions);
    protected async subscribe(subscription: MqttSubscription<any>, callback?: ClientSubscribeCallback, options?: IClientSubscribeOptions);
    protected async subscribe(topicOrSubscription: string | MqttSubscription<any>, callback?: ClientSubscribeCallback, options?: IClientSubscribeOptions) {
        if (typeof topicOrSubscription === 'object') {
            const subscriber = topicOrSubscription;
            // Get client-options from settings, if topic belongs to schema-group (config, method)
            //let options: mqtt.IClientSubscribeOptions | undefined;
            if (MqttTopic.hasTopicAtIndex(subscriber.topicArray, MqttTopic.TOPIC_INDEX_SCHEMA_GROUP)) {
                const group = MqttTopic.getTopicAtIndex(subscriber.topicArray, MqttTopic.TOPIC_INDEX_SCHEMA_GROUP);
                // *set* must be any, because only 'config' and 'method'-groups contains subscribe-property
                const set = this.getSettings() as any;
                if (set[group] && set[group].subscribe) {
                    const opt = set[group].subscribe as IMqttConnectionOptions;
                    options = {
                        qos: opt.qos
                    };
                }
            }
            await this.subscribe(subscriber.topic, callback, options);
            return;
        }
        await this.sendSubscription(topicOrSubscription, options);
        Logger.info('Has send subscription for ' + topicOrSubscription);
    }

    protected unsubscribe(topic: string, callback?: (error?) => void) {
        this.sendUnsubscribe(topic)
            .then(() => {
                if (callback) {
                    callback();
                }
            }).catch((err) => {
                if (callback) {
                    callback(err);
                }
            })
    }


    public addSubscriber(subscriber: MqttSubscription<any>) {
        return new Promise<boolean>((resolve, reject) => {
            try {
                // topic is subscribed
                const subExist = this.subscriptions.find(s => s.topic === subscriber.topic);
                if (subExist) {
                    resolve(subExist.isSubscribed);
                } else {
                    this.subscriptions.push(subscriber);
                    if (!subscriber.isSubscribed) {
                        // Get client-options from settings, if topic belongs to schema-group (config, method)
                        let options: IClientSubscribeOptions | undefined;
                        if (MqttTopic.hasTopicAtIndex(subscriber.topicArray, MqttTopic.TOPIC_INDEX_SCHEMA_GROUP)) {
                            const group = MqttTopic.getTopicAtIndex(subscriber.topicArray, MqttTopic.TOPIC_INDEX_SCHEMA_GROUP);
                            // *set* must be any, because only 'config' and 'method'-groups contains subscribe-property
                            const set = this.getSettings() as any;
                            if (set[group] && set[group].subscribe) {
                                const opt = set[group].subscribe as IMqttConnectionOptions;
                                options = {
                                    qos: opt.qos
                                };
                            }
                        }
                        this.subscribe(subscriber.topic, (error, granted) => { }, options).then( () => resolve(true));
                    }
                }
            } catch (error) {
                reject(error);
            }
        });
    }

    public getSubscriber(topic: string) {
        return this.subscriptions.find(s => s.topic === topic);
    }

    public removeSubscriber(topic: string | MqttSubscription<any>) {
        let t = typeof topic === 'string' ? topic : topic.topic;
        return new Promise<MqttSubscription<any>>((resolve, reject) => {
            try {
                // topic is subscribed
                const subExist = this.subscriptions.find(s => s.topic === t);
                if (subExist) {
                    this.unsubscribe(subExist.topic, (error) => {
                        if (error) {
                            reject(error);
                        } else {
                            subExist.isSubscribed = false;
                            const i = this.subscriptions.findIndex(s => s.topic === t);
                            resolve(this.subscriptions.splice(i, 1)[0]);
                        }
                    });
                } else {
                    const err = new Error('Remove subscriber from MQTT-Client failed, subscriber with topic `' + t + '` not found!');
                    reject(err);
                }
            } catch (error) {
                reject(error);
            }
        });
    }

    /**
     * If connected and last contact exceeded, sends a ping request to the server
     */
    private async checkKeepAlive() {
        if (this.isConnected && Date.now() > this.nextPingTime) {
            const ok = await this.sendPing();
            if (!ok) {
                Logger.warn('Ping failed! Disconnect requested!');
                this.setDisconnected();
            }
        }
    }


    /**
     * Send will-topic to gateway.
     * Will topic is defined in settings
     */
    private async sendWillTopic(): Promise<boolean> {
        const willMsg: IMqttSnWillTopicData = {
            cmd: 'willtopic',
            qos: this.getSettings().publish.qos,
            retain: this.getSettings().publish.retain,
            willTopic: this.getSettings().will.topic
        };
        try {
            await this.sendMessageToGw(willMsg);
            return true;
        } catch (err) {
            Logger.error('Failed to send will-topic!', err);
            return false;
        }
    }
    /**
     * Send will-message to gateway.
     * Will message is defined in settings
     */
    private async sendWillMessage(): Promise<boolean> {
        const msg = new BooleanVar('CONN_STATE', true);
        const buffer = DataPackerOperator.GET().packData(msg);
        const willMsg: IMqttSnWillMessageData = {
            cmd: 'willmsg',
            willMessage: buffer
        };
        try {
            await this.sendMessageToGw(willMsg);
            return true;
        } catch (err) {
            Logger.error('Failed to send will-message!', err);
            return false;
        }
    }

    /**
     * Sends a ping-request (keep-alive) to the gateway.
     * Returns true if a ping-response were received.
     */
    private async sendPing(applyClientId = false): Promise<boolean> {
        const set = this.getSettings() as IMqttSnSettings;

        const connOptions: IMqttSnPingData = {
            cmd: 'pingreq',
            clientId: applyClientId ? this.getSettings().clientId : undefined
        };
        try {
            await this.sendMessageToGw(connOptions);
            await this.waitForMessage('pingresp');
            return true;
        } catch (err) {
            return false;
        }
    }

    /**
     * Sends a topic-request to the gateway.
     * Returns the topic-id received.
     * If maximum retries are reached, an error is thrown.
     */
    private async requestTopic(topicname: string, retryCounter?: number): Promise<number> {
        if (!this.isConnected) {
            throw new Error('Driver is not connected to gateway!');
        }
        retryCounter = retryCounter || 0;
        if (retryCounter > this.maxRetries) {
            throw new Error('Failed to request topic-id, maximum number of retries reached!');
        }
        const opt: IMqttSnMessageData = {
            cmd: 'register',
            topicIdType: 'normal',
            topicId: 0,
            topicName: topicname,
            msgId: this.getNextMsgId()
        };

        try {
            const sndMsg = await this.sendMessageToGw(opt) as IMqttSnMessageData;
            const recMsg = await this.waitForMessage('regack', sndMsg.msgId) as IMqttSnMessageData;

            if (recMsg.returnCode === undefined) {
                Logger.warn('Has not received a return-code in register response! Retry topic-registration!');
                return await this.requestTopic(topicname, retryCounter++);
            }
            if (recMsg.returnCode === EMqttSnReturnCode.accept) {
                if (recMsg.topicId !== undefined) {
                    this.setTopicReference(topicname, recMsg.topicId);
                    return recMsg.topicId;
                } else {
                    Logger.warn('Has not received a topic-id! Retry topic-registration!');
                    return await this.requestTopic(topicname, retryCounter++);
                }
            } else {
                Logger.warn('Has not received return-code accept! Retry topic-registration!');
                await this.wait(100, 500);
                return await this.requestTopic(topicname, retryCounter++);
            }

        } catch (err) {
            Logger.error('Request topic failed!', err);
            await this.wait();
            return await this.requestTopic(topicname, retryCounter++);
        }
    }



    private async sendSubscription(topic: string, retryCounter?: number): Promise<void>;
    private async sendSubscription(topic: string, options?: IClientSubscribeOptions, retryCounter?: number): Promise<void>;
    private async sendSubscription(topic: string, optionsOrCounter?: IClientSubscribeOptions | number, counter?: number): Promise<void> {
        let options: IClientSubscribeOptions | undefined;
        let retryCounter = 0;
        if (optionsOrCounter !== undefined) {
            if (typeof optionsOrCounter === 'object') {
                options = optionsOrCounter;
                if (counter) {
                    retryCounter = counter;
                }
            } else {
                retryCounter = optionsOrCounter;
            }
        }

        if (!this.isConnected) {
            throw new Error('Driver is not connected to gateway!');
        }
        retryCounter = retryCounter || 0;
        if (retryCounter > this.maxRetries) {
            throw new Error('Failed to subscribe `' + topic + '`, maximum number of retries reached!');
        }

        /* let topicId = 0;
        if (!this.hasTopicReference(topic)) {
            topicId = await this.requestTopic(topic);
        } else {
            topicId = this.getTopicReference(topic).id;
        } */

        const opt: IMqttSnSubscribeData = {
            cmd: 'subscribe',
            qos: options && options.qos ? options.qos : this.getSettings().subscribe.qos,
            dup: retryCounter > 0,
            topicIdType: 'normal',
            topicName: topic,
            msgId: this.getNextMsgId(),
        };
        try {
            const sendMsg = await this.sendMessageToGw(opt) as IMqttSnSubscribeData;
            const recMsg = await this.waitForMessage('suback', sendMsg.msgId) as IMqttSnSubscribeData;
            if (recMsg.returnCode === EMqttSnReturnCode.accept) {
                if (recMsg.topicId !== undefined) {
                    this.setTopicReference(topic, recMsg.topicId);
                }
                return;
            } else {
                throw new Error('Has not received return-code accept!');
            }
        } catch (err) {
            Logger.error('Send subscription for `' + topic + '` failed! Retry again!', err);
            await this.wait(100, 500);
            if (options)
                return await this.sendSubscription(topic, options, retryCounter++);
            else
                return await this.sendSubscription(topic, retryCounter++);
        }
    }

    private async sendUnsubscribe(topic: string, retryCounter?: number): Promise<void> {

        retryCounter = retryCounter || 0;

        if (!this.isConnected) {
            throw new Error('Driver is not connected to gateway!');
        }
        retryCounter = retryCounter || 0;
        if (retryCounter > this.maxRetries) {
            throw new Error('Failed to unsubscribe `' + topic + '`, maximum number of retries reached!');
        }

        let topicId = 0;
        if (!this.hasTopicReference(topic)) {
            topicId = await this.requestTopic(topic);
        } else {
            Logger.warn('Use topic-name instead topic-id for unsubscription! Topic-id not found for `' + topic + '`!');
        }
        // Use of topic-name and topic-id seems to be different in standard to other messages; topicType normal => use topic-name
        const opt: IMqttSnSubscribeData = {
            cmd: 'unsubscribe',
            topicIdType: topicId !== undefined ? 'pre-defined' : 'normal',
            topicId: topicId !== undefined ? topicId : undefined,
            topicName: topicId === undefined ? topic : undefined,
            msgId: this.getNextMsgId(),
        };
        try {
            const sendMsg = await this.sendMessageToGw(opt) as IMqttSnSubscribeData;
            const recMsg = await this.waitForMessage('unsuback', sendMsg.msgId) as IMqttSnSubscribeData;
            if (recMsg.returnCode !== undefined && recMsg.returnCode === EMqttSnReturnCode.accept) {
                return;
            } else {
                throw new Error('Has not received return-code `accept`! Received return-code:' + recMsg.returnCode);
            }
        } catch (err) {
            Logger.error('Send unsubscription for `' + topic + '` failed! Retry again!', err);
            await this.wait(100, 500);
            return await this.sendUnsubscribe(topic, retryCounter++);
        }
    }

    public async close(sleepTime?: number) {
        // stop keep-alive monitoring
        if (this.checkKeepAliveTimer) {
            clearInterval(this.checkKeepAliveTimer);
        }
        // send command
        await this.sendDisconnect(sleepTime);
        // close socket        
        return new Promise<void>((resolve, reject) => {
            if (this.socket) {
                this.socket.close().then(() => {
                    resolve();
                }).catch((err) => {
                    reject(err);
                });
            } else {
                resolve();
            }
        });
    }
    /**
     * Send disconnect to gateway.
     * @param sleepDuration The time the device will go to sleep in sec. If undefined, device will not go to sleep.
     */
    private async sendDisconnect(sleepDuration?: number): Promise<void> {
        if (!this.isConnected) {
            throw new Error('Driver is not connected to gateway!');
        }

        // Use of topic-name and topic-id seems to be different in standard to other messages; topicType normal => use topic-name
        const opt: any = {
            cmd: 'disconnect',
            duration: sleepDuration
        };
        try {
            await this.sendMessageToGw(opt);
        } catch (err) {
            Logger.error('Send disconnect failed!', err);
        }
    }

    private lastMsgId = 0;
    /**
     * Returns a unique message-id between 0 (excl.) and 2¹⁶-1 (uint16)
     */
    private getNextMsgId(): number {
        this.lastMsgId++;
        if (this.lastMsgId <= 0 || this.lastMsgId > 65535) {
            this.lastMsgId = 1;
        }
        return this.lastMsgId;
    }

    /**
     * Returns true if a message of this command should contains a msgId-field
     */
    private isCmdWithMsgId(cmd: string) {
        return cmd === 'register' || cmd === 'regack' || cmd === 'publish' ||
            cmd === 'puback' || cmd === 'pubrec' || cmd === 'pubrel' ||
            cmd === 'pubcomp' || cmd === 'subscribe' || cmd === 'suback' ||
            cmd === 'unsubscribe' || cmd === 'unsuback';
    }

    private waitForMessage(cmd: string): Promise<IMqttSnData>;
    private waitForMessage(cmd: string, msgId: number): Promise<IMqttSnData>;
    private waitForMessage(cmd: string, timeout: number): Promise<IMqttSnData>;
    private waitForMessage(cmd: string, msgId: number, timeout: number): Promise<IMqttSnData>;
    /**
     * Is waiting until a message with the specific command (message-type) and optional msgId received.
     * For commands with a messageId-field (e.g. publish,...), msgId must be provided. 
     */
    private waitForMessage(cmd: string, msgId?: number, timeout?: number): Promise<IMqttSnData> {
        const withMsgId = this.isCmdWithMsgId(cmd);
        if (this.isCmdWithMsgId(cmd)) {
            timeout = timeout || 10000;
        } else {
            timeout = msgId || 10000;
        }

        return new Promise<IMqttSnData>((resolve, reject) => {
            if (withMsgId && msgId === undefined) {
                reject(new Error('A message-id is required packages with cmd ' + cmd));
            }

            const timeMax = Date.now() + (timeout || 10000);
            const checkTimeout = setInterval(() => {
                if (Date.now() > timeMax) {
                    clearTimeout(checkTimeout);
                    this.on.removeListener('reveived', callback);
                    reject(new Error('Time exceeded during waiting for message-type ' + cmd + (withMsgId ? ' and msg-id ' + msgId + '!' : '!')));
                }
            }, 1000);

            const callback = (data) => {
                if (!data) {
                    return;
                }
                const msg = data as IMqttSnData;
                if (msg.cmd === cmd) {
                    if (withMsgId) {
                        const m = msg as IMqttSnMessageData;
                        if (m.msgId && m.msgId === msgId) {
                            clearTimeout(checkTimeout);
                            this.on.removeListener('reveived', callback);
                            resolve(msg);
                        }
                    } else {
                        clearTimeout(checkTimeout);
                        this.on.removeListener('reveived', callback);
                        resolve(msg);
                    }
                }
            };
            this.on.on('received', callback);
        });
    }

    /**
     * Wait for random-time between 10msec and 1sec
     */
    private wait(): Promise<void>;
    /**
     * Wait for the given time
     */
    private wait(timeToWait: number): Promise<void>;
    /**
     * Wait for a random time between minTime and maxTime in msec
     */
    private wait(minTime: number, maxTime: number): Promise<void>;
    private wait(min?: number, max?: number): Promise<void> {
        let waitTime = 10;
        if (!min && !max) {
            waitTime = Math.random() * 1000 + 10;
        } else if (min && max) {
            waitTime = Math.random() * (max - min) + min;
        } else if (min) {
            waitTime = min;
        }

        return new Promise<void>((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, waitTime);
        });
    }

    //#region Topic-References

    private _levelDB;
    private static dbName = "./db/iot-test-mqtt-sn";
    private static dbTopicKey = "mqtt-sn-topics";
    protected get levelDB() {
        if (!this._levelDB) {
            var level = require('level');
            this._levelDB = level(MqttSnDriver.dbName);
        }
        return this._levelDB;
    }

    protected async prepareTopicReferences() {
        if (this.getSettings().saveTopics) {
            try {
                this.topicReferences = await this.loadTopicReferences();
            } catch (err) {
                this.topicReferences = [];
                Logger.error('Failed to load topic-references from DB!', err);
            }
        } else {
            this.topicReferences = [];
        }
    }

    /**
     * Loads the topics from DB.
     * Returns true if loaded sucessfully. False if a list does not exists.
     */
    protected loadTopicReferences() {
        return new Promise<{ name: string, id: number }[]>((resolve, reject) => {
            this.levelDB.get(MqttSnDriver.dbTopicKey, (err, value) => {
                if (err || !value) {
                    // key not found
                    Logger.warn('Topics not stored in Level-DB!');
                    resolve([]);
                } else {
                    try {
                        const list = JSON.parse(value) as Array<{ name: string, id: number }>;
                        if (Array.isArray(list)) {
                            this.topicReferences = list;
                            Logger.info('Has loaded topics from DB! Length: ' + list.length);
                            resolve(list);
                        } else {
                            Logger.warn('List of topices received from DB is not an array!');
                            resolve([]);
                        }
                    } catch (err) {
                        reject(new Error('Load topics from level-db failed! ' + (err as Error).message));
                    }
                }
            });
        });
    }

    protected getTopicReference(topic: string | number) {
        const tp = typeof topic === 'string' ? this.topicReferences.find(t => t.name === topic) : this.topicReferences.find(t => t.id === topic);
        if (tp) {
            return tp;
        } else {
            throw new Error('Topic with name or id `' + topic + '` not found in topic-reference-list!');
        }
    }


    protected hasTopicReference(topicId: number);
    protected hasTopicReference(topicName: string);
    protected hasTopicReference(topic: number | string) {
        if (typeof topic === 'number')
            return this.topicReferences.some(t => t.id === topic);
        else
            return this.topicReferences.some(t => t.name === topic);
    }

    protected setTopicReference(name: string, id: number) {
        let changed = false;
        // name exists --> update id
        if (this.hasTopicReference(name)) {
            const tr = this.getTopicReference(name);
            if (tr.id !== id) {
                tr.id = id;
                changed = true;
            }
        } else if (this.hasTopicReference(id)) {
            // id exists --> update name
            const tr = this.getTopicReference(id);
            if (tr.name !== name) {
                tr.name = name;
                changed = true;
            }
        } else {
            // not in list --> add
            this.topicReferences.push({ name: name, id: id });
            changed = true;
        }

        if (changed && this.getSettings().saveTopics) {
            const stValue = JSON.stringify(this.topicReferences);
            this.levelDB.put(MqttSnDriver.dbTopicKey, stValue, (error) => {
                if (error) {
                    Logger.error('Failed to save topic-references!', error);
                }
            });
        }
    }

    /**
     * Removes all topic-references (from memory and storage - if enabled)
     */
    protected clearTopicReferences() {
        if (this.topicReferences && this.topicReferences.length > 0) {
            this.topicReferences.splice(0, this.topicReferences.length);
            if (this.getSettings().saveTopics) {
                this.levelDB.del(MqttSnDriver.dbTopicKey);
            }
        } else if (!this.topicReferences) {
            this.topicReferences = [];
        }
    }
    //#endregion


    //#region Override controller interface-methods
    public async connectForTest() {
        await this.connect();
        await this.addSubscriptions();
    }
    public async sendTest(testVarOrSchemaGroup: import("../../trigger/test-event.model").ITestVar | ESchemaGroup, buffer: Buffer) {
        const schemaGroup = typeof testVarOrSchemaGroup === 'object' ? testVarOrSchemaGroup.schemaGroup : testVarOrSchemaGroup;
        const settings = this.getSettings() as IMqttSnSettings;
        let options: IMqttConnectionOptions | undefined;
        if (settings[schemaGroup] && settings[schemaGroup].publish) {
            options = settings[schemaGroup] && settings[schemaGroup].publish;
        }
        await this.sendPublish(this.getTopicForPublish(schemaGroup), buffer, options);
    }

    public async disconnectForTest() {
        await this.close();
    }
    //#endregion

}