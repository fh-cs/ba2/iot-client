import { EventEmitter } from "events";
import { Socket, createSocket } from "dgram";
import { Logger } from "../../common/logger/logger";
import * as fs from 'fs';


export class SocketWrapper {

    /**
     * Emits events:
     * - connect: callback: () => void
     * - message: callback: (data) => void
     * - close: callback () => void
     * - error: callback (error) => void
     */
    public event = new EventEmitter();

    private _dtlsSocket;
    private _dgramSocket: Socket | undefined;
    
    constructor(
        public mode: 'udp4' |'dtls', 
        public remoteAddress: string, 
        public remotePort: number, 
        public localPort?: number,
        public certificateFile?: string,
        public certificatePrivateKeyFile?: string,
        public pskIdentity?: string | Buffer,
        public pskSecret?: string | Buffer) {

    }


    public async connect() {
        if (this._dtlsSocket) {
            try {
                this._dtlsSocket.close();
            } catch(err) {
            }
        }
        if (this._dgramSocket) {
            try {
                this._dgramSocket.close();
            }catch(error) {
            }
        }

        if (this.mode === 'dtls') {
            const dtls = require('@nodertc/dtls');
            // dtls.connect(options: Options [, callback: function]) : Socket
            this._dtlsSocket = dtls.connect({ 
                type: "udp4", 
                port: this.localPort, 
                remotePort: this.remotePort, 
                remoteAddress: this.remoteAddress,
                certificate: this.certificateFile ? fs.readFileSync(this.certificateFile) : undefined,
                certificatePrivateKey: this.certificatePrivateKeyFile ? fs.readFileSync(this.certificatePrivateKeyFile) : undefined,
                pskIdentity: this.pskIdentity,
                pskSecret: this.pskSecret
            });
            this._dtlsSocket.on('connect', () => {
                this.event.emit('connect');
            });
            this._dtlsSocket.on('data', (data) => {
                this.event.emit('message', data as Buffer);
            });
            this._dtlsSocket.on('error', (err) => {
                this.event.emit('error', err);
            });
        }

        if(this.mode === 'udp4') {
            this._dgramSocket = createSocket('udp4');
            this._dgramSocket.on('error', (err) => {
                this.event.emit('error', err);
            });
            this._dgramSocket.on('connect', async () => {
                this.event.emit('connect');
            });
            this._dgramSocket.on('message', (msg, info) => {
                this.event.emit('message', msg);
            });
            this._dgramSocket.on('close', async () => {
                this.event.emit('close');
            });
            this._dgramSocket.bind(this.localPort);
        }
    } 

    public async send(data: Buffer) {
        return new Promise<void>( (resolve, reject) => {
            if (this._dgramSocket) {
                //Logger.debug('Datagram-Socket: Max-Buffer-Size: ' + this._dgramSocket.getSendBufferSize() + 'byte');
                this._dgramSocket.send(data, 0, data.byteLength, this.remotePort, this.remoteAddress, (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            } else if (this._dtlsSocket) {
                this._dtlsSocket.write(data);
                resolve();
            } else {
                reject(new Error('Not connected!'));
            }
        });
    }

    public async close() {
        if (this._dtlsSocket) {
            try {
                this._dtlsSocket.close();
            } catch(err) {
            }
            // dtls is not providing close-event
            this.event.emit('close');
        }
        if (this._dgramSocket) {
            try {
                this._dgramSocket.close();
            } catch(err) {
            }
        }
    }
}