import { IDriverSettings } from "../driver-settings.model";
import { IMqttConnectionOptions } from "../mqtt/mqtt-settings";

export interface IMqttSnSettings extends IDriverSettings {

    /**
     * Name of IP-Address of MQTT-SN/MQTT-Gateway
     */
    gwAddress: string;
    /**
     * Port address the gateway is listen.
     * If not set or 0, default port is used.
     */
    gwPort: number;
    /**
     * Port address we are listening for incoming messages
     */
    listenPort?: number;
    /**
     * If set, connects to the MQTT-Broker over *mqtts* (instead of *mqtt*)
     */
    useSSL?: boolean;
    /**
     * Alpha-numeric identifier of the device-group this device belongs too
     */
    groupId: string;
    /**
     * Alpha-numeric identifier of the device
     * Used as topic for other subscriber
     */
    deviceId: string;
    /**
     * Maximum time-period (in 10seconds) packets sent from client to server. If time exceeded, client sends a ping-request to the server.
     * The broker returns a ping-response. If the broker receives no packets in about 1.5time of keep-alive, broker disconnects and publishes a last-will message to subscribers.
     */
    keepalive?: number;
    /**
     * If true, disable the clean-session flag. 
     */
    persistent?: boolean;
    /**
     * Save topic-names and their topic-id for next time use after reboot/restart.
     */
    saveTopics?: boolean,
    /**
     * Id of the client required for authorization at MQTT-Broker
     */
    clientId: string;
    /**
     * Path to the TLS-Certificate-File
     */
    certFile?: string;

    privateKeyFile?: string;

    pskIdentity?: string;

    pskSecret?: string;
    /**
     * Path to the CA-File for Encryption
     */
    //caFile?: string;
    /**
     * If true, stores TLS-Session-ID locally and reuse session-id for next tls-handshake procedure
     */
    // resumeTlsSession?: boolean;

    will: {
        topic: string
    },
    /**
     * Connection options definition for all data-variables
     */
    data: {
        publish: IMqttConnectionOptions
    };
    /**
     * Connection options definition for all notify-variables
     */
    notify: {
        publish: IMqttConnectionOptions
    };
    /**
     * Connection options definition for all config-variables
     */
    config: {
        publish: IMqttConnectionOptions, 
        subscribe:IMqttConnectionOptions
    };
    /**
     * Connection options definition for all method-variables
     */
    method: {
        publish: IMqttConnectionOptions, 
        subscribe:IMqttConnectionOptions
    };
    /**
     * Default-Connection options definition for publish messages not part of any group
     */
    publish: IMqttConnectionOptions;
    /**
     * Default-Connection options definition for subscriptions not part of any group
     */
    subscribe: IMqttConnectionOptions;
    
}

