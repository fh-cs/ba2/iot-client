
export interface IMqttSnData {
    cmd: string,
}

export interface IMqttSnResponseMessage extends IMqttSnData {
    returnCode: EMqttSnReturnCode
}

export enum EMqttSnReturnCode {
    accept = 'Accepted',
    rejectedCongestion = 'Rejected: congestion',
    rejectedInvalidTopicID = 'Rejected: invalid topic ID',
    rejectedNotSupported = 'Rejected: not supported'
}


export interface IMqttSnConnectData extends IMqttSnData {
    will: boolean,
    cleanSession: boolean,
    duration: number,
    clientId: string,
}

export interface IMqttSnMessageData extends IMqttSnData {
    dup?: boolean,
    qos?: 0 | 1 | 2,
    
    /**
     * Using topic-name -> short topic
     */
    topicIdType: 'normal' | 'pre-defined' | 'short topic', // = 0,
    /**
     * 16-bit integer or 2-octet-string
     * topicIdType normal or pre-defined
     */
    topicId?: number,
    /**
     * topic to publish
     * topicIdType short-topic
     */
    topicName?: string,
    /**
     * 16-bit integer (random number).
     * Required to identify ack-packets
     */
    msgId: number,

    /**
     * Only availabe at responses
     */
    returnCode?: EMqttSnReturnCode
}

export interface IMqttSnPublishData extends IMqttSnMessageData {
    retain?: boolean,
    payload?: string | Buffer
}

export interface IMqttSnPublishQoS2Data extends IMqttSnData {
    msgId: number
}

export interface IMqttSnSubscribeData extends IMqttSnMessageData {
}

export interface IMqttSnPingData extends IMqttSnData {
    /**
     * As with MQTT, the ClientId field has a variable length and contains a 1-23 character 
     * long string that uniquely identifies the client to the server.
     */
    clientId?: string
}


export interface IMqttSnWillTopicData extends IMqttSnData {
    retain?: boolean,
    qos?: 0 | 1 | 2,
    willTopic: string
}


export interface IMqttSnWillMessageData extends IMqttSnData {
    willMessage: string | Buffer
}