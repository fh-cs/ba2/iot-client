import { EDriverType } from "./driver-type.enum";

export interface IDriverSettings {
    /**
     * Name of this driver (used for identification and reference)
     */
    name: string;
    /**
     * Name of controller or List of controller-names this driver belongs too.
     */
    controller: string | string[];
    /**
     * Type of driver to be used for this test
     */
    type: EDriverType;

}