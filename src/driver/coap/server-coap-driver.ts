import { CoapDriver } from "./coap-driver";
import { IServerCoapSettings } from "./server-coap-settings";
import { Logger } from "../../common/logger/logger";
import { ESchemaGroup } from "../../common/schema-group.enum";




export class ServerCoapDriver extends CoapDriver {

    /**
     * CoAP-Server
     * API-Documentation: https://www.npmjs.com/package/coap#createserveroptions-requestlistener
     */
    private server: any;    

    constructor(settings?: IServerCoapSettings) {
        super(settings);
    }

    public getSettings() {
        return super.getSettings() as IServerCoapSettings;
    }

    /**
     * Called by test-controller
     * Open port for listening
     */
    public connectForTest(): Promise<void> {
        return new Promise<void>( (resolve, reject) => {
            const set = this.getSettings();
            const coap = require('coap');
            this.server = coap.createServer();
            //this.server.on('request', this.onRequest);
            this.server.on('request', (req, resp) => {
                Logger.debug('CoAP-Server `' + this.name + '`: Received (onRequest) on URL: ' + req.url);
                const schemaGroup = this.getPartOfUrl(req.url, CoapDriver.URL_INDEX_SCHEMA_GROUP) as ESchemaGroup;
                this.onReceive.next({ schemaGroup: schemaGroup, data: req.payload });
            });
            this.server.listen(set.port, set.host, () => {
                Logger.info('CoAP-Server listening on ' + (set.host ? set.host : '*') + ':' + set.port);
                resolve();
            });
        });
    }
    
    
    public async sendTest(testVar: import("../../trigger/test-event.model").ITestVar | ESchemaGroup, buffer: Buffer): Promise<void> {
        return;
    }

    public disconnectForTest(): Promise<void> {
        return new Promise<void>( (resolve, reject) => {
            if (this.server) {
                this.server.close( () => {
                    Logger.info('CoAP-Server stopped listening!');
                    resolve();
                });
            }
        });  
    }


}