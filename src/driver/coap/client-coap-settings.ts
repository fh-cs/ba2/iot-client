import { ICoapSettings } from "./coap-settings";

export interface IDeviceCoapSettings extends ICoapSettings {
    confirmable?: boolean,
    observe?:boolean

}