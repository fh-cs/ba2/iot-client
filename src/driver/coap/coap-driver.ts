import { Driver } from "../driver";
import { ICoapSettings } from "./coap-settings";
import { ESchemaGroup } from "../../common/schema-group.enum";

export abstract class CoapDriver extends Driver {
    public static readonly URL_DELIMITER = '/';
    public static readonly URL_INDEX_INDICATOR = 1;
    public static readonly URL_INDEX_GROUP_ID = 2;
    public static readonly URL_INDEX_DEVICE_ID = 3;
    public static readonly URL_INDEX_SCHEMA_GROUP = 4;
    public static readonly URL_INDEX_PAYLOAD_TYPE = 5;
    public static readonly URL_INDEX_VARNAME = 6;

    constructor(settings?: ICoapSettings) {
        super(settings);
    }

    public getSettings() {
        return super.getSettings() as ICoapSettings;
    }

    /**
     * Returns the CoAP URL in following format:\
     * iot1/[device-group-id]/[device-id]/[schemaGroup]/[payloadType]/(varName)
     * DeviceGroupID and DeviceID are provided from driver-settings.
     * @param schemaGroup
     * The schema-group the variable(s) (defined in data or topic) belongs too
     * @param payloadType
     * defines how variables are packed on payload (as array or single-object)
     * - obj: as single object
     * - arr: multiple objects
     * @param varName
     * Name of the variable. Only required if payloadType is *obj*.
     */
    public getUrl(schemaGroup: ESchemaGroup, payloadType: 'obj' | 'arr', varName?: string): string {
        const set = this.getSettings();
        const delimiter = CoapDriver.URL_DELIMITER;
        if (!set.groupId) {
            throw new Error('Miss `groupId` in Settings for driver `' + this.name + '`!');
        }
        if (!set.deviceId) {
            throw new Error('Miss `deviceId` in Settings for driver `' + this.name + '`!');
        }
        if (payloadType === 'obj' && !varName) {
            throw new Error('Url-Builder requires a variable-name if payload-type is `obj`!');
        }
        return 'iot1' + delimiter + 
                set.groupId + delimiter + 
                set.deviceId + delimiter + 
                schemaGroup + delimiter + 
                payloadType + delimiter + 
                (varName ? delimiter + varName : '');
    }

    /**
     * Returns the url-part at the urlIndex-Position (zero-based).
     * For urlIndex, use URL_INDEX_*-constants.
     */
    getPartOfUrl(url: string, urlIndex: number) {
        const parts = url.split(CoapDriver.URL_DELIMITER);
        if ( urlIndex >= 0 && urlIndex < parts.length) {
            return parts[urlIndex];
        } else {
           throw new Error('UrlIndex ' + urlIndex + ' out of range! Url-Length: ' + parts.length); 
        }
    }
    /**
     * Returns true if a url-part at urlIndex (zero-based-position) is available.
     * For urlIndex, use URL_INDEX_*-constants.
     */
    hasPartOfUrl(url: string, urlIndex: number) {
        const parts = url.split(CoapDriver.URL_DELIMITER);
        return (urlIndex >= 0 && urlIndex < parts.length && !!parts[urlIndex]);   
    }
    /**
     * Returns true if the url-part at urlIndex (zero-based-position) is equal partToCheck.
     * For urlIndex, use URL_INDEX_*-constants.
     */
    isPartOfUrl(url: string, urlIndex: number, partToCheck: string) {
        return this.hasPartOfUrl(url, urlIndex) ? (this.getPartOfUrl(url, urlIndex) === partToCheck) : false;
    }

}