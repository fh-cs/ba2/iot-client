import { CoapDriver } from "./coap-driver";
import { IDeviceCoapSettings as IClientCoapSettings } from "./client-coap-settings";
import { ESchemaGroup } from "../../common/schema-group.enum";
import { ECoapMethod } from "./coap-methods.enum";
import { Logger } from "../../common/logger/logger";

export class ClientCoapDriver extends CoapDriver {

    constructor(settings?: IClientCoapSettings) {
        super(settings);
    }

    public getSettings() {
        return super.getSettings() as IClientCoapSettings;
    }

    private getRequestOptions(schemaGroup: ESchemaGroup) {
        const set = this.getSettings();
        return {
            hostname: set.host,
            port: set.port,
            method: ECoapMethod.POST,
            confirmable: set.confirmable,
            observe: set.observe,
            pathname: this.getUrl(schemaGroup, 'arr')
        };
    }
    public async connectForTest(): Promise<void> {
        return;        
    }    
    
    public sendTest(testVar: import("../../trigger/test-event.model").ITestVar | ESchemaGroup, buffer: Buffer): Promise<void> {
        return new Promise<void>( (resolve, reject) => {
            const sg = typeof testVar === 'object' ? testVar.schemaGroup : testVar;
            const coap = require('coap');
            const opts = this.getRequestOptions(sg);
            const req = coap.request(opts);
            Logger.debug('CoAP-Client `' + this.name +'` send to server path: ' + opts.pathname + ' Payload-Len: ' + buffer.byteLength + 'byte', JSON.stringify(buffer));
            req.write(buffer);
            req.on('response', this.onResponse);
            req.end();
            resolve();
        });
    }

    private onResponse(resp: any) {
        Logger.debug('CoAP-Client received response!', resp);
        resp.on('end', () => {
            Logger.debug('CoAP-Client response ended!');
        });
    }

    
    public async disconnectForTest(): Promise<void> {
        return;
    }


}