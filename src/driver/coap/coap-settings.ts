import { IDriverSettings } from "../driver-settings.model";

export interface ICoapSettings extends IDriverSettings { 
    port: number,
    host: string,
    groupId: string,
    deviceId: string,
    password: string
}