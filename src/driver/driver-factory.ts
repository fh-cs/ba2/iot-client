import { Driver } from "./driver";
import { IDriverSettings } from "./driver-settings.model";
import { EDriverType } from "./driver-type.enum";
import { IMqttSettings } from "./mqtt/mqtt-settings";
import { DeviceMqttDriver } from "./mqtt/device-mqtt-driver";
import { ServerMqttDriver } from "./mqtt/server-mqtt-driver";
import { ClientCoapDriver } from "./coap/client-coap-driver";
import { IDeviceCoapSettings } from "./coap/client-coap-settings";
import { IServerCoapSettings } from "./coap/server-coap-settings";
import { ServerCoapDriver } from "./coap/server-coap-driver";
import { MqttSnGateway } from "./mqtt-sn-gw/mqtt-sn-gw-driver";
import { IMqttSnGatewaySettings } from "./mqtt-sn-gw/mqtt-sn-gw-settings";
import { IMqttSnSettings } from "./mqtt-sn/mqtt-sn-driver-settings";
import { DeviceMqttSnDriver } from "./mqtt-sn/device-mqtt-sn-driver";
import { Udt2DtlsGateway } from "./upd2dtls/udp2dtls-gw";
import { IUdp2DtlsGatewaySettings } from "./upd2dtls/udp2dtls-gw-settings";


export class DriverFactory {
    private static _services = new Map<string, Driver>();

    public static CREATE(settings: IDriverSettings) {
        if (!settings) {
            throw new Error('Create new driver failed, settings are not provided!');
        }
        switch (settings.type) {
            case EDriverType.DEVICE_MQTT_JSON:
                return new DeviceMqttDriver(settings as IMqttSettings);
            case EDriverType.SERVER_MQTT_JSON:
                return new ServerMqttDriver(settings as IMqttSettings);
            case EDriverType.CLIENT_COAP:
                return new ClientCoapDriver(settings as IDeviceCoapSettings);
            case EDriverType.SERVER_COAP:
                return new ServerCoapDriver(settings as IServerCoapSettings);
            case EDriverType.MQTT_SN_GW:
                return new MqttSnGateway(settings as IMqttSnGatewaySettings);
            case EDriverType.MQTT_SN_DEVICE:
                return new DeviceMqttSnDriver(settings as IMqttSnSettings);
            case EDriverType.UDP2DTLS_GW:
                return new Udt2DtlsGateway(settings as IUdp2DtlsGatewaySettings);
            default:
                throw new Error('DriverFactory: Type of ' + settings.type + ' does not exist (or not implemented yet)!');
        }
    }
    public static getService(nameOrSettings: string | IDriverSettings) {
        let name = '';
        let settings: IDriverSettings | undefined;
        if (typeof nameOrSettings === 'string') {
            name = nameOrSettings;
            settings = undefined;
        } else {
            name = nameOrSettings.name;
            settings = nameOrSettings;
        }
        const serv = this._services.get(name);
        if (serv) {
            return serv;
        } else {
            if (settings) {
                let driver = this.CREATE(settings);
                this._services.set(name, driver);
                return driver;
            } else {
                throw new Error('DriverFactory: Settings required to create new instance!');
            }
        }
    }
}