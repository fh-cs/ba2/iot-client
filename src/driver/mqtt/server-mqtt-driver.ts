import { MqttBufferSubscription } from "./mqtt-subscription";
import { ESchemaGroup } from "../../common/schema-group.enum";
import { MqttDriver } from "./mqtt-driver";

export class ServerMqttDriver extends MqttDriver {
    protected getTopicForPublish(schemaGroup: ESchemaGroup): string {
        return this.getTopic('dev', schemaGroup, 'arr');
    }
    protected getTopicForSubscription(schemaGroup: ESchemaGroup): string {
        return this.getTopic('srv', schemaGroup, 'arr', true);
    }

    protected async addSubscriptions() {
        await this.addSubscription(ESchemaGroup.DATA);
        await this.addSubscription(ESchemaGroup.CONFIG);
        await this.addSubscription(ESchemaGroup.NOTIFY);
        await this.addSubscription(ESchemaGroup.METHOD);
    }

    private async addSubscription(schemaGroup: ESchemaGroup) {
        const sub = new MqttBufferSubscription({
            topic: this.getTopicForSubscription(schemaGroup),
            callback: (topic, msg) => {
                this.onReceive.next({ schemaGroup: schemaGroup, data: msg });
            }
        });
        await this.addSubscriber(sub);
    }
}