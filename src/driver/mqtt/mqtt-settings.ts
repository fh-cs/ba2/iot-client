import { IDriverSettings } from "../driver-settings.model";

export interface IMqttSettings extends IDriverSettings {


    /**
     * Name of IP-Address of MQTT-Broker
     */
    host: string;
    /**
     * Port address the broker is listen.
     * If not set or 0, default port is used.
     */
    port?: number;
    /**
     * If set, connects to the MQTT-Broker over *mqtts* (instead of *mqtt*)
     */
    useSSL?: boolean;
    /**
     * Alpha-numeric identifier of the device-group this device belongs too
     */
    groupId: string;
    /**
     * Alpha-numeric identifier of the device
     * Used as topic for other subscriber
     */
    deviceId: string;
    /**
     * Maximum time-period (in 10seconds) packets sent from client to server. If time exceeded, client sends a ping-request to the server.
     * The broker returns a ping-response. If the broker receives no packets in about 1.5time of keep-alive, broker disconnects and publishes a last-will message to subscribers.
     */
    keepalive?: number;
    /**
     * If true, disable the clean-session flag. 
     */
    persistent?: boolean;
    /**
     * Id of the client required for authorization at MQTT-Broker
     */
    clientId: string;
    /**
     * The password required for device-authentication
     */
    password: string;
    /**
     * Path to the TLS-Certificate-File
     */
    certFile?: string;
    /**
     * Path to the CA-File for Encryption
     */
    caFile?: string;
    /**
     * If true, stores TLS-Session-ID locally and reuse session-id for next tls-handshake procedure
     */
    resumeTlsSession?: boolean;
    /**
     * Connection options definition for all data-variables
     */
    data: {
        publish: IMqttConnectionOptions
    };
    /**
     * Connection options definition for all notify-variables
     */
    notify: {
        publish: IMqttConnectionOptions
    };
    /**
     * Connection options definition for all config-variables
     */
    config: {
        publish: IMqttConnectionOptions, 
        subscribe:IMqttConnectionOptions
    };
    /**
     * Connection options definition for all method-variables
     */
    method: {
        publish: IMqttConnectionOptions, 
        subscribe:IMqttConnectionOptions
    };
    /**
     * Default-Connection options definition for publish messages not part of any group
     */
    publish: IMqttConnectionOptions;
    /**
     * Default-Connection options definition for subscriptions not part of any group
     */
    subscribe: IMqttConnectionOptions;
    
}


export interface IMqttConnectionOptions {
    /**
     * Quality of Service
     * 0 = At most once
     * 1 = At least once
     * 2 = Exactly once
     */
    qos: 0 | 1 | 2,
    /**
     * The broker stores the last retained message and the corresponding QoS for that topic.\
     * Each client that subscribes to a topic pattern that matches the topic of the retained 
     * message receives the retained message immediately after they subscribe.\
     * The broker stores only one retained message per topic.
     */
    retain: boolean
}