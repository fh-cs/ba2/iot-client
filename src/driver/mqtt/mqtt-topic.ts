export class MqttTopic {
    public static readonly TOPIC_DELIMITER = '/';
    public static readonly SINGLE_WILDCARD = '+';
    public static readonly MULTIPLE_WILDCARD = '#';

    public static readonly TOPIC_INDEX_GROUP_ID = 2;
    public static readonly TOPIC_INDEX_DEVICE_ID = 3;
    public static readonly TOPIC_INDEX_SCHEMA_GROUP = 4;
    public static readonly TOPIC_INDEX_VARNAME = 5;


    private _topic: string;
    public get topic() {
        return this._topic;
    }
    private _topicArray: string[];
    public get topicArray() {
        return this._topicArray;
    }

    constructor(topic: string) {
        this._topicArray = MqttTopic.convertTopicToArray(topic);
        // convert to array first for clean-up
        this._topic = MqttTopic.convertArrayToTopic(this._topicArray);
        
    }
    /**
     * Converts the given topic to a string-array of topic-items
     */
    public static convertTopicToArray(topic: string) {
        let t = topic.trim();
        if (t.startsWith(MqttTopic.TOPIC_DELIMITER)) {
            t = t.slice(1);
        }
        if (t.endsWith(MqttTopic.TOPIC_DELIMITER)) {
            t = t.slice(0, t.length - 1);
        }
        return t.split(MqttTopic.TOPIC_DELIMITER);
    }

    public static convertArrayToTopic(topicArray: string[]) {
        let topic = '';
        topicArray.forEach(t => {
            if (topic) {
                topic += MqttTopic.TOPIC_DELIMITER + t;
            } else {
                topic = t;
            }
        });
        return topic;
    }

    public static getTopicAsString(topicIn: string | string[]) {
        if (typeof topicIn === 'string') {
            return topicIn;
        } else {
            return this.convertArrayToTopic(topicIn);
        }
    }

       /**
     * Compares the topics and returns true, if the subscribed-topic belongs to the received-topic.
     * Wildcards are allowed for the subscribed-topic.
     * @param topicReceived The topic received from the broker (without wildcards)
     * @param topicSubscribed The topic subscribed to the broker (wildcards allowed)
     */
    public static isTopic(topicReceived: string | string[], topicSubscribed: string | string[]) {
        let topicRec: string[];
        let topicSub: string[];

        if (typeof topicReceived === 'string') {
            topicRec = MqttTopic.convertTopicToArray(topicReceived);
        } else {
            topicRec = topicReceived;
        }
        if (typeof topicSubscribed === 'string') {
            topicSub = MqttTopic.convertTopicToArray(topicSubscribed);
        } else {
            topicSub = topicSubscribed;
        }

        for (let i=0; i < topicRec.length; i++) {
            if (i < topicSub.length) {
                if (topicSub[i] === this.SINGLE_WILDCARD) {
                    //Next loop
                } else if (topicSub[i] === this.MULTIPLE_WILDCARD) {
                    return true;
                } else if (topicSub[i] !== topicRec[i]) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    public static isLastTopic(topic: string | string[], compareTopic: string) {
        let ta: string[];
        if (typeof topic === 'string') {
            ta = MqttTopic.convertTopicToArray(topic);
        } else {
            ta = topic;
        }
        if (ta && ta.length > 0) {
            return ta[ta.length - 1] === compareTopic;
        } else {
            return false;
        }
    }

    /**
     * Returns the topic at the given index.
     * Use static properties TOPIC_INDEX_* for index selection.
     */
    public static getTopicAtIndex(topic: string | string[], index: number) {
        let topicArray: string[];
        if (typeof topic === 'string') {
            topicArray = this.convertTopicToArray(topic);
        } else {
            topicArray = topic;
        }
        if (!topicArray || topicArray.length < 1) {
            throw new Error('Topic not set!');
        }
        if (index >= topicArray.length) {
            throw new Error('Topic at index `' + index + '` does not exist! Index must be between 0 and ' + (topicArray.length - 1) + '!');
        }
        return topicArray[index];
    }
    /**
     * Returns true if a topic is available at that index.
     * Use static properties TOPIC_INDEX_* for index selection.
     */
    public static hasTopicAtIndex(topic: string | string[], index: number) {
        try {
            return !!this.getTopicAtIndex(topic, index);
        } catch (e) {
            return false;
        }
    }

    /**
     * Returns true if a topic is available at that index.
     * Use static properties TOPIC_INDEX_* for index selection.
     */
    public static isTopicAtIndex(topic: string | string[], index: number, partTopic: string) {
        try {
            return this.getTopicAtIndex(topic, index) === partTopic;
        } catch (e) {
            return false;
        }
    }


    public isTopic(topicReceived: string | string[]) {
        return MqttTopic.isTopic(topicReceived, this.topicArray);
    }
    public isLastTopic(lastTopic: string) {
        return MqttTopic.isLastTopic(this.topicArray, lastTopic);
    }

    /**
     * Returns the topic at the given index.
     * Use static properties TOPIC_INDEX_* for index selection.
     */
    public getTopicAtIndex(index: number) {
        return MqttTopic.getTopicAtIndex(this.topicArray, index);
    }
    /**
     * Returns true if a topic is available at that index.
     * Use static properties TOPIC_INDEX_* for index selection.
     */
    public hasTopicAtIndex(index: number) {
        MqttTopic.hasTopicAtIndex(this.topicArray, index);
    }
    
}