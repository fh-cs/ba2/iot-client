import { MqttTopic } from "./mqtt-topic";

export interface IMqttSubscription<T> {
    topic: string,
    callback: (topic: string, message: T) => void
}


export abstract class MqttSubscription<T> implements IMqttSubscription<T> {
    

    public topic: string;
    public callback: (topic: string, message: T) => void;
    public typeOfData: 'string' |'number' | 'Buffer' | 'json' = 'json';
    private _topicArray?: string[];

    /**
     * Returns the topic of that subscription as string-array
     */
    public get topicArray() {
        if (!this._topicArray) {
            this._topicArray = MqttTopic.convertTopicToArray(this.topic);
        }
        return this._topicArray;
    }

    public isSubscribed = false;

    constructor(subscription: IMqttSubscription<T>, typeOfData: 'string' |'number' | 'Buffer' | 'json') {
        this.topic = subscription.topic;
        this.callback = subscription.callback;
        this.typeOfData = typeOfData;
    }


    public abstract emit(topic: string | string[], message: Buffer): void;

    /**
     * Returns true if the given topic belongs to the subscribed topic.
     * @param topic Topic of a received message from broker
     */
    isTopic(topic: string | string[]) {
        return MqttTopic.isTopic(topic, this.topicArray);
    }
}


export class MqttBufferSubscription extends MqttSubscription<Buffer> {
    constructor(subscription: IMqttSubscription<Buffer>) {
        super(subscription, 'Buffer');
    }
    public emit(topic: string | string[], message: Buffer): void {
        if (this.isTopic(topic)) {
            const t = MqttTopic.getTopicAsString(topic);
            this.callback(t, message);
        }
    }
}

export class MqttJsonSubscription<T> extends MqttSubscription<T> {
    constructor(subscription: IMqttSubscription<T>) {
        super(subscription, 'json');
    }
    public emit(topic: string | string[], message: Buffer): void {
        if (this.isTopic(topic)) {
            const obj = JSON.parse(message.toString()) as T;
            const t = MqttTopic.getTopicAsString(topic);
            this.callback(t, obj);
        }
    }
}

export class MqttStringSubscription extends MqttSubscription<string> {
    constructor(subscription: IMqttSubscription<string>) {
        super(subscription, 'string');
    }
    public emit(topic: string | string[], message: Buffer): void {
        if (this.isTopic(topic)) {
            const t = MqttTopic.getTopicAsString(topic);
            this.callback(t, message.toString());
        }
    }
}

export class MqttNumberSubscription extends MqttSubscription<number> {
    constructor(subscription: IMqttSubscription<number>) {
        super(subscription, 'number');
    }
    public emit(topic: string | string[], message: Buffer): void {
        if (this.isTopic(topic)) {
            const t = MqttTopic.getTopicAsString(topic);
            this.callback(t, message.readFloatBE(0));
        }
    }
}


