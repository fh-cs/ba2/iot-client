import mqtt, { ClientSubscribeCallback, PacketCallback, IClientPublishOptions, IClientOptions, IClientSubscribeOptions } from 'mqtt';
import { IMqttSettings, IMqttConnectionOptions } from './mqtt-settings';
import { MqttSubscription } from './mqtt-subscription';
import * as zlib from 'zlib';
import { MqttTopic } from './mqtt-topic';
import { BooleanVar } from '../../controller/rt-variable/runtime-var.model';
import { Driver } from '../driver';
import { ESchemaGroup } from '../../common/schema-group.enum';
import { Logger } from '../../common/logger/logger';



export abstract class MqttDriver extends Driver {

    public async connectForTest() {
        await this.connect();
        await this.addSubscriptions();
    }
    public async sendTest(testVarOrSchemaGroup: import("../../trigger/test-event.model").ITestVar | ESchemaGroup, buffer: Buffer) {
        const schemaGroup = typeof testVarOrSchemaGroup === 'object' ? testVarOrSchemaGroup.schemaGroup : testVarOrSchemaGroup;
        const settings = this.getSettings() as IMqttSettings;
        let options: IMqttConnectionOptions | undefined;
        if (settings[schemaGroup] && settings[schemaGroup].publish) {
            options = settings[schemaGroup] && settings[schemaGroup].publish;
        }
        await this.publish(this.getTopicForPublish(schemaGroup), buffer, options);
    }
    public async disconnectForTest() {
        return new Promise<void>((resolve, reject) => {
            if (this.client) {
                this.client.end(false, () => {
                    Logger.info('MQTT-Client ' + this.name + ' has closed connection!');
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    client: mqtt.MqttClient | undefined;


    private subscriptions = new Array<MqttSubscription<any>>();
    private _tlsSession: Buffer | undefined;

    //private _levelDB;
    private static dbName = "./db/iot-test";
    private static dbSessionKey = "mqtts-tls-session-2";
    /* protected get levelDB() {
        if (!this._levelDB) {
            var level = require('level');
            this._levelDB = level(MqttDriver.dbName);
        }
        return this._levelDB;
    }
 */

    protected get tlsSession(): Buffer {
        if (this._tlsSession === undefined) {
            throw new Error('A TLS Session is currently not available');
        }
        return this._tlsSession;
    }
    protected set tlsSession(value: Buffer) {
        this._tlsSession = value;
        if (value) {
            const stValue = JSON.stringify(value);
            const level = require('level');
            const levelDB = level(MqttDriver.dbName);
            levelDB.put(MqttDriver.dbSessionKey, stValue, (error) => {
                if (error) {
                    Logger.error('Failed to save tls-session-id', error);
                }
                levelDB.close();
            });
        } else {
            const level = require('level');
            const levelDB = level(MqttDriver.dbName);
            levelDB.del(MqttDriver.dbSessionKey, (error) => {
                if (error) {
                    Logger.error('Failed to save tls-session-id', error);
                }
                levelDB.close();
            });
        }
    }
    protected tlsSessionAvailable() {
        return this._tlsSession !== undefined;
    }

    protected loadTlsSessionFromDB() {
        return new Promise<boolean>((resolve, reject) => {
            const level = require('level');
            const levelDB = level(MqttDriver.dbName);
            levelDB.get(MqttDriver.dbSessionKey, (err, value) => {
                levelDB.close();
                if (err || !value) {
                    // key not found
                    Logger.warn('TLS-Session-ID not stored in Level-DB! Resume session not possible!');
                    this._tlsSession = undefined;
                    resolve(false);
                } else {
                    try {
                        const buf = JSON.parse(value) as Buffer;
                        this._tlsSession = Buffer.from(buf);
                        resolve(true);
                    } catch(err) {
                        reject(new Error('Load last tls-session-id from level-db failed! ' + (err as Error).message));
                    }
                    
                }
            });
        });
    }

    protected clearTlsSession() {
        return new Promise<void>((resolve, reject) => {
            this._tlsSession = undefined;
            const level = require('level');
            const levelDB = level(MqttDriver.dbName);
            levelDB.del(MqttDriver.dbSessionKey, (err) => {
                levelDB.close();
                resolve();
            });
        });
        
    }

    constructor(settings?: IMqttSettings) {
        super(settings);
        if((this.getSettings() as IMqttSettings).resumeTlsSession) {
            this.loadTlsSessionFromDB();
        }
    }

    public get url() {
        if (this.getSettings()) {
            const set = this.getSettings() as IMqttSettings;
            return 'mqtt' + (set.useSSL ? 's://' : '://') + set.host + (set.port ? ':' + Number(set.port) : '');
        } else {
            throw new Error('Settings not loaded!');
        }

    }
    public get isConnected() {
        return this.client ? this.client.connected : false;
    }


    protected abstract async addSubscriptions(): Promise<void>;
    protected abstract getTopicForPublish(schemaGroup: ESchemaGroup): string;
    protected abstract getTopicForSubscription(schemaGroup: ESchemaGroup): string;
    /**
     * Returns the topic in following format:\
     * - if topicToAppend is provided:\
     *  iot1/[device-group-id]/[device-id]/[topicToAppend]
     * - if topicToAppend not provided:\
     * iot1/[device-group-id]/[device-id]
     * @param forSubscription 
     * Appends a multiple-wildcard (#) at the end of the topic for subscription
     */
    public getTopic(topicToAppend?: string): string;
    public getTopic(topicToAppend?: string, forSubscription?: boolean): string;
    /**
     * Returns the topic in following format:\
     * iot1/[device-group-id]/[device-id]/[devOrSrv]/[schemaGroup]/[dataType]/(varName)
     * @param devOrSrv 
     * Direction of communication (to device or to server)\
     * Possible values are:
     *  - dev: Communication to device (device is subscriber, server publisher)
     *  - srv: Communication to server (server is subscriber, device publischer)
     * @param schemaGroup
     * The schema-group the variable(s) (defined in data or topic) belongs too
     *  - data: Value from the iot-client to the server
     *  - notify: Binary messages from the client to the server (alarm, warning,...)
     *  - config: Configuration-value from the server to the client. The client returns the value read from the device after request from server.
     *  - methods: Command from the server to the device. If the iot-client forwarded the command to the device successfully, the iot-client returns the value send to the device. In case of error, value null will be returned instead.
     * @param varName
     * Name of the variable. Only required if previous topic is obj.
     * @param forSubscription 
     * Appends a multiple-wildcard (#) at the end of the topic for subscription
     */
    public getTopic(devOrSrv: 'dev' | 'srv', schemaGroup: ESchemaGroup | '+', dataType: 'obj' | 'arr' | '+', forSubscription?: boolean): string;
    public getTopic(devOrSrv: 'dev' | 'srv', schemaGroup: ESchemaGroup | '+', dataType: 'obj' | 'arr' | '+', varName?: string, appendWildcardForSubscription?: boolean): string;
    public getTopic(devSrvOrTopicToAppend?: string, schemaGroupOrSubscription?: ESchemaGroup | '+' | boolean, dataType?: 'obj' | 'arr' | '+', varNameOrSubscription?: string | boolean, forSubscription?: boolean): string {
        const set = this.getSettings() as IMqttSettings;
        const baseTopic = 'iot1' + MqttTopic.TOPIC_DELIMITER + set.groupId + MqttTopic.TOPIC_DELIMITER + set.deviceId;
        if (devSrvOrTopicToAppend === 'dev' || devSrvOrTopicToAppend === 'srv') {
            if (!schemaGroupOrSubscription) {
                throw new Error('Argument schemaGroup is missing!');
            }
            if (typeof schemaGroupOrSubscription !== 'string') {
                throw new Error('Argument schemaGroup must be from type `string`!');
            }
            if (!dataType) {
                throw new Error('Argument dataType is missing!');
            }

            let varName: string | undefined;
            let appendSub = false;
            if (varNameOrSubscription && typeof varNameOrSubscription === 'string') {
                varName = varNameOrSubscription;
            }
            if (varNameOrSubscription && typeof varNameOrSubscription === 'boolean') {
                appendSub = varNameOrSubscription;
            }
            if (forSubscription) {
                appendSub = forSubscription;
            }

            let appTopic = MqttTopic.TOPIC_DELIMITER + devSrvOrTopicToAppend +
                MqttTopic.TOPIC_DELIMITER + schemaGroupOrSubscription +
                MqttTopic.TOPIC_DELIMITER + dataType +
                (varName ? MqttTopic.TOPIC_DELIMITER + varName : '');
            appTopic += appendSub ? MqttTopic.TOPIC_DELIMITER + MqttTopic.MULTIPLE_WILDCARD : '';

            return baseTopic + appTopic;
        } else {
            // Append topic (first version)
            if (schemaGroupOrSubscription && typeof schemaGroupOrSubscription !== 'boolean') {
                throw new Error('Argument `forSubscription` must be from type `boolean`!');
            }
            let appTopic = devSrvOrTopicToAppend ? MqttTopic.TOPIC_DELIMITER + devSrvOrTopicToAppend : '';
            appTopic += schemaGroupOrSubscription ? MqttTopic.TOPIC_DELIMITER + MqttTopic.MULTIPLE_WILDCARD : '';
            return baseTopic + appTopic;
        }

    }


    public async connect() {
        const set = this.getSettings() as IMqttSettings;
        if (set.resumeTlsSession) {
            await this.loadTlsSessionFromDB();
        } else {
            await this.clearTlsSession();
        }
        return new Promise<void>((resolve, reject) => {
            Logger.debug('MQTT Start connecting to: ' + this.url);
            const connOptions: IClientOptions = {
                will: {
                    topic: this.getTopic('srv', ESchemaGroup.NOTIFY, 'obj', 'CONN_STATE'),
                    payload: JSON.stringify(new BooleanVar('CONN_STATE', false)),
                    qos: set.notify.publish.qos,
                    retain: set.notify.publish.retain
                },
                clientId: set.clientId,
                username: set.clientId,
                password: set.password,
                keepalive: set.keepalive,
                clean: !set.persistent,
                ca: set.caFile,
                cert: set.certFile,
                rejectUnauthorized: false,
                session: this.tlsSessionAvailable() ? this.tlsSession : undefined
            };
            this.client = mqtt.connect(this.url, connOptions);
            this.client.on('connect', () => {
                Logger.info('MQTT connected to ' + this.url);
                this.publish(this.getTopic('srv', ESchemaGroup.NOTIFY, 'obj', 'CONN_STATE'), new BooleanVar('CONN_STATE', true), set.notify.publish);
                // Subscribe none-subscribed subscriptions
                this.subscriptions.forEach(s => {
                    if (!s.isSubscribed) {
                        this.subscribe(s);
                    }
                });
                resolve();
            });
            this.client.on('message', (topic, message) => {
                this.processMessage(topic, message);
            });
            this.client.on('session', (buffer) => {
                Logger.info('Received session ticket');
                this.tlsSession = buffer;
            });


        });
    }

    private async processMessage(topic: string, message: Buffer) {
        const topicArray = MqttTopic.convertTopicToArray(topic);
        if (MqttTopic.isLastTopic(topicArray, 'zip')) {
            Logger.debug('MQTT received topic : ' + topic + ' Compressed message-length: ' + message.length);
            message = await this.extractData(message, 'Buffer') as Buffer;
            topicArray.splice(topicArray.length - 1, 1);
            topic = MqttTopic.convertArrayToTopic(topicArray);
        }
        Logger.debug('MQTT received topic : ' + topic + ' Payload-length: ' + message.byteLength + 'byte');
        this.subscriptions.forEach(sub => {
            try {
                sub.emit(topicArray, message);
            } catch (error) {
                Logger.error('Failed to emit message to subsciber! Topic subscribed: ' + sub.topic + ' Topic received: ' + topic, error);
            }
        });
    }

    public publish(topic: string, message: string | Buffer | object | number, compress?: boolean): Promise<boolean>;
    public publish(topic: string, message: string | Buffer | object | number, options?: IClientPublishOptions): Promise<boolean>;
    public async publish(topic: string, message: string | Buffer | object | number, optionsOrCompress?: IClientPublishOptions | boolean, compress?: boolean): Promise<boolean> {
        const set = this.getSettings() as IMqttSettings;
        if (!set) {
            throw new Error('Invalid call of publish! Settings are missing!');
        }
        const opt: IClientPublishOptions = {
            qos: set.publish.qos,
            retain: set.publish.retain
        };
        let comp = false;
        if (optionsOrCompress !== undefined) {
            if (typeof optionsOrCompress === 'boolean') {
                // options is argument 'compromise' (overloaded)
                comp = optionsOrCompress;
            } else {
                // options contains publish-options
                const options = optionsOrCompress as IMqttConnectionOptions;
                opt.qos = options.qos;
                opt.retain = options.retain
            };
        }

        if (compress) {
            comp = compress;
        }
        // Prepare data
        let data: string | Buffer;
        if (typeof message === 'string') {
            data = message;
        } else if (typeof message === 'number') {
            data = Buffer.allocUnsafe(4);
            data.writeFloatBE(message, 0);
        } else {
            if (Buffer.isBuffer(message)) {
                data = message;
            } else {
                data = JSON.stringify(message);
            }
        }

        if (comp) {
            data = await this.compressData(data);
            topic += '/zip';
        }

        return new Promise<boolean>((resolve, reject) => {
            if (this.client) {
                this.client.publish(topic, data, opt, (err, packet) => {
                    if (err) {
                        Logger.error('Failed to publish message at topic `' + topic + '`!', err);
                        resolve(false);
                    } else {
                        resolve(true);
                    }
                });
            } else {
                reject(new Error('Publish message at topic `' + topic + '` failed, MQTT-Client not initialized!'));
            }
        });
    }

    protected subscribe(topic: string, callback?: ClientSubscribeCallback, options?: IClientSubscribeOptions): void;
    protected subscribe(subscription: MqttSubscription<any>, callback?: ClientSubscribeCallback, options?: IClientSubscribeOptions): void;
    protected subscribe(topicOrSubscription: string | MqttSubscription<any>, callback?: ClientSubscribeCallback, options?: IClientSubscribeOptions): void {
        if (typeof topicOrSubscription === 'object') {
            const subscriber = topicOrSubscription;
            // Get client-options from settings, if topic belongs to schema-group (config, method)
            //let options: mqtt.IClientSubscribeOptions | undefined;
            if (MqttTopic.hasTopicAtIndex(subscriber.topicArray, MqttTopic.TOPIC_INDEX_SCHEMA_GROUP)) {
                const group = MqttTopic.getTopicAtIndex(subscriber.topicArray, MqttTopic.TOPIC_INDEX_SCHEMA_GROUP);
                // *set* must be any, because only 'config' and 'method'-groups contains subscribe-property
                const set = this.getSettings() as any;
                if (set[group] && set[group].subscribe) {
                    const opt = set[group].subscribe as IMqttConnectionOptions;
                    options = {
                        qos: opt.qos
                    };
                }
            }
            return this.subscribe(subscriber.topic, callback, options);
        }

        const set = this.getSettings() as IMqttSettings;
        let opt: IClientSubscribeOptions;
        if (!options) {
            opt = {
                qos: set.subscribe.qos
            }
        } else {
            opt = options;
        }
        if (this.client) {
            this.client.subscribe(topicOrSubscription, opt, callback);
        } else {
            throw new Error('Subscribe failed, MQTT-Client not initialized! Topic: ' + topicOrSubscription);
        }
    }

    protected unsubscribe(topic: string, callback?: PacketCallback) {
        if (this.client) {
            this.client.unsubscribe(topic, callback);
        } else {
            throw new Error('Unsubscribe failed, MQTT-Client not initialized! Topic: ' + topic);
        }
    }


    public addSubscriber(subscriber: MqttSubscription<any>) {
        return new Promise<boolean>((resolve, reject) => {
            try {
                // topic is subscribed
                const subExist = this.subscriptions.find(s => s.topic === subscriber.topic);
                if (subExist) {
                    resolve(subExist.isSubscribed);
                } else {
                    this.subscriptions.push(subscriber);
                    if (!subscriber.isSubscribed) {
                        // Get client-options from settings, if topic belongs to schema-group (config, method)
                        let options: mqtt.IClientSubscribeOptions | undefined;
                        if (MqttTopic.hasTopicAtIndex(subscriber.topicArray, MqttTopic.TOPIC_INDEX_SCHEMA_GROUP)) {
                            const group = MqttTopic.getTopicAtIndex(subscriber.topicArray, MqttTopic.TOPIC_INDEX_SCHEMA_GROUP);
                            // *set* must be any, because only 'config' and 'method'-groups contains subscribe-property
                            const set = this.getSettings() as any;
                            if (set[group] && set[group].subscribe) {
                                const opt = set[group].subscribe as IMqttConnectionOptions;
                                options = {
                                    qos: opt.qos
                                };
                            }
                        }

                        this.subscribe(subscriber.topic, (error, granted) => {
                            if (error) {
                                Logger.error('Failed to subscribe topic ' + subscriber.topic, error);
                                resolve(false);
                            } else {
                                subscriber.isSubscribed = true;
                                resolve(true);
                            }
                        }, options);
                    }
                }
            } catch (error) {
                reject(error);
            }
        });
    }

    public getSubscriber(topic: string) {
        return this.subscriptions.find(s => s.topic === topic);
    }

    public removeSubscriber(topic: string | MqttSubscription<any>) {
        let t = typeof topic === 'string' ? topic : topic.topic;
        return new Promise<MqttSubscription<any>>((resolve, reject) => {
            try {
                // topic is subscribed
                const subExist = this.subscriptions.find(s => s.topic === t);
                if (subExist) {
                    this.unsubscribe(subExist.topic, (error, packet) => {
                        if (error) {
                            reject(error);
                        } else {
                            subExist.isSubscribed = false;
                            const i = this.subscriptions.findIndex(s => s.topic === t);
                            resolve(this.subscriptions.splice(i, 1)[0]);
                        }
                    });
                } else {
                    const err = new Error('Remove subscriber from MQTT-Client failed, subscriber with topic `' + t + '` not found!');
                    reject(err);
                }
            } catch (error) {
                reject(error);
            }
        });
    }


    protected compressData(data: string | Buffer | object): Promise<Buffer> {
        let input: string | Buffer;
        if (typeof data === 'object' && !Buffer.isBuffer(data)) {
            input = JSON.stringify(data);
        } else {
            input = data;
        }
        return new Promise<Buffer>((resolve, reject) => {
            zlib.deflate(input, (err, buf) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(buf);
                }
            });
        });
    }

    protected extractDataToString(compressedData: Buffer) {
        return this.extractData(compressedData, 'string') as Promise<string>;
    }
    protected extractDataToBuffer(compressedData: Buffer) {
        return this.extractData(compressedData, 'Buffer') as Promise<Buffer>;
    }

    protected extractDataToObject<T extends object>(compressedData: Buffer) {
        return this.extractData(compressedData, 'object') as Promise<T>;
    }

    protected extractData(compressedData: Buffer, type: 'string' | 'Buffer' | 'object'): Promise<string | Buffer | object> {
        return new Promise<string | Buffer | object>((resolve, reject) => {
            zlib.inflate(compressedData, (err, buf) => {
                if (err) {
                    reject(err);
                } else {
                    if (type === 'string') {
                        resolve(buf.toString());
                    } else if (type === 'object') {
                        resolve(JSON.parse(buf.toString()));
                    } else {
                        resolve(buf);
                    }
                }
            });
        });
    }




}